package com.zjshell.pricing;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author YangLei
 */
@SpringBootApplication
@EnableAsync
@EnableTransactionManagement
@MapperScan("com.zjshell.pricing.*.mapper")
public class FebsApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
        SpringApplication.run(FebsApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(FebsApplication.class);
    }

	
/*    public static void main(String[] args) {
        new SpringApplicationBuilder(FebsApplication.class).run(args);
    }*/

}
