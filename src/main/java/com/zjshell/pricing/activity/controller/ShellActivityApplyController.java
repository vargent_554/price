package com.zjshell.pricing.activity.controller;


import com.zjshell.pricing.activity.dto.ShellActivityApplyDto;
import com.zjshell.pricing.activity.entity.ActivityDic;
import com.zjshell.pricing.activity.entity.ShellActivity;
import com.zjshell.pricing.activity.entity.ShellActivityApply;
import com.zjshell.pricing.activity.service.ShellActivityService;
import com.zjshell.pricing.common.annotation.ControllerEndpoint;
import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsResponse;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.generator.entity.GeneratorConfig;
import com.zjshell.pricing.generator.helper.GeneratorHelper;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;


@Slf4j
@RestController
@RequestMapping("shellActivityApply")
public class ShellActivityApplyController extends BaseController {

    @Autowired
    private ShellActivityService shellActivityService;

    @Autowired
    private GeneratorHelper generatorHelper;


    @PostMapping("shellActivityApplyAdd")
    @RequiresPermissions("shellActivityApply:add")
    @ControllerEndpoint(operation = "新增活动主体申请", exceptionMessage = "新增活动主体申请失败")
    public FebsResponse addshellActivityApply(@Valid ShellActivityApply shellActivityApply) {
        this.shellActivityService.doAddShellActivityApply(shellActivityApply);
        return new FebsResponse().success();
    }


    @PostMapping("shellActivityApplyDetailAdd")
    @RequiresPermissions("shellActivityApplyDetail:add")
    @ControllerEndpoint(operation = "新增活动详情", exceptionMessage = "新增活动详情失败")
    public FebsResponse addhellActivityApplyDetail(String jsonApply) {
        ShellActivityApplyDto shellActivityApplyDto=JSONObject.parseObject(jsonApply,ShellActivityApplyDto.class);
        this.shellActivityService.doAddShellActivityApplyDetail(shellActivityApplyDto);
        return new FebsResponse().success();
    }


    @GetMapping("shellActivityApplyList")
    @ControllerEndpoint(operation = "活动申请分页", exceptionMessage = "活动申请分页")
    public FebsResponse shellActivityApplyList(ShellActivityApply shellActivityApply,QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.shellActivityService.findshellActivityApplyList(shellActivityApply, request));
        return new FebsResponse().success().data(dataTable);
    }

    @GetMapping("shellActivityApplyDetail")
    @ControllerEndpoint(operation = "活动申请详情", exceptionMessage = "活动申请详情")
    public FebsResponse shellActivityApplyDetail(String  id) {
        ShellActivityApplyDto shellActivityApplyDto=shellActivityService.findShellActivityApplyDetailById(id);
        return new FebsResponse().success().data(shellActivityApplyDto);
    }


    @GetMapping("shellActivityApplyPush")
    @ControllerEndpoint(operation = "活动申请推送crm", exceptionMessage = "活动申请推送crm失败")
    public FebsResponse shellActivityApplyPush(String  id) {
        ShellActivityApplyDto shellActivityApplyDto=shellActivityService.findShellActivityApplyDetailById(id);
        try {
            generatorHelper.shellActivityApplyMainPush(new GeneratorConfig(),shellActivityApplyDto);
            generatorHelper.shellActivityApplyTotalPush(new GeneratorConfig(),shellActivityApplyDto);
            generatorHelper.shellActivityApplyDetailPush(new GeneratorConfig(),shellActivityApplyDto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new FebsResponse().success().data(null);
    }

//    @GetMapping("createShellActivityApply")
//    @ControllerEndpoint(operation = "生成excel", exceptionMessage = "生成excel失败")
//    public Object createShellActivityApply() {
//
//        try {
//            generatorHelper.generateTest(new GeneratorConfig());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return new FebsResponse().success().data(null);
//    }

}
