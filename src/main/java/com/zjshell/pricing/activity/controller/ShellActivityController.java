package com.zjshell.pricing.activity.controller;

import com.zjshell.pricing.activity.dto.ShellActivityOilDto;
import com.zjshell.pricing.activity.entity.ActivityDic;
import com.zjshell.pricing.activity.entity.ActivityDicDetail;
import com.zjshell.pricing.activity.entity.ShellActivityDic;
import com.zjshell.pricing.activity.entity.ShellActivityOil;
import com.zjshell.pricing.activity.service.ShellActivityService;
import com.zjshell.pricing.common.annotation.ControllerEndpoint;
import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsResponse;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.exception.FebsException;
import com.zjshell.pricing.dict.entity.DictSite;
import com.zjshell.pricing.site.entity.ShellSite;
import com.zjshell.pricing.site.service.IShellSiteService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("shellActivity")
public class ShellActivityController extends BaseController {

    @Autowired
    private ShellActivityService shellActivityService;

    @GetMapping("dictList")
    @ControllerEndpoint(exceptionMessage = "获取活动数据字典失败")
    public FebsResponse dictList(ActivityDic activityDic, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.shellActivityService.findActivityDictList(activityDic, request));
        return new FebsResponse().success().data(dataTable);
    }

    @GetMapping("dictDetailList")
    @ControllerEndpoint(exceptionMessage = "获取活动数据字典详情列表失败")
    public FebsResponse dictDetailList(ActivityDicDetail activityDicDetail, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.shellActivityService.findActivityDictDetailList(activityDicDetail, request));
        return new FebsResponse().success().data(dataTable);
    }


    @PostMapping("dicAdd")
    @RequiresPermissions("shellActivityDic:add")
    @ControllerEndpoint(operation = "新增活动数据字典", exceptionMessage = "新增活动数据字典失败")
    public FebsResponse addShellActivityDic(@Valid ActivityDic activityDic) {
        this.shellActivityService.doSaveShellActivityDic(activityDic);
        return new FebsResponse().success();
    }

    @PostMapping("dicUpdate")
    @RequiresPermissions("shellActivityDic:update")
    @ControllerEndpoint(operation = "修改活动数据字典", exceptionMessage = "修改活动数据字典失败")
    public FebsResponse updateShellActivityDic(@Valid ActivityDic activityDic) throws FebsException {
        this.shellActivityService.updateShellActivityDic(activityDic);
        return new FebsResponse().success();
    }

    @GetMapping("dicDelete/{dsIds}")
    @RequiresPermissions("shellActivityDic:delete")
    @ControllerEndpoint(operation = "删除活动数据字典", exceptionMessage = "删除活动数据字典失败")
    public FebsResponse deleteShellActivityDic(@NotBlank(message = "{required}") @PathVariable String dsIds) throws FebsException {
        String[] ids = dsIds.split(StringPool.COMMA);
        this.shellActivityService.deleteShellActivityDic(ids);
        return new FebsResponse().success();
    }


    @PostMapping("dicDetailAdd")
    @RequiresPermissions("shellActivityDetailDic:add")
    @ControllerEndpoint(operation = "新增活动数据字典明细", exceptionMessage = "新增活动数据字典明细失败")
    public FebsResponse addShellActivityDicDetail(@Valid ActivityDicDetail activityDicDetail) {
        this.shellActivityService.doSaveShellActivityDicDetail(activityDicDetail);
        return new FebsResponse().success();
    }

    @PostMapping("dicDetailUpdate")
    @RequiresPermissions("shellActivityDetailDic:update")
    @ControllerEndpoint(operation = "修改活动数据字典明细", exceptionMessage = "修改活动数据字典明细失败")
    public FebsResponse updateShellActivityDicDetail(@Valid ActivityDicDetail activityDicDetail) throws FebsException {
        this.shellActivityService.updateShellActivityDicDetail(activityDicDetail);
        return new FebsResponse().success();
    }

    @GetMapping("dicDetailDelete/{dsIds}")
    @RequiresPermissions("shellActivityDetailDic:delete")
    @ControllerEndpoint(operation = "删除活动数据字典明细", exceptionMessage = "删除活动数据字典明细失败")
    public FebsResponse deleteShellActivityDicDetail(@NotBlank(message = "{required}") @PathVariable String dsIds) throws FebsException {
        String[] ids = dsIds.split(StringPool.COMMA);
        this.shellActivityService.deleteShellActivityDicDetail(ids);
        return new FebsResponse().success();
    }


    @GetMapping("shellActivityDictList")
//    @RequiresPermissions("shellActivityDict:view")
    @ControllerEndpoint(exceptionMessage = "获取油站活动数据字典失败")
    public FebsResponse shellActivityDictList(ShellActivityDic shellActivityDic, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.shellActivityService.findshellActivityDictList(shellActivityDic, request));
        return new FebsResponse().success().data(dataTable);
    }


    @PostMapping("shellActivityDictAdd")
    @RequiresPermissions("shellActivityDict:add")
    @ControllerEndpoint(operation = "新增活动项目数据字典", exceptionMessage = "新增活动项目数据字典失败")
    public FebsResponse shellActivityDicAdd(@Valid ShellActivityDic shellActivityDic) {
        this.shellActivityService.doSaveActivityDic(shellActivityDic);
        return new FebsResponse().success();
    }

    @PostMapping("shellActivityDictUpdate")
    @RequiresPermissions("shellActivityDict:update")
    @ControllerEndpoint(operation = "修改活动项目数据字典", exceptionMessage = "修改活动项目数据字典失败")
    public FebsResponse shellActivityDicUpdate(@Valid ShellActivityDic shellActivityDic) throws FebsException {
        this.shellActivityService.updateActivityDic(shellActivityDic);
        return new FebsResponse().success();
    }

    @GetMapping("shellActivityDictDelete/{dsIds}")
    @RequiresPermissions("shellActivityDict:delete")
    @ControllerEndpoint(operation = "删除活动项目数据字典", exceptionMessage = "删除活动项目数据字典失败")
    public FebsResponse shellActivityDicDelete(@NotBlank(message = "{required}") @PathVariable String dsIds) throws FebsException {
        String[] ids = dsIds.split(StringPool.COMMA);
        this.shellActivityService.deleteActivityDic(ids);
        return new FebsResponse().success();
    }

    /**
     * 活动设置远程检索油站信息
     * @param shellSite
     * @return
     */
    @GetMapping("shellOilList")
    @ControllerEndpoint(exceptionMessage = "油站远程检索匹配失败")
    public FebsResponse shellOilList( ShellSite shellSite) {
        List<ShellSite> shellSiteList=this.shellActivityService.findShellSiteList(shellSite);
        return new FebsResponse().success().data(shellSiteList);
    }

    @GetMapping("shellOilDetail")
    @ControllerEndpoint(exceptionMessage = "油站详情")
    public FebsResponse shellOilDetail(String shellId) {
        ShellSite shellSite=this.shellActivityService.findShellSiteDetail(shellId);
        return new FebsResponse().success().data(shellSite);
    }

    /**
     * 检索油站结果
     */
    /**
     * 活动设置远程检索油站信息
     * @param shellSite
     * @return
     * activityType 0是价格调查1是活动调查
     */
    @GetMapping("shellOilAllList")
    @ControllerEndpoint(exceptionMessage = "油站活动匹配失败")
    public FebsResponse shellOilAllList( String  shellId,String activityType,String searchDate) {

        List<ShellActivityOilDto> shellActivityOilDtos=this.shellActivityService.findShellAllSiteList(shellId,activityType,searchDate);
        IPage<ShellActivityOilDto> page =new Page<>();
        page.setTotal(100);
        page.setCurrent(1);
        page.setSize(100);
        page.setRecords(shellActivityOilDtos);
        Map<String, Object> dataTable = getDataTable(page);
        return new FebsResponse().success().data(dataTable);
    }

    @GetMapping("shellOilAllListA")
    @ControllerEndpoint(exceptionMessage = "油站活动匹配失败")
    public FebsResponse shellOilAllListA( String  shellId,String activityType) {

        List<ShellActivityOilDto> shellActivityOilDtos=this.shellActivityService.findShellAllSiteListA(shellId,activityType);
        IPage<ShellActivityOilDto> page =new Page<>();
        page.setTotal(100);
        page.setCurrent(1);
        page.setSize(100);
        page.setRecords(shellActivityOilDtos);
        Map<String, Object> dataTable = getDataTable(page);
        return new FebsResponse().success().data(dataTable);
    }



    /**
     * 价格保存
     */
    @PostMapping("shellActivityPriceSave")
    @RequiresPermissions("shellActivityPrice:save")
    @ControllerEndpoint(operation = "价格调查保存", exceptionMessage = "价格调查保存")
    public FebsResponse shellActivityPriceSave(String jsonPrice) throws FebsException {


//        List<ShellActivityOilDto> a=new ArrayList<>();
//        ShellActivityOilDto d=new ShellActivityOilDto();
//        d.setOilCode("2222");
//        d.setShellId("1");
//        List<ShellActivityOil> oils=new ArrayList<>();
//        ShellActivityOil c=new ShellActivityOil();
//        c.setOilCode("332");
//        oils.add(c);
//        d.setShellActivityOilList(oils);
//        a.add(d);
//        a.add(d);
//        String json =JSONObject.toJSONString(a);
        List<ShellActivityOilDto> shellActivityOilDtoList= JSONObject.parseArray(jsonPrice,ShellActivityOilDto.class);
        shellActivityService.doSaveShellActivityPrice(shellActivityOilDtoList);
        return new FebsResponse().success();
    }



    /**
     * 活动保存
     */
    @PostMapping("shellActivityDetailSave")
//    @RequiresPermissions("shellActivityDetailSave:save")
    @ControllerEndpoint(operation = "活动调查保存", exceptionMessage = "活动调查保存")
    public FebsResponse shellActivityDetailSave(String jsonPrice) throws FebsException {


//        List<ShellActivityOilDto> a=new ArrayList<>();
//        ShellActivityOilDto d=new ShellActivityOilDto();
//        ShellActivityOilDto t=new ShellActivityOilDto();
//        d.setOilCode("2222");
//        d.setShellId("1");
//        t.setOilCode("2222");
//        t.setShellId("1");
//        List<ShellActivityOil> oils=new ArrayList<>();
//        List<ShellActivityOil> oils1=new ArrayList<>();
//        ShellActivityOil c=new ShellActivityOil();
//        c.setOilCode("332");
//        ShellActivityOil cc=new ShellActivityOil();
//        c.setOilCode("3323");
//        oils.add(c);
//        oils.add(cc);
//        oils1.add(c);
//        oils1.add(cc);
//        d.setShellActivityOilList(oils);
//        t.setShellActivityOilList(oils1);
//        a.add(d);
//        a.add(t);
//        String json =JSONObject.toJSONString(a);
        List<ShellActivityOilDto> shellActivityOilDtoList= JSONObject.parseArray(jsonPrice,ShellActivityOilDto.class);
        shellActivityService.doSaveShellActivityDetail(shellActivityOilDtoList);
        return new FebsResponse().success();
    }


    /**
     * 油站活动调查历史
     * @param shellSite
     * @return
     * activityType 0是价格调查1是活动调查
     */
    @GetMapping("shellActovityList")
    @ControllerEndpoint(exceptionMessage = "油站活动调查历史")
    public FebsResponse shellActovityList( String  shellId,String activityType) {
        //activityType 0 价格调查 1活动调查
        if(StringUtils.isEmpty(activityType)&&activityType.equals("0")){

        }else{

        }
        return new FebsResponse().success().data(null);
    }


    /**
     * 常规设置
     * @param shellSite
     * @return
     * activityType
     */
    @GetMapping("activityDicDetailList")
    @ControllerEndpoint(exceptionMessage = "常规设置列表")
    public FebsResponse activityDicDetailList( String  code) {
        ActivityDicDetail detail=new ActivityDicDetail();
        detail.setParentCode(code);
        List<ActivityDicDetail> activityDicDetailList=shellActivityService.findDictListByParentCode(detail);
        return new FebsResponse().success().data(activityDicDetailList);
    }


    /**
     * //促销活动设置（自营0和竞争对手1）
     * @param shellSite
     * @return
     * activityType
     */
    @GetMapping("shellActivityDicList")
    @ControllerEndpoint(exceptionMessage = "促销活动设置")
    public FebsResponse shellActivityDicList( String  type) {
        ShellActivityDic dic=new ShellActivityDic();
        dic.setType(type);
        List<ShellActivityDic> activityDicDetailList=shellActivityService.findShellActivityByType(dic);
        return new FebsResponse().success().data(activityDicDetailList);
    }


    /**
     * 活动保存
     */
    @PostMapping("shellActivityDetaiDel")
    @ControllerEndpoint(operation = "单条活动删除", exceptionMessage = "单条活动删除")
    public FebsResponse shellActivityDetaiDel(String id) throws FebsException {
        shellActivityService.doShellActivityDetaiDel(id);
        return new FebsResponse().success();
    }
}
