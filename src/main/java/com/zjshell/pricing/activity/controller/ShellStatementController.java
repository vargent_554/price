package com.zjshell.pricing.activity.controller;

import com.zjshell.pricing.activity.dto.ShellActivityApplyDto;
import com.zjshell.pricing.activity.dto.ShellStatementDto;
import com.zjshell.pricing.activity.entity.ActivityDic;
import com.zjshell.pricing.activity.entity.ActivityDicDetail;
import com.zjshell.pricing.activity.entity.ShellStatement;
import com.zjshell.pricing.activity.service.ShellActivityService;
import com.zjshell.pricing.common.annotation.ControllerEndpoint;
import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsResponse;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.exception.FebsException;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.zjshell.pricing.activity.service.ShellActivityService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("shellStatement")
public class ShellStatementController extends BaseController {

    @Autowired
    private ShellActivityService shellActivityService;

    @GetMapping("shellStatementList")
    @ControllerEndpoint(exceptionMessage = "价值评估表分页")
    public FebsResponse shellStatementList(ShellStatement shellStatement, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.shellActivityService.findShellStatementList(shellStatement, request));
        return new FebsResponse().success().data(dataTable);
    }


    @GetMapping("shellStatement/{dsIds}")
    @RequiresPermissions("shellStatement:delete")
    @ControllerEndpoint(operation = "删除价值评估表", exceptionMessage = "删除价值评估表失败")
    public FebsResponse deleteShellStatement(@NotBlank(message = "{required}") @PathVariable String dsIds) throws FebsException {
        String[] ids = dsIds.split(StringPool.COMMA);
        this.shellActivityService.deleteShellStatement(ids);
        return new FebsResponse().success();
    }

    @GetMapping("shellStatementyDetail")
    @ControllerEndpoint(operation = "价值评估表详情", exceptionMessage = "价值评估表详情")
    public FebsResponse shellStatementyDetail(String  id) {
        ShellStatementDto shellStatementDto=shellActivityService.findShellStatementyDetailById(id);
        return new FebsResponse().success().data(shellStatementDto);
    }

    @PostMapping("shellStatementAdd")
    @RequiresPermissions("shellStatement:add")
    @ControllerEndpoint(operation = "新增价值评估表", exceptionMessage = "新增价值评估表")
    public FebsResponse addShellStatement(String jsonStatement) {
        ShellStatementDto shellStatementDto=JSONObject.parseObject(jsonStatement,ShellStatementDto.class);
        this.shellActivityService.doSaveShellStatement(shellStatementDto);
        return new FebsResponse().success();
    }

    @PostMapping("shellStatementUpdate")
    @RequiresPermissions("shellStatement:update")
    @ControllerEndpoint(operation = "修改价值评估表", exceptionMessage = "修改价值评估表失败")
    public FebsResponse updateShellStatement(String jsonStatement) throws FebsException {
        ShellStatementDto shellStatementDto=JSONObject.parseObject(jsonStatement,ShellStatementDto.class);
        this.shellActivityService.updateShellStatement(shellStatementDto);
        return new FebsResponse().success();
    }
}
