package com.zjshell.pricing.activity.controller;

import com.zjshell.pricing.activity.entity.ActivityDic;
import com.zjshell.pricing.activity.entity.ActivityDicDetail;
import com.zjshell.pricing.activity.entity.ShellActivity;
import com.zjshell.pricing.activity.entity.ShellActivityDic;
import com.zjshell.pricing.activity.service.ShellActivityService;
import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsConstant;
import com.zjshell.pricing.common.utils.FebsUtil;
import com.zjshell.pricing.dict.entity.DictSite;
import com.zjshell.pricing.dict.service.IDictSiteService;
import com.zjshell.pricing.job.entity.Job;
import com.zjshell.pricing.job.service.IJobService;
import com.zjshell.pricing.site.entity.ShellSiteInfo;
import com.zjshell.pricing.site.entity.ShellSiteInfoList;
import com.zjshell.pricing.site.entity.ShellSiteOil;
import com.zjshell.pricing.site.service.IShellSiteInfoService;
import com.zjshell.pricing.site.service.IShellSiteOilService;
import com.zjshell.pricing.site.service.IShellSiteService;
import com.zjshell.pricing.activity.service.ShellActivityService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;


/**
 *
 */
@Controller("shellActivity")
@RequestMapping(FebsConstant.VIEW_PREFIX + "activity")
public class ViewController extends BaseController{

		@Autowired
		private ShellActivityService shellActivityService;



	    @GetMapping("shellActivity")
	    @RequiresPermissions("activity:shellActivity:view")
	    public String shellActivity() {
	        return FebsUtil.view("activity/activity");
	    }

		@GetMapping("shellActivityNew")
		@RequiresPermissions("activity:shellActivityNew:view")
		public String shellActivityNew() {
		return FebsUtil.view("activity/activityNew");
	}


		@GetMapping("shellActivity/marketSale/{oilType}")
		@RequiresPermissions("shellActivity:marketSale")
		public String shellActivityMarketSale(@PathVariable long oilType, Model model) {
			model.addAttribute("oilType", oilType);
	    	//常规设置
			ActivityDicDetail detail=new ActivityDicDetail();
			String code1="SALETYPE";
			detail.setParentCode(code1);
			List<ActivityDicDetail> saleTypeList=shellActivityService.findDictListByParentCode(detail);
			model.addAttribute("saleTypeList",saleTypeList);
			String code2="SALEUNIT";
			detail.setParentCode(code2);
			List<ActivityDicDetail> saleUnitList=shellActivityService.findDictListByParentCode(detail);
			model.addAttribute("saleUnitList",saleUnitList);
			String code3="CUSTOMER";
			detail.setParentCode(code3);
			List<ActivityDicDetail> consumerList=shellActivityService.findDictListByParentCode(detail);
			model.addAttribute("consumerList",consumerList);
			//促销活动设置（自营0和竞争对手1）
			ShellActivityDic dic=new ShellActivityDic();
			dic.setType("0");
			List<ShellActivityDic> zyActivityList=shellActivityService.findShellActivityByType(dic);
			model.addAttribute("zyActivityList",zyActivityList);
			dic.setType("1");
			List<ShellActivityDic> jzActivityList=shellActivityService.findShellActivityByType(dic);
			model.addAttribute("jzActivityList",jzActivityList);
	    	return FebsUtil.view("activity/marketSale");
		}

		@GetMapping("shellActivityDic")
		@RequiresPermissions("activity:shellActivityDic:view")
		public String shellActivityDic() {
			return FebsUtil.view("activity/dict");
		}

		@GetMapping("shellActivityDic/add/{isUpdate}")
		@RequiresPermissions("shellActivityDic:add")
		public String shellActivityDicAdd(@PathVariable long isUpdate, Model model) {
			model.addAttribute("isUpdate", isUpdate);
	    	return FebsUtil.view("activity/dictAdd");
		}

		@GetMapping("shellActivityDic/dictDetail/{dictId}")
		@RequiresPermissions("shellActivityDic:dictDetail")
		public String shellActivityDicDetail(@PathVariable long dictId, Model model) {
			model.addAttribute("dictId", dictId);
			ActivityDic activityDic=shellActivityService.findDicById(dictId);
			model.addAttribute("activityDic", activityDic);
			return FebsUtil.view("activity/dictDetail");
		}

		@GetMapping("shellActivityDetailDic/add/{dictId}")
		@RequiresPermissions("shellActivityDetailDic:add")
		public String shellActivityDicAddDetail(@PathVariable long dictId, Model model) {
			model.addAttribute("dictId", dictId);
			return FebsUtil.view("activity/dictDetailAdd");
		}
//		@GetMapping("shellActivityDic/update/{dictId}")
//		@RequiresPermissions("shellActivityDic:update")
//		public String shellActivityDicUpdate(@NotBlank(message = "{required}") @PathVariable Long dictId, Model model) {
//			ShellActivityDic shellActivityDic=shellActivityService.findActivityDetailById(dictId);
//			model.addAttribute("shellActivityDic", shellActivityDic);
//			return FebsUtil.view("activity/");
//		}



		//促销活设置
		@GetMapping("shellActivityDict")
		@RequiresPermissions("activity:shellActivityDict:view")
		public String shellActivityDict() {
			return FebsUtil.view("activity/shellActivity");
		}

      	//促销活动-新增
        @GetMapping("shellActivityDict/add/{isUpdate}")
        @RequiresPermissions("shellActivityDict:add")
        public String shellActivityDictAdd(@PathVariable long isUpdate, Model model) {
			model.addAttribute("isUpdate", isUpdate);
	    	return FebsUtil.view("activity/shellActivityAdd");
        }

       	//促销活动-修改
		@GetMapping("shellActivityDict/update")
		@RequiresPermissions("shellActivityDict:update")
		public String shellActivityDictUpdate(@PathVariable long data, Model model) {
	    	return FebsUtil.view("activity/shellActivityUpdate");
		}

//		活动内容
		@GetMapping("shellActivityContent/{acId}")
//		@RequiresPermissions("activity:shellActivityContent:view")
		public String shellActivityContent(@PathVariable long acId, Model model) {
			model.addAttribute("acId", acId);
			return FebsUtil.view("activity/activityContent");
		}

//		活动申请
		@GetMapping("shellActivityApply")
		@RequiresPermissions("activity:shellActivityApply:view")
		public String shellActivityApply() {
			return FebsUtil.view("activity/activityApply");
		}
//		活动申请-新增
		@GetMapping("shellActivityApply/add")
		@RequiresPermissions("shellActivityApply:add")
		public String shellActivityApplyAdd() {
			return FebsUtil.view("activity/applyAdd");
		}
//		价值评估
		@GetMapping("shellStatement")
		@RequiresPermissions("activity:shellStatement:view")
		public String shellStatement() {
			return FebsUtil.view("activity/evaluationValue");
		}
//		价值评估-新增
		@GetMapping("shellStatement/add/{isUpdate}")
		@RequiresPermissions("shellStatement:add")
		public String shellStatementAdd(@PathVariable long isUpdate, Model model) {

			model.addAttribute("isUpdate", isUpdate);
			return FebsUtil.view("activity/evaluationValueAdd");
		}
//		价值评估-评分说明
		@GetMapping("shellStatement/info")
		@RequiresPermissions("shellStatement:info")
		public String shellStatementInfo() {
			return FebsUtil.view("activity/evaluationValueInfo");
		}
//		壳牌地图
		@GetMapping("shellMap")
//		@RequiresPermissions("activity:shellMap:view")
		public String shellMap() {
			return FebsUtil.view("activity/shellMap");
		}
}
