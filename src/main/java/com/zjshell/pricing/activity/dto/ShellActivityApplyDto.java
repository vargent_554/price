package com.zjshell.pricing.activity.dto;


import com.zjshell.pricing.activity.entity.ShellActivityApply;
import com.zjshell.pricing.activity.entity.ShellActivityApplyGift;
import com.zjshell.pricing.activity.entity.ShellActivityApplyRepurchase;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ShellActivityApplyDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String applyId;

    private ShellActivityApply shellActivityApply;

    private List<ShellActivityApplyGift> shellActivityApplyGiftList;

    private List<ShellActivityApplyRepurchase> shellActivityApplyRepurchaseList;


    private List<ShellActivityApplyOilDto> shellActivityApplyOilDtoList;


    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    public List<ShellActivityApplyGift> getShellActivityApplyGiftList() {
        return shellActivityApplyGiftList;
    }

    public void setShellActivityApplyGiftList(List<ShellActivityApplyGift> shellActivityApplyGiftList) {
        this.shellActivityApplyGiftList = shellActivityApplyGiftList;
    }

    public List<ShellActivityApplyRepurchase> getShellActivityApplyRepurchaseList() {
        return shellActivityApplyRepurchaseList;
    }

    public void setShellActivityApplyRepurchaseList(List<ShellActivityApplyRepurchase> shellActivityApplyRepurchaseList) {
        this.shellActivityApplyRepurchaseList = shellActivityApplyRepurchaseList;
    }

    public ShellActivityApply getShellActivityApply() {
        return shellActivityApply;
    }

    public void setShellActivityApply(ShellActivityApply shellActivityApply) {
        this.shellActivityApply = shellActivityApply;
    }

    public List<ShellActivityApplyOilDto> getShellActivityApplyOilDtoList() {
        return shellActivityApplyOilDtoList;
    }

    public void setShellActivityApplyOilDtoList(List<ShellActivityApplyOilDto> shellActivityApplyOilDtoList) {
        this.shellActivityApplyOilDtoList = shellActivityApplyOilDtoList;
    }
}
