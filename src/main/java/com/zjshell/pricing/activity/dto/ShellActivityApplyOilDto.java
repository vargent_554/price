package com.zjshell.pricing.activity.dto;

import com.zjshell.pricing.activity.entity.*;
import com.zjshell.pricing.activity.entity.ShellActivityApplyCoupon;
import com.zjshell.pricing.activity.entity.ShellActivityApplyOil;

import java.io.Serializable;
import java.util.List;

public class ShellActivityApplyOilDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private ShellActivityApplyOil shellActivityApplyOil;

    private List<ShellActivityApplyCar> shellActivityApplyCarList;

    private List<ShellActivityApplyCoupon> shellActivityApplyCouponList;


    public ShellActivityApplyOil getShellActivityApplyOil() {
        return shellActivityApplyOil;
    }

    public void setShellActivityApplyOil(ShellActivityApplyOil shellActivityApplyOil) {
        this.shellActivityApplyOil = shellActivityApplyOil;
    }

    public List<ShellActivityApplyCar> getShellActivityApplyCarList() {
        return shellActivityApplyCarList;
    }

    public void setShellActivityApplyCarList(List<ShellActivityApplyCar> shellActivityApplyCarList) {
        this.shellActivityApplyCarList = shellActivityApplyCarList;
    }

    public List<ShellActivityApplyCoupon> getShellActivityApplyCouponList() {
        return shellActivityApplyCouponList;
    }

    public void setShellActivityApplyCouponList(List<ShellActivityApplyCoupon> shellActivityApplyCouponList) {
        this.shellActivityApplyCouponList = shellActivityApplyCouponList;
    }


}
