package com.zjshell.pricing.activity.dto;

import com.zjshell.pricing.activity.entity.ShellActivityOil;
import com.zjshell.pricing.activity.entity.ShellActivityOilDetail;
import com.zjshell.pricing.activity.entity.ShellActivityOil;
import com.zjshell.pricing.activity.entity.ShellActivityOilDetail;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ShellActivityOilDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String icon;
    private String address;
    private Date oilDate;
    private List<ShellActivityOil> shellActivityOilList;
    private List<ShellActivityOilDetail> shellActivityOilDetailList;
    private String liveUrl;
    private String  id;
    private String shellId;
    private String type;
    private String shellName;
    private String shellCode;
    private String remark;
    private String oilCode;
    private Date actDate;

    public Date getActDate() {
        return actDate;
    }

    public void setActDate(Date actDate) {
        this.actDate = actDate;
    }

    public List<ShellActivityOilDetail> getShellActivityOilDetailList() {
        return shellActivityOilDetailList;
    }

    public void setShellActivityOilDetailList(List<ShellActivityOilDetail> shellActivityOilDetailList) {
        this.shellActivityOilDetailList = shellActivityOilDetailList;
    }

    public String getOilCode() {
        return oilCode;
    }

    public void setOilCode(String oilCode) {
        this.oilCode = oilCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getShellId() {
        return shellId;
    }

    public void setShellId(String shellId) {
        this.shellId = shellId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getShellName() {
        return shellName;
    }

    public void setShellName(String shellName) {
        this.shellName = shellName;
    }

    public String getShellCode() {
        return shellCode;
    }

    public void setShellCode(String shellCode) {
        this.shellCode = shellCode;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getOilDate() {
        return oilDate;
    }

    public void setOilDate(Date oilDate) {
        this.oilDate = oilDate;
    }

    public List<ShellActivityOil> getShellActivityOilList() {
        return shellActivityOilList;
    }

    public void setShellActivityOilList(List<ShellActivityOil> shellActivityOilList) {
        this.shellActivityOilList = shellActivityOilList;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
