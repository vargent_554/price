package com.zjshell.pricing.activity.dto;

import com.zjshell.pricing.activity.entity.ShellStatement;
import com.zjshell.pricing.activity.entity.ShellStatementCompetitor;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;
import com.zjshell.pricing.activity.entity.ShellStatement;
import com.zjshell.pricing.activity.entity.ShellStatementCompetitor;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class ShellStatementDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String shellId;

    private String id;

    private ShellStatement shellStatement;

    private List<ShellStatementCompetitor> shellStatementCompetitorList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ShellStatement getShellStatement() {
        return shellStatement;
    }

    public String getShellId() {
        return shellId;
    }

    public void setShellId(String shellId) {
        this.shellId = shellId;
    }

    public void setShellStatement(ShellStatement shellStatement) {
        this.shellStatement = shellStatement;
    }

    public List<ShellStatementCompetitor> getShellStatementCompetitorList() {
        return shellStatementCompetitorList;
    }

    public void setShellStatementCompetitorList(List<ShellStatementCompetitor> shellStatementCompetitorList) {
        this.shellStatementCompetitorList = shellStatementCompetitorList;
    }
}
