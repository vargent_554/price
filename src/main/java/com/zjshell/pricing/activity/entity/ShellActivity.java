package com.zjshell.pricing.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("t_shell_activity")
@Excel("壳牌活动和油站一对一")
public class ShellActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("shell_id")
    private Long shellId;

    @TableField("type")
    private String type;

    @TableField("shell_code")
    private String shellCode;

    @TableField("shell_name")
    private String shellName;

    @TableField("img_url_price")
    private String imgUrlPrice;

    @TableField("img_url_live")
    private String imgUrlLive;

    @TableField("remark")
    private String remark;

    @TableField("price_date")
    private Date priceDate;

    @TableField("activity_date")
    private Date activityDate;

    @TableField("create_time")
    private Date createTime;

    @TableField("modify_time")
    private Date modifyTime;

    public Date getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(Date priceDate) {
        this.priceDate = priceDate;
    }

    public Date getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(Date activityDate) {
        this.activityDate = activityDate;
    }

    public String getImgUrlLive() {
        return imgUrlLive;
    }

    public void setImgUrlLive(String imgUrlLive) {
        this.imgUrlLive = imgUrlLive;
    }

    public String getImgUrlPrice() {
        return imgUrlPrice;
    }

    public void setImgUrlPrice(String imgUrlPrice) {
        this.imgUrlPrice = imgUrlPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShellId() {
        return shellId;
    }

    public void setShellId(Long shellId) {
        this.shellId = shellId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getShellCode() {
        return shellCode;
    }

    public void setShellCode(String shellCode) {
        this.shellCode = shellCode;
    }

    public String getShellName() {
        return shellName;
    }

    public void setShellName(String shellName) {
        this.shellName = shellName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
