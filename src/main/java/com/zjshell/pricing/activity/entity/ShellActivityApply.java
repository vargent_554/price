package com.zjshell.pricing.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("t_shell_activity_apply")
@Excel("活动申请")
public class ShellActivityApply implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("shell_id")
    private String shellId;

    @TableField("shell_name")
    private String shellName;

    @TableField("activity_name")
    private String activityName;

    @TableField("begin_time")
    private Date beginTime;

    @TableField("end_time")
    private Date endTime;

    @TableField("activity_days")
    private Integer activityDays;

    @TableField("activity_before_days")
    private Integer activityBeforeDays;

    @TableField("activity_after_days")
    private Integer activityAfterDays;

    @TableField("activity_all_days")
    private Integer activityAllDays;

    @TableField("apply_id")
    private Long applyId;

    @TableField("apply_name")
    private String applyName;

    @TableField("status")
    private String status;

    @TableField("gift_total")
    private String giftTotal;

    @TableField("remark")
    private String remark;

    @TableField("add_sale_price")
    private String addSalePrice;

    @TableField("add_input_price")
    private String addInputPrice;

    @TableField("base_total_car")
    private String baseTotalCar;

    @TableField("base_total_sale")
    private String baseTotalSale;

    @TableField("activity_total_car")
    private String activityTotalCar;

    @TableField("activity_total_sale")
    private String activityTotalSale;

    @TableField("kpi_sale_kl")
    private String kpiSaleKl;

    @TableField("kpi_92_kl")
    private String kpi92Kl;

    @TableField("kpi_0_kl")
    private String kpi0Kl;

    @TableField("kpi_95_kl")
    private String kpi95Kl;

    @TableField("kpi_sale_c3")
    private String kpiSaleC3;

    @TableField("kpi_92_c3")
    private String kpi92C3;

    @TableField("kpi_0_c3")
    private String kpi0C3;

    @TableField("kpi_95_c3")
    private String kpi95C3;

    @TableField("nfr")
    private String nfr;

    @TableField("direct_down")
    private String directDown;

    @TableField("coupons")
    private String coupons;

    @TableField("repurchase")
    private String repurchase;

    @TableField("gifts")
    private String gifts;

    @TableField("materiel_price")
    private String materielPrice;

    @TableField("net_profit")
    private String netProfit;

    @TableField("total_cost")
    private String totalCost;

    @TableField("roi")
    private String roi;

    @TableField("create_time")
    private Date createTime;

    @TableField("modify_time")
    private Date modifyTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getShellId() {
        return shellId;
    }

    public void setShellId(String shellId) {
        this.shellId = shellId;
    }

    public String getShellName() {
        return shellName;
    }

    public void setShellName(String shellName) {
        this.shellName = shellName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getActivityDays() {
        return activityDays;
    }

    public void setActivityDays(Integer activityDays) {
        this.activityDays = activityDays;
    }

    public Integer getActivityBeforeDays() {
        return activityBeforeDays;
    }

    public void setActivityBeforeDays(Integer activityBeforeDays) {
        this.activityBeforeDays = activityBeforeDays;
    }

    public Integer getActivityAfterDays() {
        return activityAfterDays;
    }

    public void setActivityAfterDays(Integer activityAfterDays) {
        this.activityAfterDays = activityAfterDays;
    }

    public Integer getActivityAllDays() {
        return activityAllDays;
    }

    public void setActivityAllDays(Integer activityAllDays) {
        this.activityAllDays = activityAllDays;
    }

    public Long getApplyId() {
        return applyId;
    }

    public void setApplyId(Long applyId) {
        this.applyId = applyId;
    }

    public String getApplyName() {
        return applyName;
    }

    public void setApplyName(String applyName) {
        this.applyName = applyName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGiftTotal() {
        return giftTotal;
    }

    public void setGiftTotal(String giftTotal) {
        this.giftTotal = giftTotal;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAddSalePrice() {
        return addSalePrice;
    }

    public void setAddSalePrice(String addSalePrice) {
        this.addSalePrice = addSalePrice;
    }

    public String getAddInputPrice() {
        return addInputPrice;
    }

    public void setAddInputPrice(String addInputPrice) {
        this.addInputPrice = addInputPrice;
    }

    public String getBaseTotalCar() {
        return baseTotalCar;
    }

    public void setBaseTotalCar(String baseTotalCar) {
        this.baseTotalCar = baseTotalCar;
    }

    public String getBaseTotalSale() {
        return baseTotalSale;
    }

    public void setBaseTotalSale(String baseTotalSale) {
        this.baseTotalSale = baseTotalSale;
    }

    public String getActivityTotalCar() {
        return activityTotalCar;
    }

    public void setActivityTotalCar(String activityTotalCar) {
        this.activityTotalCar = activityTotalCar;
    }

    public String getActivityTotalSale() {
        return activityTotalSale;
    }

    public void setActivityTotalSale(String activityTotalSale) {
        this.activityTotalSale = activityTotalSale;
    }

    public String getKpiSaleKl() {
        return kpiSaleKl;
    }

    public void setKpiSaleKl(String kpiSaleKl) {
        this.kpiSaleKl = kpiSaleKl;
    }

    public String getKpi92Kl() {
        return kpi92Kl;
    }

    public void setKpi92Kl(String kpi92Kl) {
        this.kpi92Kl = kpi92Kl;
    }

    public String getKpi0Kl() {
        return kpi0Kl;
    }

    public void setKpi0Kl(String kpi0Kl) {
        this.kpi0Kl = kpi0Kl;
    }

    public String getKpi95Kl() {
        return kpi95Kl;
    }

    public void setKpi95Kl(String kpi95Kl) {
        this.kpi95Kl = kpi95Kl;
    }

    public String getKpiSaleC3() {
        return kpiSaleC3;
    }

    public void setKpiSaleC3(String kpiSaleC3) {
        this.kpiSaleC3 = kpiSaleC3;
    }

    public String getKpi92C3() {
        return kpi92C3;
    }

    public void setKpi92C3(String kpi92C3) {
        this.kpi92C3 = kpi92C3;
    }

    public String getKpi0C3() {
        return kpi0C3;
    }

    public void setKpi0C3(String kpi0C3) {
        this.kpi0C3 = kpi0C3;
    }

    public String getKpi95C3() {
        return kpi95C3;
    }

    public void setKpi95C3(String kpi95C3) {
        this.kpi95C3 = kpi95C3;
    }

    public String getNfr() {
        return nfr;
    }

    public void setNfr(String nfr) {
        this.nfr = nfr;
    }

    public String getDirectDown() {
        return directDown;
    }

    public void setDirectDown(String directDown) {
        this.directDown = directDown;
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }

    public String getRepurchase() {
        return repurchase;
    }

    public void setRepurchase(String repurchase) {
        this.repurchase = repurchase;
    }

    public String getGifts() {
        return gifts;
    }

    public void setGifts(String gifts) {
        this.gifts = gifts;
    }

    public String getMaterielPrice() {
        return materielPrice;
    }

    public void setMaterielPrice(String materielPrice) {
        this.materielPrice = materielPrice;
    }

    public String getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(String netProfit) {
        this.netProfit = netProfit;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getRoi() {
        return roi;
    }

    public void setRoi(String roi) {
        this.roi = roi;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
