package com.zjshell.pricing.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("t_shell_activity_apply_oil")
@Excel("活动申请油品")
public class ShellActivityApplyOil implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("activity_id")
    private Long activityId;

    @TableField("activity_name")
    private String activityName;

    @TableField("oil_code")
    private String oilCode;

    @TableField("purchase_price")
    private String purchasePrice;


    @TableField("listed_price")
    private String listedPrice;

    @TableField("sale_per")
    private String salePer;

    @TableField("profit_per")
    private String profitPer;

    @TableField("material_price")
    private String materialPrice;

    @TableField("expect_per_sale")
    private String expectPerSale;

    @TableField("oil_amount_per")
    private String oilAmountPer;

    @TableField("actual_per_sale")
    private String actualPerSale;

    @TableField("activity_per")
    private String activityPer;

    @TableField("base_perday_car")
    private Integer basePerdayCar;

    @TableField("base_perl_day")
    private String basePerlDay;

    @TableField("base_persale_amount")
    private String basePersaleAmount;

    @TableField("base_perc3_amount")
    private String basePerc3Amount;

    @TableField("activity_perday_car")
    private Integer activityPerdayCar;

    @TableField("activity_perl_day")
    private String activityPerlDay;

    @TableField("activity_persale_amount")
    private String activityPersaleAmount;

    @TableField("activity_perc3_amount")
    private String activityPerc3Amount;

    @TableField("remark")
    private String remark;

    @TableField("create_time")
    private Date createTime;

    @TableField("modify_time")
    private Date modifyTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getOilCode() {
        return oilCode;
    }

    public void setOilCode(String oilCode) {
        this.oilCode = oilCode;
    }

    public String getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(String purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getListedPrice() {
        return listedPrice;
    }

    public void setListedPrice(String listedPrice) {
        this.listedPrice = listedPrice;
    }

    public String getSalePer() {
        return salePer;
    }

    public void setSalePer(String salePer) {
        this.salePer = salePer;
    }

    public String getProfitPer() {
        return profitPer;
    }

    public void setProfitPer(String profitPer) {
        this.profitPer = profitPer;
    }

    public String getMaterialPrice() {
        return materialPrice;
    }

    public void setMaterialPrice(String materialPrice) {
        this.materialPrice = materialPrice;
    }

    public String getExpectPerSale() {
        return expectPerSale;
    }

    public void setExpectPerSale(String expectPerSale) {
        this.expectPerSale = expectPerSale;
    }

    public String getOilAmountPer() {
        return oilAmountPer;
    }

    public void setOilAmountPer(String oilAmountPer) {
        this.oilAmountPer = oilAmountPer;
    }

    public String getActualPerSale() {
        return actualPerSale;
    }

    public void setActualPerSale(String actualPerSale) {
        this.actualPerSale = actualPerSale;
    }

    public String getActivityPer() {
        return activityPer;
    }

    public void setActivityPer(String activityPer) {
        this.activityPer = activityPer;
    }

    public Integer getBasePerdayCar() {
        return basePerdayCar;
    }

    public void setBasePerdayCar(Integer basePerdayCar) {
        this.basePerdayCar = basePerdayCar;
    }

    public String getBasePerlDay() {
        return basePerlDay;
    }

    public void setBasePerlDay(String basePerlDay) {
        this.basePerlDay = basePerlDay;
    }

    public String getBasePersaleAmount() {
        return basePersaleAmount;
    }

    public void setBasePersaleAmount(String basePersaleAmount) {
        this.basePersaleAmount = basePersaleAmount;
    }

    public String getBasePerc3Amount() {
        return basePerc3Amount;
    }

    public void setBasePerc3Amount(String basePerc3Amount) {
        this.basePerc3Amount = basePerc3Amount;
    }

    public Integer getActivityPerdayCar() {
        return activityPerdayCar;
    }

    public void setActivityPerdayCar(Integer activityPerdayCar) {
        this.activityPerdayCar = activityPerdayCar;
    }

    public String getActivityPerlDay() {
        return activityPerlDay;
    }

    public void setActivityPerlDay(String activityPerlDay) {
        this.activityPerlDay = activityPerlDay;
    }

    public String getActivityPersaleAmount() {
        return activityPersaleAmount;
    }

    public void setActivityPersaleAmount(String activityPersaleAmount) {
        this.activityPersaleAmount = activityPersaleAmount;
    }

    public String getActivityPerc3Amount() {
        return activityPerc3Amount;
    }

    public void setActivityPerc3Amount(String activityPerc3Amount) {
        this.activityPerc3Amount = activityPerc3Amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
