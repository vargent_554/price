package com.zjshell.pricing.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("t_shell_statement")
@Excel("油站价值评估表")
public class ShellStatement implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField("shell_id")
    private Long shellId;

    @TableField("shell_name")
    private String shellName;

    @TableField("evaluator")
    private String evaluator;

    @TableField("opt_date")
    private Date optDate;

    @TableField("site_code")
    private String siteCode;

    @TableField("site_address")
    private String siteAddress;

    @TableField("site_city")
    private String siteCity;

    @TableField("district_manager")
    private String districtManager;

    @TableField("territory_manager")
    private String territoryManager;

    @TableField("site_primarily")
    private String sitePrimarily;

    @TableField("area_primarily")
    private String areaPrimarily;

    @TableField("card_prepaid")
    private String cardPrepaid;

    @TableField("loyalty_customer")
    private String loyaltyCustomer;

    @TableField("position_number")
    private String positionNumber;

    @TableField("price_monolith")
    private String priceMonolith;

    @TableField("service")
    private String service;

    @TableField("site_facility")
    private String siteFacility;

    @TableField("total_in")
    private String totalIn;

    @TableField("ago_mogas")
    private String agoMogas;

    @TableField("real_estate")
    private String realEstate;

    @TableField("price")
    private String price;

    @TableField("traffic_flow")
    private String trafficFlow;

    @TableField("price_communication")
    private String priceCommunication;

    @TableField("site_format")
    private String siteFormat;

    @TableField("retail_operation")
    private String retailOperation;

    @TableField("brand_value")
    private String brandValue;

    @TableField("product_quality")
    private String productQuality;

    @TableField("total_record")
    private String totalRecord;

    @TableField("high_record")
    private String highRecord;

    @TableField("potential_volume")
    private String potentialVolume;

    @TableField("peryear_actualvolume")
    private String peryearActualvolume;

    @TableField("mogas_mg")
    private String mogasMg;

    @TableField("diesel_mg")
    private String dieselMg;

    @TableField("special_pricing")
    private String specialPricing;

    @TableField("market_strategy")
    private String marketStrategy;

    @TableField("tactic_employ")
    private String tacticEmploy;

    @TableField("reason")
    private String reason;

    @TableField("attempt_location")
    private String attemptLocation;

    @TableField("create_time")
    private Date createTime;

    @TableField("modify_time")
    private Date modifyTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShellId() {
        return shellId;
    }

    public void setShellId(Long shellId) {
        this.shellId = shellId;
    }

    public String getShellName() {
        return shellName;
    }

    public void setShellName(String shellName) {
        this.shellName = shellName;
    }

    public String getEvaluator() {
        return evaluator;
    }

    public void setEvaluator(String evaluator) {
        this.evaluator = evaluator;
    }

    public Date getOptDate() {
        return optDate;
    }

    public void setOptDate(Date optDate) {
        this.optDate = optDate;
    }

    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }

    public String getSiteAddress() {
        return siteAddress;
    }

    public void setSiteAddress(String siteAddress) {
        this.siteAddress = siteAddress;
    }

    public String getSiteCity() {
        return siteCity;
    }

    public void setSiteCity(String siteCity) {
        this.siteCity = siteCity;
    }

    public String getDistrictManager() {
        return districtManager;
    }

    public void setDistrictManager(String districtManager) {
        this.districtManager = districtManager;
    }

    public String getTerritoryManager() {
        return territoryManager;
    }

    public void setTerritoryManager(String territoryManager) {
        this.territoryManager = territoryManager;
    }

    public String getSitePrimarily() {
        return sitePrimarily;
    }

    public void setSitePrimarily(String sitePrimarily) {
        this.sitePrimarily = sitePrimarily;
    }

    public String getAreaPrimarily() {
        return areaPrimarily;
    }

    public void setAreaPrimarily(String areaPrimarily) {
        this.areaPrimarily = areaPrimarily;
    }

    public String getCardPrepaid() {
        return cardPrepaid;
    }

    public void setCardPrepaid(String cardPrepaid) {
        this.cardPrepaid = cardPrepaid;
    }

    public String getLoyaltyCustomer() {
        return loyaltyCustomer;
    }

    public void setLoyaltyCustomer(String loyaltyCustomer) {
        this.loyaltyCustomer = loyaltyCustomer;
    }

    public String getPositionNumber() {
        return positionNumber;
    }

    public void setPositionNumber(String positionNumber) {
        this.positionNumber = positionNumber;
    }

    public String getPriceMonolith() {
        return priceMonolith;
    }

    public void setPriceMonolith(String priceMonolith) {
        this.priceMonolith = priceMonolith;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getSiteFacility() {
        return siteFacility;
    }

    public void setSiteFacility(String siteFacility) {
        this.siteFacility = siteFacility;
    }

    public String getTotalIn() {
        return totalIn;
    }

    public void setTotalIn(String totalIn) {
        this.totalIn = totalIn;
    }

    public String getAgoMogas() {
        return agoMogas;
    }

    public void setAgoMogas(String agoMogas) {
        this.agoMogas = agoMogas;
    }

    public String getRealEstate() {
        return realEstate;
    }

    public void setRealEstate(String realEstate) {
        this.realEstate = realEstate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTrafficFlow() {
        return trafficFlow;
    }

    public void setTrafficFlow(String trafficFlow) {
        this.trafficFlow = trafficFlow;
    }

    public String getPriceCommunication() {
        return priceCommunication;
    }

    public void setPriceCommunication(String priceCommunication) {
        this.priceCommunication = priceCommunication;
    }

    public String getSiteFormat() {
        return siteFormat;
    }

    public void setSiteFormat(String siteFormat) {
        this.siteFormat = siteFormat;
    }

    public String getRetailOperation() {
        return retailOperation;
    }

    public void setRetailOperation(String retailOperation) {
        this.retailOperation = retailOperation;
    }

    public String getBrandValue() {
        return brandValue;
    }

    public void setBrandValue(String brandValue) {
        this.brandValue = brandValue;
    }

    public String getProductQuality() {
        return productQuality;
    }

    public void setProductQuality(String productQuality) {
        this.productQuality = productQuality;
    }

    public String getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(String totalRecord) {
        this.totalRecord = totalRecord;
    }

    public String getHighRecord() {
        return highRecord;
    }

    public void setHighRecord(String highRecord) {
        this.highRecord = highRecord;
    }

    public String getPotentialVolume() {
        return potentialVolume;
    }

    public void setPotentialVolume(String potentialVolume) {
        this.potentialVolume = potentialVolume;
    }

    public String getPeryearActualvolume() {
        return peryearActualvolume;
    }

    public void setPeryearActualvolume(String peryearActualvolume) {
        this.peryearActualvolume = peryearActualvolume;
    }

    public String getMogasMg() {
        return mogasMg;
    }

    public void setMogasMg(String mogasMg) {
        this.mogasMg = mogasMg;
    }

    public String getDieselMg() {
        return dieselMg;
    }

    public void setDieselMg(String dieselMg) {
        this.dieselMg = dieselMg;
    }

    public String getSpecialPricing() {
        return specialPricing;
    }

    public void setSpecialPricing(String specialPricing) {
        this.specialPricing = specialPricing;
    }

    public String getMarketStrategy() {
        return marketStrategy;
    }

    public void setMarketStrategy(String marketStrategy) {
        this.marketStrategy = marketStrategy;
    }

    public String getTacticEmploy() {
        return tacticEmploy;
    }

    public void setTacticEmploy(String tacticEmploy) {
        this.tacticEmploy = tacticEmploy;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getAttemptLocation() {
        return attemptLocation;
    }

    public void setAttemptLocation(String attemptLocation) {
        this.attemptLocation = attemptLocation;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
