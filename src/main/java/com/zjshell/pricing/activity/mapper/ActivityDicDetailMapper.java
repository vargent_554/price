package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ActivityDicDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjshell.pricing.activity.entity.ActivityDicDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityDicDetailMapper extends BaseMapper<ActivityDicDetail> {

    IPage<ActivityDicDetail> findActivityDictDetailList(Page<ActivityDicDetail> page, @Param("activityDicDetail") ActivityDicDetail activityDicDetail);

    List<ActivityDicDetail> findDictListByParentCode(@Param("activityDicDetail") ActivityDicDetail activityDicDetail);
}
