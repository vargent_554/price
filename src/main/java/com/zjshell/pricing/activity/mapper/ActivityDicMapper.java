package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ActivityDic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectKey;

public interface ActivityDicMapper extends BaseMapper<ActivityDic> {

    IPage<ActivityDic> findActivityDictList(Page page, @Param("activityDic") ActivityDic activityDic);



//    @Insert("INSERT INTO ...")
//    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//    @SelectKey(statement = "select last_insert_id()", keyProperty = "id", before = false, resultType = long.class)
//    <insert id="insertSelective" parameterType="com.jincou.dlo.TLivePressOriginDO" useGeneratedKeys="true" keyProperty="autoId">
//    void insertBackId(ActivityDic activityDic);
}
