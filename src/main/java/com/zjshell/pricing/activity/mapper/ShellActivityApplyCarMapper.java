package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ShellActivityApply;
import com.zjshell.pricing.activity.entity.ShellActivityApplyCar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellActivityApplyCarMapper extends BaseMapper<ShellActivityApplyCar> {

    //查询list
    List<ShellActivityApplyCar> findList(@Param("shellActivityApplyCar") ShellActivityApplyCar shellActivityApplyCar);
}
