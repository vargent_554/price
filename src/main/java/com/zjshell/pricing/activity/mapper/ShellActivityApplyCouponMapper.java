package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ShellActivityApply;
import com.zjshell.pricing.activity.entity.ShellActivityApplyCar;
import com.zjshell.pricing.activity.entity.ShellActivityApplyCoupon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellActivityApplyCouponMapper extends BaseMapper<ShellActivityApplyCoupon> {


    List<ShellActivityApplyCoupon> findList(@Param("shellActivityApplyCoupon") ShellActivityApplyCoupon shellActivityApplyCoupon);
}
