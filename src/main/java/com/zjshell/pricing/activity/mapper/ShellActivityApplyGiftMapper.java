package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ShellActivityApply;
import com.zjshell.pricing.activity.entity.ShellActivityApplyGift;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellActivityApplyGiftMapper extends BaseMapper<ShellActivityApplyGift> {


    List<ShellActivityApplyGift> findList(@Param("shellActivityApplyGift") ShellActivityApplyGift shellActivityApplyGift);
}
