package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ShellActivity;
import com.zjshell.pricing.activity.entity.ShellActivityApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellActivityApplyMapper extends BaseMapper<ShellActivityApply> {

    //分页查询
    IPage<ShellActivityApply> findshellActivityApplyList(Page<ShellActivityApply> page, @Param("shellActivityApply") ShellActivityApply shellActivityApply);
}
