package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ShellActivityApply;
import com.zjshell.pricing.activity.entity.ShellActivityApplyOil;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellActivityApplyOilMapper extends BaseMapper<ShellActivityApplyOil> {

    //查询油数据列表
    List<ShellActivityApplyOil> findByParentId(@Param("shellActivityApplyOil") ShellActivityApplyOil shellActivityApplyOil);
}
