package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ShellActivityApply;
import com.zjshell.pricing.activity.entity.ShellActivityApplyGift;
import com.zjshell.pricing.activity.entity.ShellActivityApplyRepurchase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellActivityApplyRepurchaseMapper extends BaseMapper<ShellActivityApplyRepurchase> {


    List<ShellActivityApplyRepurchase> findList(@Param("shellActivityApplyRepurchase") ShellActivityApplyRepurchase shellActivityApplyRepurchase);
}
