package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ActivityDic;
import com.zjshell.pricing.activity.entity.ShellActivityDic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellActivityDicMapper extends BaseMapper<ShellActivityDic> {


    IPage<ShellActivityDic> findShellActivityDictList(Page<ShellActivityDic> page, @Param("shellActivityDic") ShellActivityDic shellActivityDic);

    List<ShellActivityDic> findDicList(@Param("shellActivityDic") ShellActivityDic shellActivityDic);
}
