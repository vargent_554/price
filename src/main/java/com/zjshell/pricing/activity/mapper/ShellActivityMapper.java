package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ShellActivity;
import com.zjshell.pricing.activity.entity.ShellActivityDic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellActivityMapper extends BaseMapper<ShellActivity> {

    //通过油站id查询活动
    List<ShellActivity> selectBySiteId(@Param("siteId") Long siteId);

}
