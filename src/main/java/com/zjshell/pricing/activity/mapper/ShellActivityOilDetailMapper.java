package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ShellActivityOil;
import com.zjshell.pricing.activity.entity.ShellActivityOilDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellActivityOilDetailMapper extends BaseMapper<ShellActivityOilDetail> {

    List<ShellActivityOilDetail> findOilDetailList(@Param("shellActivityOilDetail") ShellActivityOilDetail shellActivityOilDetail);
}
