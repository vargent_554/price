package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ShellActivity;
import com.zjshell.pricing.activity.entity.ShellActivityOil;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellActivityOilMapper extends BaseMapper<ShellActivityOil> {

    //查询价格调查最新数据
    List<ShellActivityOil> findOilList(@Param("shellActivityOil") ShellActivityOil shellActivityOil);
}
