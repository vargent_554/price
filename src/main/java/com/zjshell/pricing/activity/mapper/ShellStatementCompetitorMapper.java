package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ShellStatement;
import com.zjshell.pricing.activity.entity.ShellStatementCompetitor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface ShellStatementCompetitorMapper extends BaseMapper<ShellStatementCompetitor> {

}
