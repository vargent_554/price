package com.zjshell.pricing.activity.mapper;

import com.zjshell.pricing.activity.entity.ActivityDic;
import com.zjshell.pricing.activity.entity.ShellStatement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

public interface ShellStatementMapper extends BaseMapper<ShellStatement> {

    IPage<ShellStatement> findShellStatementList(Page page, @Param("shellStatement") ShellStatement shellStatement);
    //查询最后一条数据
    ShellStatement findLast();
}
