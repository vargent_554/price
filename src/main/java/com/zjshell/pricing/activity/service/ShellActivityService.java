package com.zjshell.pricing.activity.service;

import com.zjshell.pricing.activity.dto.ShellActivityApplyDto;
import com.zjshell.pricing.activity.dto.ShellActivityOilDto;
import com.zjshell.pricing.activity.dto.ShellStatementDto;
import com.zjshell.pricing.activity.entity.*;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.site.entity.ShellSite;
import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

public interface ShellActivityService {

    IPage<ActivityDic> findActivityDictList(ActivityDic activityDic, QueryRequest request);

    IPage<ActivityDicDetail> findActivityDictDetailList(ActivityDicDetail activityDicDetail, QueryRequest request);
    //数据字典新增
    void doSaveShellActivityDic(ActivityDic activityDic);
    //修改活动数据字典
    void updateShellActivityDic(ActivityDic activityDic);
    //删除活动数据字典
    void deleteShellActivityDic(String[] ids);

    void deleteShellActivityDicDetail(String[] ids);

    void updateShellActivityDicDetail(ActivityDicDetail activityDicDetail);

    void doSaveShellActivityDicDetail(ActivityDicDetail activityDicDetail);
    //壳牌活动数据字典
    IPage<ShellActivityDic> findshellActivityDictList(ShellActivityDic shellActivityDic, QueryRequest request);

    void doSaveActivityDic(ShellActivityDic shellActivityDic);

    void updateActivityDic(ShellActivityDic shellActivityDic);

    void deleteActivityDic(String[] ids);
    //油站模糊匹配 名字和编码
    List<ShellSite> findShellSiteList(ShellSite shellSite);

    //活动调研查询所有油站以及竞争油站数据
    List<ShellActivityOilDto> findShellAllSiteList(String shellId,String activityType,String searchDate);
    //价格调研
    void doSaveShellActivityPrice(List<ShellActivityOilDto> shellActivityOilDtoList);
    //活动调研保存
    void doSaveShellActivityDetail(List<ShellActivityOilDto> shellActivityOilDtoList);
    //通过活动id查询详情
    ShellActivityDic findActivityDetailById(Long dictId);
    //字典id查询字典
    ActivityDic findDicById(Long dictId);
    //通过编码查询list
    List<ActivityDicDetail> findDictListByParentCode(ActivityDicDetail detail);
    //查询活动促销
    List<ShellActivityDic> findShellActivityByType(ShellActivityDic dic);
    //活动申请主体信息
    void doAddShellActivityApply(ShellActivityApply shellActivityApply);
    //活动申请详情
    void doAddShellActivityApplyDetail(ShellActivityApplyDto shellActivityApplyDto);
    //活动申请分页
    IPage<ShellActivityApply> findshellActivityApplyList(ShellActivityApply shellActivityApply, QueryRequest request);
    //详情查询
    ShellActivityApplyDto findShellActivityApplyDetailById(String id);
    //价值评估分页
    IPage<?> findShellStatementList(ShellStatement shellStatement, QueryRequest request);
    //价值评估表删除
    void deleteShellStatement(String[] ids);
    //  价值评估表详情
    ShellStatementDto findShellStatementyDetailById(String id);
    //新增价值评估表
    void doSaveShellStatement(ShellStatementDto shellStatementDto);
    //油站详情
    ShellSite findShellSiteDetail(String shellId);
    //价值评估表修改
    void updateShellStatement(ShellStatementDto shellStatementDto);

    List<ShellActivityOilDto> findShellAllSiteListA(String shellId, String activityType);
    //活动单条删除
    void doShellActivityDetaiDel(String id);
}
