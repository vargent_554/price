package com.zjshell.pricing.activity.service.impl;

import com.zjshell.pricing.activity.dto.ShellActivityApplyDto;
import com.zjshell.pricing.activity.dto.ShellActivityApplyOilDto;
import com.zjshell.pricing.activity.dto.ShellActivityOilDto;
import com.zjshell.pricing.activity.dto.ShellStatementDto;
import com.zjshell.pricing.activity.entity.*;
import com.zjshell.pricing.activity.mapper.*;
import com.zjshell.pricing.activity.service.ShellActivityService;
import com.zjshell.pricing.common.entity.FebsConstant;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.exception.FebsException;
import com.zjshell.pricing.common.utils.DateUtil;
import com.zjshell.pricing.common.utils.SortUtil;
import com.zjshell.pricing.dict.entity.DictSite;
import com.zjshell.pricing.dict.mapper.DictSiteMapper;
import com.zjshell.pricing.site.entity.CompetitorSite;
import com.zjshell.pricing.site.entity.ShellSite;
import com.zjshell.pricing.site.entity.ShellSiteCompare;
import com.zjshell.pricing.site.mapper.CompetitorSiteMapper;
import com.zjshell.pricing.site.mapper.ShellSiteCompareMapper;
import com.zjshell.pricing.site.mapper.ShellSiteMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjshell.pricing.activity.dto.ShellActivityApplyDto;
import com.zjshell.pricing.activity.dto.ShellActivityApplyOilDto;
import com.zjshell.pricing.activity.dto.ShellActivityOilDto;
import com.zjshell.pricing.activity.dto.ShellStatementDto;
import com.zjshell.pricing.activity.entity.*;
import com.zjshell.pricing.activity.mapper.*;
import com.zjshell.pricing.activity.service.ShellActivityService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.DateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ShellActivityServiceImpl extends ServiceImpl<ActivityDicMapper,ActivityDic> implements ShellActivityService {


    @Autowired
    private ActivityDicMapper activityDicMapper;
    @Autowired
    private ActivityDicDetailMapper activityDicDetailMapper;
    @Autowired
    private ShellActivityDicMapper shellActivityDicMapper;
    @Autowired
    private ShellSiteMapper shellSiteMapper;
    @Autowired
    private ShellActivityMapper shellActivityMapper;
    @Autowired
    private ShellSiteCompareMapper shellSiteCompareMapper;
    @Autowired
    private ShellActivityOilMapper shellActivityOilMapper;
    @Autowired
    private ShellActivityOilDetailMapper shellActivityOilDetailMapper;
    @Autowired
    private ShellActivityApplyMapper shellActivityApplyMapper;
    @Autowired
    private ShellActivityApplyCarMapper shellActivityApplyCarMapper;
    @Autowired
    private ShellActivityApplyCouponMapper shellActivityApplyCouponMapper;
    @Autowired
    private ShellActivityApplyGiftMapper shellActivityApplyGiftMapper;
    @Autowired
    private ShellActivityApplyOilMapper shellActivityApplyOilMapper;
    @Autowired
    private ShellActivityApplyRepurchaseMapper shellActivityApplyRepurchaseMapper;
    @Autowired
    private ShellStatementMapper shellStatementMapper;
    @Autowired
    private ShellStatementCompetitorMapper shellStatementCompetitorMapper;
    @Autowired
    private CompetitorSiteMapper competitorSiteMapper;
    @Autowired
    private DictSiteMapper dictSiteMapper;

    @Override
    public IPage<ActivityDic> findActivityDictList(ActivityDic activityDic, QueryRequest request) {
        Page<ActivityDic> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_DESC, false);
        return this.baseMapper.findActivityDictList(page,activityDic);
    }

    @Override
    public IPage<ActivityDicDetail> findActivityDictDetailList(ActivityDicDetail activityDicDetail, QueryRequest request) {
        Page<ActivityDicDetail> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_DESC, false);
        return activityDicDetailMapper.findActivityDictDetailList(page,activityDicDetail);
    }

    @Override
    @Transactional
    public void doSaveShellActivityDic(ActivityDic activityDic) {
        activityDic.setCreateTime(new Date());
        activityDic.setModifyTime(new Date());
        activityDicMapper.insert(activityDic);
    }

    @Override
    @Transactional
    public void updateShellActivityDic(ActivityDic activityDic) {
        activityDicMapper.updateById(activityDic);
    }

    @Override
    @Transactional
    public void deleteShellActivityDic(String[] ids) {
        activityDicMapper.deleteBatchIds(Arrays.asList(ids));
        for(String aid:Arrays.asList(ids)){
            LambdaQueryWrapper<ActivityDicDetail> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.in(ActivityDicDetail::getParentId, aid);
            List<ActivityDicDetail> list=activityDicDetailMapper.selectList(queryWrapper);
            if (CollectionUtils.isNotEmpty(list)){
                List<String> aIdList = new ArrayList<>();
                list.forEach(d -> aIdList.add(String.valueOf(d.getId())));
                activityDicDetailMapper.deleteBatchIds(aIdList);
            }
        }
    }

    @Override
    public void deleteShellActivityDicDetail(String[] ids) {
        activityDicDetailMapper.deleteBatchIds(Arrays.asList(ids));
    }

    @Override
    public void updateShellActivityDicDetail(ActivityDicDetail activityDicDetail) {
        activityDicDetailMapper.updateById(activityDicDetail);
    }

    @Override
    public void doSaveShellActivityDicDetail(ActivityDicDetail activityDicDetail) {
        ActivityDic activityDic=activityDicMapper.selectById(activityDicDetail.getParentId());
        activityDicDetail.setParentCode(activityDic.getCode());
        activityDicDetail.setCreateTime(new Date());
        activityDicDetail.setModifyTime(new Date());
        activityDicDetailMapper.insert(activityDicDetail);
    }

    @Override
    public IPage<ShellActivityDic> findshellActivityDictList(ShellActivityDic shellActivityDic, QueryRequest request) {
        Page<ShellActivityDic> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_DESC, false);
        return this.shellActivityDicMapper.findShellActivityDictList(page,shellActivityDic);
    }

    @Override
    public void doSaveActivityDic(ShellActivityDic shellActivityDic) {
        shellActivityDic.setCreateTime(new Date());
        shellActivityDic.setModifyTime(new Date());
        shellActivityDicMapper.insert(shellActivityDic);
    }

    @Override
    public void updateActivityDic(ShellActivityDic shellActivityDic) {
        shellActivityDicMapper.updateById(shellActivityDic);
    }

    @Override
    public void deleteActivityDic(String[] ids) {
        shellActivityDicMapper.deleteBatchIds(Arrays.asList(ids));
    }

    @Override
    public List<ShellSite> findShellSiteList(ShellSite shellSite) {
        return shellSiteMapper.findShellListMatchCodeName(shellSite);
    }

    @Override
    public List<ShellActivityOilDto> findShellAllSiteList(String shellId, String activityType,String searchDate) {
        //查询到当前油站数据
        ShellSite site=new ShellSite();
        site.setSiteid(Long.parseLong(shellId));
        ShellSite shellSite=shellSiteMapper.findEntityById(site);
        if(shellSite==null){
            throw new FebsException("油站数据异常");
        }
        List<ShellActivityOilDto> shellActivityOilDtos=new ArrayList<>();
        //检索油站信息录入 --判断是否创建活动
        ShellActivityOilDto dto=new ShellActivityOilDto();
        dto.setAddress(shellSite.getSiteCode()+","+shellSite.getSiteAreaName());
//        ShellActivity shellActivity=shellActivityMapper.selectBySiteId(shellSite.getSiteid());
        List<ShellActivity> shellActivityList=shellActivityMapper.selectBySiteId(shellSite.getSiteid());
        if(CollectionUtils.isEmpty(shellActivityList)){
            //活动新增检验油站活动是否多个
            ShellActivity shellActivity=new ShellActivity();
            shellActivity.setShellId(shellSite.getSiteid());
            shellActivity.setType("0");
            shellActivity.setShellCode(shellSite.getSiteCode());
            shellActivity.setShellName(shellSite.getSiteName());
//            shellActivity.setImgUrlPrice(shellSite.gets);
//            shellActivity.setRemark(dto.getRemark());
            shellActivity.setPriceDate(new Date());
            shellActivity.setActivityDate(new Date());
            shellActivity.setCreateTime(new Date());
            shellActivity.setModifyTime(new Date());
            shellActivityMapper.insert(shellActivity);
            //通过shellid查询活动一对一
            shellActivityList=shellActivityMapper.selectBySiteId(shellSite.getSiteid());
        }
        dto.setIcon(String.valueOf(shellSite.getSiteBrand()));
        dto.setType(String.valueOf(shellSite.getShellCompare()));
        dto.setShellId(shellSite.getSiteid().toString());
        dto.setShellCode(shellSite.getSiteCode());
        dto.setShellName(shellSite.getSiteName());
        dto.setOilCode(shellSite.getOilCode());
        dto.setAddress(shellSite.getSiteAddress());
        if(!CollectionUtils.isEmpty(shellActivityList)){
            dto.setLiveUrl(shellActivityList.get(0).getImgUrlPrice());
            dto.setRemark(shellActivityList.get(0).getRemark());
            dto.setOilDate(shellActivityList.get(0).getPriceDate());
            dto.setActDate(shellActivityList.get(0).getActivityDate());
        }
//        if(activityType.equals("0")){
            //价格调查
            ShellActivityOil shellActivityOil=new ShellActivityOil();
            if(!StringUtils.isEmpty(searchDate)){
                shellActivityOil.setOptDate(DateUtil.parse(searchDate,DateUtil.yyyy_MM_dd));
            }
            shellActivityOil.setShellId(shellSite.getSiteid());
            List<ShellActivityOil> shellActivityOilList=shellActivityOilMapper.findOilList(shellActivityOil);
            if(!CollectionUtils.isEmpty(shellActivityOilList)){
              dto.setShellActivityOilList(shellActivityOilList);
            }else{
              //不存在插入油品
                String str=shellSite.getOilCode();
                if(!StringUtils.isEmpty(str)){
                    String[] oilIds=str.split(",");
                    for(int i=0;i<oilIds.length;i++){
                       DictSite tp=dictSiteMapper.selectById(Long.parseLong(oilIds[i]));
                       if(!ObjectUtils.isEmpty(tp)){
                           ShellActivityOil oil=new ShellActivityOil();
                           oil.setShellId(shellSite.getSiteid());
                           if(tp.getDictName().contains("#")){
                               oil.setOilCode("#"+tp.getDictName().split("#")[0]);
                           }else{
                               oil.setOilCode(tp.getDictName());
                           }
                           oil.setPrice("0");
                           oil.setOptDate(DateUtil.parse(searchDate,DateUtil.yyyy_MM_dd));
                           oil.setOilName(tp.getDictName());
                           oil.setStatus("0");
                           oil.setActivityId(shellActivityList.get(0).getId());
                           oil.setCreateTime(new Date());
                           oil.setModifyTime(new Date());
                           shellActivityOilMapper.insert(oil);
                       }
                    }
                }
                ShellActivityOil temp=new ShellActivityOil();
//                temp.setStatus("0");
                if(!StringUtils.isEmpty(searchDate)){
                    temp.setOptDate(DateUtil.parse(searchDate,DateUtil.yyyy_MM_dd));
                }
                temp.setShellId(shellSite.getSiteid());
                shellActivityOilList=shellActivityOilMapper.findOilList(temp);
                dto.setShellActivityOilList(shellActivityOilList);
            }
//        }else{
//            //活动调查
            ShellActivityOilDetail shellActivityOilDetail=new ShellActivityOilDetail();
//            shellActivityOilDetail.setStatus("0");
            shellActivityOilDetail.setShellId(shellSite.getSiteid());
            if(!StringUtils.isEmpty(searchDate)){
                shellActivityOilDetail.setOptDate(DateUtil.parse(searchDate,DateUtil.yyyy_MM_dd));
            }
            List<ShellActivityOilDetail> shellActivityOilDetailList=shellActivityOilDetailMapper.findOilDetailList(shellActivityOilDetail);
            if(!CollectionUtils.isEmpty(shellActivityOilDetailList)){
            dto.setShellActivityOilDetailList(shellActivityOilDetailList);
        }
//        }
        shellActivityOilDtos.add(dto);
        //0壳牌1竞争对手油站 检索自营查询竞争对手的检索竞争对手查自营的
//        if(shellSite.getShellCompare()==0){
            //壳牌查找 相关竞争油站
//            List<ShellSiteCompare> shellSiteCompareList=shellSiteCompareMapper.findBySiteid(shellSite.getSiteid());
            //竞争对手表
            List<CompetitorSite> competitorSiteList=competitorSiteMapper.findByShellSiteid(shellSite.getSiteid());
            if(!CollectionUtils.isEmpty(competitorSiteList)){
                for(CompetitorSite st:competitorSiteList){
                    ShellActivityOilDto dt=new ShellActivityOilDto();
                    dt.setAddress(st.getSiteCode()+","+st.getSiteAreaName());
                    List<ShellActivity> shellActivityList1=shellActivityMapper.selectBySiteId(st.getSiteid());
                    if(CollectionUtils.isEmpty(shellActivityList1)){
                        //活动新增检验油站活动是否多个
                        ShellActivity shellActivity=new ShellActivity();
                        shellActivity.setShellId(st.getSiteid());
                        shellActivity.setType("1");
                        shellActivity.setShellCode(st.getSiteCode());
                        shellActivity.setShellName(st.getSiteName());
            //            shellActivity.setImgUrlPrice(shellSite.gets);
            //            shellActivity.setRemark(dto.getRemark());
                        shellActivity.setPriceDate(new Date());
                        shellActivity.setActivityDate(new Date());
                        shellActivity.setCreateTime(new Date());
                        shellActivity.setModifyTime(new Date());
                        shellActivityMapper.insert(shellActivity);
                        //通过shellid查询活动一对一
                        shellActivityList1=shellActivityMapper.selectBySiteId(st.getSiteid());
                    }
                    dt.setIcon(String.valueOf(st.getSiteBrand()));
                    dt.setType("1");
                    dt.setShellCode(st.getSiteCode());
                    dt.setShellId(st.getSiteid().toString());
                    dt.setShellName(st.getSiteName());
                    dt.setAddress(st.getSiteAddress());
                    dt.setOilCode(st.getOilCode());
                    if(!CollectionUtils.isEmpty(shellActivityList1)){
                        dt.setLiveUrl(shellActivityList1.get(0).getImgUrlPrice());
                        dt.setRemark(shellActivityList1.get(0).getRemark());
                        dt.setOilDate(shellActivityList1.get(0).getPriceDate());
                        dt.setActDate(shellActivityList1.get(0).getActivityDate());
                    }
                    //价格调查
                    ShellActivityOil so=new ShellActivityOil();
//                    so.setStatus("0");
                    if(!StringUtils.isEmpty(searchDate)){
                        so.setOptDate(DateUtil.parse(searchDate,DateUtil.yyyy_MM_dd));
                    }
                    so.setShellId(st.getSiteid());
                    List<ShellActivityOil> soList=shellActivityOilMapper.findOilList(so);
                    if(!CollectionUtils.isEmpty(soList)){
                        dt.setShellActivityOilList(soList);
                    }else{
                        //不存在插入油品
                        String str=st.getOilCode();
                        if(!StringUtils.isEmpty(str)){
                            String[] oilIds=str.split(",");
                            for(int i=0;i<oilIds.length;i++){
                                DictSite tp=dictSiteMapper.selectById(Long.parseLong(oilIds[i]));
                                if(!ObjectUtils.isEmpty(tp)){
                                    ShellActivityOil oil=new ShellActivityOil();
                                    oil.setShellId(st.getSiteid());
                                    if(tp.getDictName().contains("#")){
                                        oil.setOilCode("#"+tp.getDictName().split("#")[0]);
                                    }else{
                                        oil.setOilCode(tp.getDictName());
                                    }
                                    oil.setOptDate(DateUtil.parse(searchDate,DateUtil.yyyy_MM_dd));
                                    oil.setOilName(tp.getDictName());
                                    oil.setStatus("0");
                                    oil.setActivityId(shellActivityList1.get(0).getId());
                                    oil.setCreateTime(new Date());
                                    oil.setModifyTime(new Date());
                                    shellActivityOilMapper.insert(oil);
                                }
                            }
                        }
                        ShellActivityOil temp=new ShellActivityOil();
//                        temp.setStatus("0");
                        temp.setOptDate(DateUtil.parse(searchDate,DateUtil.yyyy_MM_dd));
                        temp.setShellId(st.getSiteid());
                        soList=shellActivityOilMapper.findOilList(temp);
                        dto.setShellActivityOilList(soList);
                    }
                    //活动调查
                    ShellActivityOilDetail shellActivityOilDetail1=new ShellActivityOilDetail();
//                    shellActivityOilDetail1.setStatus("0");
                    shellActivityOilDetail1.setShellId(st.getSiteid());
                    if(!StringUtils.isEmpty(searchDate)){
                        shellActivityOilDetail1.setOptDate(DateUtil.parse(searchDate,DateUtil.yyyy_MM_dd));
                    }
                    List<ShellActivityOilDetail> shellActivityOilDetailList1=shellActivityOilDetailMapper.findOilDetailList(shellActivityOilDetail1);
                    if(!CollectionUtils.isEmpty(shellActivityOilDetailList1)) {
                        dt.setShellActivityOilDetailList(shellActivityOilDetailList1);
                    }
                    shellActivityOilDtos.add(dt);
                }
            }

        return shellActivityOilDtos;
    }

    @Override
    @Transactional
    public void doSaveShellActivityPrice(List<ShellActivityOilDto> shellActivityOilDtoList) {
        if(CollectionUtils.isEmpty(shellActivityOilDtoList)){
            throw new FebsException("数据不能为空");
        }
        for(ShellActivityOilDto dto:shellActivityOilDtoList){
            if(dto.getOilDate()==null){
                throw new FebsException("调研时间不能为空");
            }
            List<ShellActivity> acList=shellActivityMapper.selectBySiteId(Long.parseLong(dto.getShellId()));
            if(CollectionUtils.isEmpty(acList)){
                //活动新增检验油站活动是否多个
                ShellActivity shellActivity=new ShellActivity();
                shellActivity.setShellId(Long.parseLong(dto.getShellId()));
                shellActivity.setType(dto.getType());
                shellActivity.setShellCode(dto.getShellCode());
                shellActivity.setShellName(dto.getShellName());
                shellActivity.setImgUrlPrice(dto.getLiveUrl());
                shellActivity.setRemark(dto.getRemark());
                shellActivity.setPriceDate(dto.getOilDate());
                shellActivity.setActivityDate(new Date());
                shellActivity.setCreateTime(new Date());
                shellActivity.setModifyTime(new Date());
                shellActivityMapper.insert(shellActivity);
                //通过shellid查询活动一对一
                List<ShellActivity> activityList=shellActivityMapper.selectBySiteId(Long.parseLong(dto.getShellId()));
                if(CollectionUtils.isEmpty(activityList)){
                    throw new FebsException("油站数据异常");
                }
                ShellActivity temp=activityList.get(0);
                //活动油价保存
                if(!CollectionUtils.isEmpty(dto.getShellActivityOilList())){
                    for(ShellActivityOil oil:dto.getShellActivityOilList()){
                        //第一次新建活动直接插入 不删除
                        oil.setStatus("0");
                        if(dto.getOilDate()!=null){
                            oil.setOptDate(dto.getOilDate());
                        }
                        oil.setActivityId(temp.getId());
                        oil.setCreateTime(new Date());
                        oil.setModifyTime(new Date());
                        shellActivityOilMapper.insert(oil);
                    }
                }
            }else{
                //设置历史修改
                //通过shellid查询活动一对一
//                List<ShellActivity> activityList=shellActivityMapper.selectBySiteId(Long.parseLong(dto.getShellId()));
//                if(CollectionUtils.isEmpty(activityList)){
//                    throw new FebsException("油站数据异常");
//                }
                ShellActivity temp=acList.get(0);
                if(!StringUtils.isEmpty(dto.getLiveUrl())){
                    temp.setImgUrlPrice(dto.getLiveUrl());
                    temp.setPriceDate(dto.getOilDate());
                    shellActivityMapper.updateById(temp);
                }
                //活动油价保存（同油站油品同一天），先删除上次活动数据
                ShellActivityOil tt=new ShellActivityOil();
                tt.setActivityId(temp.getId());
                tt.setOptDate(dto.getOilDate());
//                tt.setStatus("0");
                List<ShellActivityOil> shellActivityOilList=shellActivityOilMapper.findOilList(tt);
                if(!CollectionUtils.isEmpty(shellActivityOilList)){
                    List<String> aIdList = new ArrayList<>();
                    shellActivityOilList.forEach(d -> aIdList.add(String.valueOf(d.getId())));
                    shellActivityOilMapper.deleteBatchIds(aIdList);
//                    for(ShellActivityOil o:shellActivityOilList){
////                        o.setStatus("1");
//                        shellActivityOilMapper.updateById(o);
//                    }
                }
                if(!CollectionUtils.isEmpty(dto.getShellActivityOilList())){
                    for(ShellActivityOil oil:dto.getShellActivityOilList()){
                        //第一次新建活动直接插入 不删除
                        oil.setStatus("0");
                        oil.setActivityId(temp.getId());
                        if(dto.getOilDate()!=null){
                            oil.setOptDate(dto.getOilDate());
                        }
                        oil.setCreateTime(new Date());
                        oil.setModifyTime(new Date());
                        shellActivityOilMapper.insert(oil);
                    }
                }
            }
        }
    }

    @Override
    public void doSaveShellActivityDetail(List<ShellActivityOilDto> shellActivityOilDtoList) {
        if(CollectionUtils.isEmpty(shellActivityOilDtoList)){
            throw new FebsException("数据不能为空");
        }
        for(ShellActivityOilDto dto:shellActivityOilDtoList){
            if(dto.getOilDate()==null){
                throw new FebsException("调研时间不能为空");
            }
            List<ShellActivity> acList=shellActivityMapper.selectBySiteId(Long.parseLong(dto.getShellId()));
            if(CollectionUtils.isEmpty(acList)){
                //活动新增检验油站活动是否多个
                ShellActivity shellActivity=new ShellActivity();
                shellActivity.setShellId(Long.parseLong(dto.getShellId()));
                shellActivity.setType(dto.getType());
                shellActivity.setShellCode(dto.getShellCode());
                shellActivity.setShellName(dto.getShellName());
                shellActivity.setImgUrlLive(dto.getLiveUrl());
                shellActivity.setRemark(dto.getRemark());
                shellActivity.setActivityDate(dto.getOilDate());
                shellActivity.setPriceDate(new Date());
                shellActivity.setCreateTime(new Date());
                shellActivity.setModifyTime(new Date());
                shellActivityMapper.insert(shellActivity);
                //通过shellid查询活动一对一
                List<ShellActivity> activityList=shellActivityMapper.selectBySiteId(Long.parseLong(dto.getShellId()));
                if(CollectionUtils.isEmpty(activityList)){
                    throw new FebsException("油站数据异常");
                }
                ShellActivity temp=activityList.get(0);
                //活动调研保存
                if(!CollectionUtils.isEmpty(dto.getShellActivityOilDetailList())){
                    for(ShellActivityOilDetail detail:dto.getShellActivityOilDetailList()){
                        //第一次新建活动直接插入 不删除
                        detail.setStatus("0");
                        detail.setShellName(dto.getShellName());
                        detail.setActivityOilId(temp.getId());
                        if(dto.getOilDate()!=null){
                            detail.setOptDate(dto.getOilDate());
                        }
                        detail.setRemark(dto.getRemark());
                        detail.setCreateTime(new Date());
                        detail.setModifyTime(new Date());
                        shellActivityOilDetailMapper.insert(detail);
                    }
                }
            }else{
                //设置历史修改
                //通过shellid查询活动一对一
//                List<ShellActivity> activityList=shellActivityMapper.selectBySiteId(Long.parseLong(dto.getShellId()));
//                if(CollectionUtils.isEmpty(activityList)){
//                    throw new FebsException("油站数据异常");
//                }
                ShellActivity temp=acList.get(0);
                if(!StringUtils.isEmpty(dto.getLiveUrl())){
                    temp.setImgUrlLive(dto.getLiveUrl());
                    temp.setActivityDate(dto.getOilDate());
                    temp.setRemark(dto.getRemark());
                    shellActivityMapper.updateById(temp);
                }
                //活动油价保存，先删除上次活动数据
                ShellActivityOilDetail tt=new ShellActivityOilDetail();
                tt.setActivityOilId(temp.getId());
                tt.setStatus("0");
                List<ShellActivityOilDetail> shellActivityOilDetailList=shellActivityOilDetailMapper.findOilDetailList(tt);
                if(!CollectionUtils.isEmpty(shellActivityOilDetailList)){
                    for(ShellActivityOilDetail o:shellActivityOilDetailList){
                        o.setStatus("1");
                        shellActivityOilDetailMapper.updateById(o);
                    }
                }
                if(!CollectionUtils.isEmpty(dto.getShellActivityOilDetailList())){
                    for(ShellActivityOilDetail detail:dto.getShellActivityOilDetailList()){
                        //第一次新建活动直接插入 不删除
                        detail.setStatus("0");
                        detail.setActivityOilId(temp.getId());
                        detail.setShellName(dto.getShellName());
                        if(dto.getOilDate()!=null){
                            detail.setOptDate(dto.getOilDate());
                        }
                        detail.setRemark(dto.getRemark());
                        detail.setCreateTime(new Date());
                        detail.setModifyTime(new Date());
                        shellActivityOilDetailMapper.insert(detail);
                    }
                }
            }
        }
    }

    @Override
    public ShellActivityDic findActivityDetailById(Long dictId) {

        return shellActivityDicMapper.selectById(dictId);
    }

    @Override
    public ActivityDic findDicById(Long dictId) {
        ActivityDic activityDic=activityDicMapper.selectById(dictId);
        return activityDic;
    }

    @Override
    public List<ActivityDicDetail> findDictListByParentCode(ActivityDicDetail detail) {
        return activityDicDetailMapper.findDictListByParentCode(detail);
    }

    @Override
    public List<ShellActivityDic> findShellActivityByType(ShellActivityDic dic) {
        return shellActivityDicMapper.findDicList(dic);
    }

    @Override
    public void doAddShellActivityApply(ShellActivityApply shellActivityApply) {
        shellActivityApply.setCreateTime(new Date());
        shellActivityApply.setModifyTime(new Date());
        shellActivityApply.setStatus("new");
        shellActivityApplyMapper.insert(shellActivityApply);
    }

    @Override
    public void doAddShellActivityApplyDetail(ShellActivityApplyDto shellActivityApplyDto) {

        ShellActivityApply shellActivityApply=shellActivityApplyMapper.selectById(shellActivityApplyDto.getApplyId());
        if(shellActivityApply==null){
            throw new FebsException("活动申请不存在");
        }
        if(CollectionUtils.isEmpty(shellActivityApplyDto.getShellActivityApplyOilDtoList())){
            throw new FebsException("活动内容不存在");
        }
        //更新主体
        shellActivityApplyMapper.updateById(shellActivityApplyDto.getShellActivityApply());
        //礼品
        if(!CollectionUtils.isEmpty(shellActivityApplyDto.getShellActivityApplyGiftList())){
            for(ShellActivityApplyGift dt:shellActivityApplyDto.getShellActivityApplyGiftList()){
                dt.setModifyTime(new Date());
                dt.setCreateTime(new Date());
                dt.setActivityId(shellActivityApply.getId());
                shellActivityApplyGiftMapper.insert(dt);
            }
        }
        //换购
        if(!CollectionUtils.isEmpty(shellActivityApplyDto.getShellActivityApplyRepurchaseList())){
            for(ShellActivityApplyRepurchase dt:shellActivityApplyDto.getShellActivityApplyRepurchaseList()){
                dt.setModifyTime(new Date());
                dt.setCreateTime(new Date());
                dt.setActivityId(shellActivityApply.getId());
                shellActivityApplyRepurchaseMapper.insert(dt);
            }
        }
        //油品详情
        for(ShellActivityApplyOilDto dto:shellActivityApplyDto.getShellActivityApplyOilDtoList()){
            ShellActivityApplyOil oil=dto.getShellActivityApplyOil();
            oil.setModifyTime(new Date());
            oil.setCreateTime(new Date());
            oil.setActivityId(shellActivityApply.getId());
            shellActivityApplyOilMapper.insert(oil);
            //车
            if(!CollectionUtils.isEmpty(dto.getShellActivityApplyCarList())){
                for(ShellActivityApplyCar dt:dto.getShellActivityApplyCarList()){
                    dt.setModifyTime(new Date());
                    dt.setCreateTime(new Date());
                    dt.setActivityId(shellActivityApply.getId());
                    shellActivityApplyCarMapper.insert(dt);
                }
            }
            //优惠券
            if(!CollectionUtils.isEmpty(dto.getShellActivityApplyCouponList())){
                for(ShellActivityApplyCoupon dt:dto.getShellActivityApplyCouponList()){
                    dt.setModifyTime(new Date());
                    dt.setCreateTime(new Date());
                    dt.setActivityId(shellActivityApply.getId());
                    shellActivityApplyCouponMapper.insert(dt);
                }
            }
        }
    }

    @Override
    public IPage<ShellActivityApply> findshellActivityApplyList(ShellActivityApply shellActivityApply, QueryRequest request) {
        Page<ShellActivityApply> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_DESC, false);
        return this.shellActivityApplyMapper.findshellActivityApplyList(page,shellActivityApply);
    }

    @Override
    public ShellActivityApplyDto findShellActivityApplyDetailById(String id) {
        //主体查询
        ShellActivityApply shellActivityApply=shellActivityApplyMapper.selectById(id);
        if(shellActivityApply==null){
            throw new FebsException("活动申请不存在");
        }
        ShellActivityApplyDto dto=new ShellActivityApplyDto();
        dto.setShellActivityApply(shellActivityApply);
        //礼品
        ShellActivityApplyGift gift=new ShellActivityApplyGift();
        gift.setActivityId(shellActivityApply.getId());
        List<ShellActivityApplyGift> shellActivityApplyGiftList=shellActivityApplyGiftMapper.findList(gift);
        dto.setShellActivityApplyGiftList(shellActivityApplyGiftList);
        //换购
        ShellActivityApplyRepurchase repurchase=new ShellActivityApplyRepurchase();
        repurchase.setActivityId(shellActivityApply.getId());
        List<ShellActivityApplyRepurchase> shellActivityApplyRepurchaseList=shellActivityApplyRepurchaseMapper.findList(repurchase);
        dto.setShellActivityApplyRepurchaseList(shellActivityApplyRepurchaseList);
        List<ShellActivityApplyOilDto> shellActivityApplyOilDtoList=new ArrayList<>();
        ShellActivityApplyOil oil=new ShellActivityApplyOil();
        oil.setActivityId(shellActivityApply.getId());
        List<ShellActivityApplyOil> shellActivityApplyOils=shellActivityApplyOilMapper.findByParentId(oil);
        if(!CollectionUtils.isEmpty(shellActivityApplyOils)){
            for(ShellActivityApplyOil shellActivityApplyOil:shellActivityApplyOils){
                ShellActivityApplyOilDto dt=new ShellActivityApplyOilDto();
                dt.setShellActivityApplyOil(shellActivityApplyOil);
                //车
                ShellActivityApplyCar car=new ShellActivityApplyCar();
                car.setActivityId(shellActivityApply.getId());
                car.setOilCode(shellActivityApplyOil.getOilCode());
                List<ShellActivityApplyCar> shellActivityApplyCarList=shellActivityApplyCarMapper.findList(car);
                dt.setShellActivityApplyCarList(shellActivityApplyCarList);
                //优惠券
                ShellActivityApplyCoupon coupon=new ShellActivityApplyCoupon();
                coupon.setActivityId(shellActivityApply.getId());
                coupon.setOilCode(shellActivityApplyOil.getOilCode());
                List<ShellActivityApplyCoupon> shellActivityApplyCouponList=shellActivityApplyCouponMapper.findList(coupon);
                dt.setShellActivityApplyCouponList(shellActivityApplyCouponList);
                shellActivityApplyOilDtoList.add(dt);
            }
        }
        dto.setShellActivityApplyOilDtoList(shellActivityApplyOilDtoList);
        return dto;
    }

    @Override
    public IPage<?> findShellStatementList(ShellStatement shellStatement, QueryRequest request) {
        Page<ActivityDic> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_DESC, false);
        return this.shellStatementMapper.findShellStatementList(page,shellStatement);
    }

    @Override
    public void deleteShellStatement(String[] ids) {
        shellStatementMapper.deleteBatchIds(Arrays.asList(ids));
        for(String aid:Arrays.asList(ids)){
            LambdaQueryWrapper<ShellStatementCompetitor> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.in(ShellStatementCompetitor::getParentId, aid);
            List<ShellStatementCompetitor> list=shellStatementCompetitorMapper.selectList(queryWrapper);
            if (CollectionUtils.isNotEmpty(list)){
                List<String> aIdList = new ArrayList<>();
                list.forEach(d -> aIdList.add(String.valueOf(d.getId())));
                shellStatementCompetitorMapper.deleteBatchIds(aIdList);
            }
        }
    }

    @Override
    public ShellStatementDto findShellStatementyDetailById(String id) {
        ShellStatementDto dto=new ShellStatementDto();
        ShellStatement shellStatement=shellStatementMapper.selectById(id);
        if(shellStatement==null){
            throw new FebsException("评估表不存在");
        }
        dto.setShellStatement(shellStatement);
        LambdaQueryWrapper<ShellStatementCompetitor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ShellStatementCompetitor::getParentId, shellStatement.getId());
        List<ShellStatementCompetitor> list=shellStatementCompetitorMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(list)){
            dto.setShellStatementCompetitorList(list);
        }
        return dto;
    }

    @Override
    public void doSaveShellStatement(ShellStatementDto shellStatementDto) {
        ShellStatement shellStatement=shellStatementDto.getShellStatement();
        shellStatement.setShellId(Long.parseLong(shellStatementDto.getShellId()));
        shellStatement.setModifyTime(new Date());
        shellStatement.setCreateTime(new Date());
        shellStatementMapper.insert(shellStatement);

        ShellStatement temp=shellStatementMapper.findLast();
        if(temp==null){
            throw new FebsException("评估表不存在");
        }
        if(CollectionUtils.isNotEmpty(shellStatementDto.getShellStatementCompetitorList())){
            for(ShellStatementCompetitor st:shellStatementDto.getShellStatementCompetitorList()){
                st.setParentId(temp.getId());
                st.setShellId(temp.getShellId());
                st.setModifyTime(new Date());
                st.setCreateTime(new Date());
                shellStatementCompetitorMapper.insert(st);
            }
        }
    }

    @Override
    public ShellSite findShellSiteDetail(String shellId) {
        return shellSiteMapper.selectById(Long.parseLong(shellId));
    }

    @Override
    public void updateShellStatement(ShellStatementDto shellStatementDto) {
        ShellStatement shellStatement=shellStatementDto.getShellStatement();
        shellStatement.setId(Long.parseLong(shellStatementDto.getId()));
        shellStatement.setModifyTime(new Date());
        shellStatement.setShellId(Long.parseLong(shellStatementDto.getShellId()));
        shellStatementMapper.updateById(shellStatement);
        //先删除再插入详情
        LambdaQueryWrapper<ShellStatementCompetitor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ShellStatementCompetitor::getParentId, shellStatement.getId());
        List<ShellStatementCompetitor> list=shellStatementCompetitorMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(list)){
            List<String> aIdList = new ArrayList<>();
            list.forEach(d -> aIdList.add(String.valueOf(d.getId())));
            shellStatementCompetitorMapper.deleteBatchIds(aIdList);
        }
        if(CollectionUtils.isNotEmpty(shellStatementDto.getShellStatementCompetitorList())){
            for(ShellStatementCompetitor st:shellStatementDto.getShellStatementCompetitorList()){
                st.setParentId(shellStatement.getId());
                st.setShellId(shellStatement.getShellId());
                st.setModifyTime(new Date());
                st.setCreateTime(new Date());
                shellStatementCompetitorMapper.insert(st);
            }
        }
    }

    @Override
    public List<ShellActivityOilDto> findShellAllSiteListA(String shellId, String activityType) {
        //查询到当前油站数据
        ShellSite site=new ShellSite();
        site.setSiteid(Long.parseLong(shellId));
        ShellSite shellSite=shellSiteMapper.findEntityById(site);
        if(shellSite==null){
            throw new FebsException("油站数据异常");
        }
        List<ShellActivityOilDto> shellActivityOilDtos=new ArrayList<>();
        //检索油站信息录入 --判断是否创建活动
        ShellActivityOilDto dto=new ShellActivityOilDto();
        dto.setAddress(shellSite.getSiteCode()+","+shellSite.getSiteAreaName());
//        ShellActivity shellActivity=shellActivityMapper.selectBySiteId(shellSite.getSiteid());
        List<ShellActivity> shellActivityList=shellActivityMapper.selectBySiteId(shellSite.getSiteid());
        if(CollectionUtils.isEmpty(shellActivityList)){
            //活动新增检验油站活动是否多个
            ShellActivity shellActivity=new ShellActivity();
            shellActivity.setShellId(shellSite.getSiteid());
            shellActivity.setType("0");
            shellActivity.setShellCode(shellSite.getSiteCode());
            shellActivity.setShellName(shellSite.getSiteName());
//            shellActivity.setImgUrlPrice(shellSite.gets);
//            shellActivity.setRemark(dto.getRemark());
            shellActivity.setPriceDate(new Date());
            shellActivity.setActivityDate(new Date());
            shellActivity.setCreateTime(new Date());
            shellActivity.setModifyTime(new Date());
            shellActivityMapper.insert(shellActivity);
            //通过shellid查询活动一对一
            shellActivityList=shellActivityMapper.selectBySiteId(shellSite.getSiteid());
        }
        dto.setIcon(String.valueOf(shellSite.getSiteBrand()));
        dto.setType(String.valueOf(shellSite.getShellCompare()));
        dto.setShellId(shellSite.getSiteid().toString());
        dto.setShellCode(shellSite.getSiteCode());
        dto.setShellName(shellSite.getSiteName());
        dto.setOilCode(shellSite.getOilCode());
        dto.setAddress(shellSite.getSiteAddress());
        if(!CollectionUtils.isEmpty(shellActivityList)){
            dto.setLiveUrl(shellActivityList.get(0).getImgUrlPrice());
            dto.setRemark(shellActivityList.get(0).getRemark());
            dto.setOilDate(shellActivityList.get(0).getPriceDate());
            dto.setActDate(shellActivityList.get(0).getActivityDate());
        }
//        if(activityType.equals("0")){
        //价格调查
        ShellActivityOil shellActivityOil=new ShellActivityOil();
//            shellActivityOil.setStatus("0");
        shellActivityOil.setShellId(shellSite.getSiteid());
        List<ShellActivityOil> shellActivityOilList=shellActivityOilMapper.findOilList(shellActivityOil);
        if(!CollectionUtils.isEmpty(shellActivityOilList)){
            dto.setShellActivityOilList(shellActivityOilList);
        }else{
            //不存在插入油品
            String str=shellSite.getOilCode();
            if(!StringUtils.isEmpty(str)){
                String[] oilIds=str.split(",");
                for(int i=0;i<oilIds.length;i++){
                    DictSite tp=dictSiteMapper.selectById(Long.parseLong(oilIds[i]));
                    if(!ObjectUtils.isEmpty(tp)){
                        ShellActivityOil oil=new ShellActivityOil();
                        oil.setShellId(shellSite.getSiteid());
                        if(tp.getDictName().contains("#")){
                            oil.setOilCode("#"+tp.getDictName().split("#")[0]);
                        }else{
                            oil.setOilCode(tp.getDictName());
                        }
                        oil.setPrice("0");
                        oil.setOptDate(new Date());
                        oil.setOilName(tp.getDictName());
                        oil.setStatus("0");
                        oil.setActivityId(shellActivityList.get(0).getId());
                        oil.setCreateTime(new Date());
                        oil.setModifyTime(new Date());
                        shellActivityOilMapper.insert(oil);
                    }
                }
            }
            ShellActivityOil temp=new ShellActivityOil();
//                temp.setStatus("0");
            temp.setShellId(shellSite.getSiteid());
            shellActivityOilList=shellActivityOilMapper.findOilList(temp);
            dto.setShellActivityOilList(shellActivityOilList);
        }
//        }else{
//            //活动调查
        ShellActivityOilDetail shellActivityOilDetail=new ShellActivityOilDetail();
//            shellActivityOilDetail.setStatus("0");
        shellActivityOilDetail.setShellId(shellSite.getSiteid());
        List<ShellActivityOilDetail> shellActivityOilDetailList=shellActivityOilDetailMapper.findOilDetailList(shellActivityOilDetail);
        if(!CollectionUtils.isEmpty(shellActivityOilDetailList)){
            dto.setShellActivityOilDetailList(shellActivityOilDetailList);
        }
//        }
        shellActivityOilDtos.add(dto);
        //0壳牌1竞争对手油站 检索自营查询竞争对手的检索竞争对手查自营的
//        if(shellSite.getShellCompare()==0){
        //壳牌查找 相关竞争油站
//            List<ShellSiteCompare> shellSiteCompareList=shellSiteCompareMapper.findBySiteid(shellSite.getSiteid());
        //竞争对手表
        List<CompetitorSite> competitorSiteList=competitorSiteMapper.findByShellSiteid(shellSite.getSiteid());
        if(!CollectionUtils.isEmpty(competitorSiteList)){
            for(CompetitorSite st:competitorSiteList){
                ShellActivityOilDto dt=new ShellActivityOilDto();
                dt.setAddress(st.getSiteCode()+","+st.getSiteAreaName());
                List<ShellActivity> shellActivityList1=shellActivityMapper.selectBySiteId(st.getSiteid());
                if(CollectionUtils.isEmpty(shellActivityList1)){
                    //活动新增检验油站活动是否多个
                    ShellActivity shellActivity=new ShellActivity();
                    shellActivity.setShellId(st.getSiteid());
                    shellActivity.setType("1");
                    shellActivity.setShellCode(st.getSiteCode());
                    shellActivity.setShellName(st.getSiteName());
                    //            shellActivity.setImgUrlPrice(shellSite.gets);
                    //            shellActivity.setRemark(dto.getRemark());
                    shellActivity.setPriceDate(new Date());
                    shellActivity.setActivityDate(new Date());
                    shellActivity.setCreateTime(new Date());
                    shellActivity.setModifyTime(new Date());
                    shellActivityMapper.insert(shellActivity);
                    //通过shellid查询活动一对一
                    shellActivityList1=shellActivityMapper.selectBySiteId(st.getSiteid());
                }
                dt.setIcon(String.valueOf(st.getSiteBrand()));
                dt.setType("1");
                dt.setShellCode(st.getSiteCode());
                dt.setShellId(st.getSiteid().toString());
                dt.setShellName(st.getSiteName());
                dt.setAddress(st.getSiteAddress());
                dt.setOilCode(st.getOilCode());
                if(!CollectionUtils.isEmpty(shellActivityList1)){
                    dt.setLiveUrl(shellActivityList1.get(0).getImgUrlPrice());
                    dt.setRemark(shellActivityList1.get(0).getRemark());
                    dt.setOilDate(shellActivityList1.get(0).getPriceDate());
                    dt.setActDate(shellActivityList1.get(0).getActivityDate());
                }
                //价格调查
                ShellActivityOil so=new ShellActivityOil();
//                    so.setStatus("0");
                so.setShellId(st.getSiteid());
                List<ShellActivityOil> soList=shellActivityOilMapper.findOilList(so);
                if(!CollectionUtils.isEmpty(soList)){
                    dt.setShellActivityOilList(soList);
                }else{
                    //不存在插入油品
                    String str=st.getOilCode();
                    if(!StringUtils.isEmpty(str)){
                        String[] oilIds=str.split(",");
                        for(int i=0;i<oilIds.length;i++){
                            DictSite tp=dictSiteMapper.selectById(Long.parseLong(oilIds[i]));
                            if(!ObjectUtils.isEmpty(tp)){
                                ShellActivityOil oil=new ShellActivityOil();
                                oil.setShellId(st.getSiteid());
                                if(tp.getDictName().contains("#")){
                                    oil.setOilCode("#"+tp.getDictName().split("#")[0]);
                                }else{
                                    oil.setOilCode(tp.getDictName());
                                }
                                oil.setOilName(tp.getDictName());
                                oil.setStatus("0");
                                oil.setActivityId(shellActivityList1.get(0).getId());
                                oil.setCreateTime(new Date());
                                oil.setModifyTime(new Date());
                                shellActivityOilMapper.insert(oil);
                            }
                        }
                    }
                    ShellActivityOil temp=new ShellActivityOil();
//                        temp.setStatus("0");
                    temp.setShellId(st.getSiteid());
                    soList=shellActivityOilMapper.findOilList(temp);
                    dto.setShellActivityOilList(soList);
                }
                //活动调查
                ShellActivityOilDetail shellActivityOilDetail1=new ShellActivityOilDetail();
//                    shellActivityOilDetail1.setStatus("0");
                shellActivityOilDetail1.setShellId(st.getSiteid());
                List<ShellActivityOilDetail> shellActivityOilDetailList1=shellActivityOilDetailMapper.findOilDetailList(shellActivityOilDetail1);
                if(!CollectionUtils.isEmpty(shellActivityOilDetailList1)) {
                    dt.setShellActivityOilDetailList(shellActivityOilDetailList1);
                }
                shellActivityOilDtos.add(dt);
            }
        }

        return shellActivityOilDtos;
    }

    @Override
    public void doShellActivityDetaiDel(String id) {
        shellActivityOilDetailMapper.deleteById(id);
    }
}
