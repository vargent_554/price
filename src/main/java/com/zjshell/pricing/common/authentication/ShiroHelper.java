package com.zjshell.pricing.common.authentication;

import org.apache.shiro.authz.AuthorizationInfo;

import com.zjshell.pricing.common.annotation.Helper;

/**
 * @author YangLei
 */
@Helper
public class ShiroHelper extends ShiroRealm {

    /**
     * 获取当前用户的角色和权限集合
     *
     * @return AuthorizationInfo
     */
    public AuthorizationInfo getCurrentuserAuthorizationInfo() {
        return super.doGetAuthorizationInfo(null);
    }
}
