package com.zjshell.pricing.common.entity;

/**
 * @author YangLei
 */
public enum LimitType {
    /**
     * 传统类型
     */
    CUSTOMER,
    /**
     *  根据 IP地址限制
     */
    IP
}
