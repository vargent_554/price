package com.zjshell.pricing.common.function;

import com.zjshell.pricing.common.exception.RedisConnectException;

/**
 * @author YangLei
 */
@FunctionalInterface
public interface JedisExecutor<T, R> {
    R excute(T t) throws RedisConnectException;
}
