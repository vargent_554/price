package com.zjshell.pricing.common.properties;

import lombok.Data;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

/**
 * @author YangLei
 */
@Data
@SpringBootConfiguration
@PropertySource(value = {"classpath:febs.properties"})
@ConfigurationProperties(prefix = "febs")
public class FebsProperties {

    private ShiroProperties shiro = new ShiroProperties();
    private boolean autoOpenBrowser = true;
    private String[] autoOpenBrowserEnv = {};
    private SwaggerProperties swagger = new SwaggerProperties();
    public ShiroProperties getShiro() {
		return shiro;
	}
	public void setShiro(ShiroProperties shiro) {
		this.shiro = shiro;
	}
	public boolean isAutoOpenBrowser() {
		return autoOpenBrowser;
	}
	public void setAutoOpenBrowser(boolean autoOpenBrowser) {
		this.autoOpenBrowser = autoOpenBrowser;
	}
	public String[] getAutoOpenBrowserEnv() {
		return autoOpenBrowserEnv;
	}
	public void setAutoOpenBrowserEnv(String[] autoOpenBrowserEnv) {
		this.autoOpenBrowserEnv = autoOpenBrowserEnv;
	}
	public SwaggerProperties getSwagger() {
		return swagger;
	}
	public void setSwagger(SwaggerProperties swagger) {
		this.swagger = swagger;
	}
    
}
