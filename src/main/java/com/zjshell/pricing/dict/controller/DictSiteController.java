package com.zjshell.pricing.dict.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.wuwenze.poi.ExcelKit;
import com.zjshell.pricing.common.annotation.ControllerEndpoint;
import com.zjshell.pricing.common.entity.DictSiteTree;
import com.zjshell.pricing.common.entity.FebsResponse;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.exception.FebsException;
import com.zjshell.pricing.dict.entity.DictSite;
import com.zjshell.pricing.dict.entity.ShellStation;
import com.zjshell.pricing.dict.service.IDictSiteService;
import com.zjshell.pricing.dict.service.IShellStationService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 类名称:DictSiteController
 * 类描述:  
 * 创建人:杨磊   
 * 修改人:XXX 
 * 修改时间:2019年10月22日 上午10:55:42
 * 修改备注:  
 * @version 1.0.0
 * Copyright(c)2019浙江壳牌-版权所有
 */
@Slf4j
@RestController
@RequestMapping("dictsite")
public class DictSiteController {
	@Autowired
    private IDictSiteService dictSiteService;
	
	@Autowired
    private IShellStationService shellStationService;
	
	@GetMapping("select/tree")
    @ControllerEndpoint(exceptionMessage = "获取油站字典树失败")
    public List<DictSiteTree<DictSite>> getDictSiteTree() throws FebsException {
        return this.dictSiteService.findDictSites();
    }
	
	@GetMapping("parentSelect/{parentId}")
    @ControllerEndpoint(exceptionMessage = "获取油站字典树失败")
    public FebsResponse getParentDictSite(@NotBlank(message = "{required}") @PathVariable Long parentId) throws FebsException {
		DictSite dictSite = new DictSite();
		dictSite.setParentId(parentId);
		return new FebsResponse().success().data(this.dictSiteService.getListByParentId(dictSite));
    }
	
	@GetMapping("parentTree/{parentId}")
    @ControllerEndpoint(exceptionMessage = "获取油站字典树失败")
    public List<DictSiteTree<DictSite>> getDictSiteParentTree(@NotBlank(message = "{required}") @PathVariable Long parentId) throws FebsException {
		DictSite dictSite = new DictSite();
		dictSite.setParentId(parentId);
        return this.dictSiteService.findParentDictSites(dictSite);
    }


    @GetMapping("tree")
    @ControllerEndpoint(exceptionMessage = "获取油站字典树失败")
    public FebsResponse getDictSiteTree(DictSite dictSite) throws FebsException {
        List<DictSiteTree<DictSite>> dictSites = this.dictSiteService.findDictSites(dictSite);
        return new FebsResponse().success().data(dictSites);
    }


    @PostMapping
    @RequiresPermissions("dictsite:add")
    @ControllerEndpoint(operation = "新增油站字典", exceptionMessage = "新增油站字典失败")
    public FebsResponse addDictSite(@Valid DictSite dictSite) {
    	if(dictSite.getParentId() != null)
    	{
    		DictSite dictSite2 = dictSiteService.getById(dictSite.getParentId());
        	int dlevel = dictSite2.getDlevel()+1;
        	String droot = dictSite2.getDroot()+dictSite.getParentId()+',';
        	dictSite.setDlevel(dlevel);
        	dictSite.setDroot(droot);
    	}
    	else {
    		String droot = "0,";
    		int dlevel = 1;
    		dictSite.setDlevel(dlevel);
    		dictSite.setDroot(droot);
		}
        this.dictSiteService.createDictSite(dictSite);
        return new FebsResponse().success();
    }

    @GetMapping("delete/{dsIds}")
    @RequiresPermissions("dictsite:delete")
    @ControllerEndpoint(operation = "删除油站字典", exceptionMessage = "删除油站字典失败")
    public FebsResponse deleteDictSites(@NotBlank(message = "{required}") @PathVariable String dsIds) throws FebsException {
        String[] ids = dsIds.split(StringPool.COMMA);
        this.dictSiteService.deleteDictSites(ids);
        return new FebsResponse().success();
    }

    @PostMapping("update")
    @RequiresPermissions("dictsite:update")
    @ControllerEndpoint(operation = "修改油站字典", exceptionMessage = "修改油站字典失败")
    public FebsResponse updateDictSite(@Valid DictSite dictSite) throws FebsException {
    	if(dictSite.getParentId()>0)
    	{
    		DictSite dictSite2 = dictSiteService.getById(dictSite.getParentId());
        	int dlevel = dictSite2.getDlevel()+1;
        	String droot = dictSite2.getDroot()+dictSite.getParentId()+',';
        	dictSite.setDlevel(dlevel);
        	dictSite.setDroot(droot);
    	}
        this.dictSiteService.updateById(dictSite);
        return new FebsResponse().success();
    }

    @GetMapping("excel")
    @RequiresPermissions("dictsite:export")
    @ControllerEndpoint(exceptionMessage = "导出Excel失败")
    public void export(DictSite dictSite, QueryRequest request, HttpServletResponse response) throws FebsException {
        List<DictSite> dictSites = this.dictSiteService.findDictSites(dictSite, request);
        ExcelKit.$Export(DictSite.class, response).downXlsx(dictSites, false);
    }
    
    
    @GetMapping("station")
	@RequiresPermissions("dictsite:station")
    public FebsResponse getShellStations(ShellStation shellStation) {
        return new FebsResponse().success().data(shellStationService.findShellStation(shellStation));
    }
}
