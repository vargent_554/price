package com.zjshell.pricing.dict.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsConstant;
import com.zjshell.pricing.common.utils.FebsUtil;


/**
 * 
 * 类名称:ViewController
 * 类描述:  
 * 创建人:杨磊   
 * 修改人:XXX 
 * 修改时间:2019年10月22日 上午10:55:33
 * 修改备注:  
 * @version 1.0.0
 * Copyright(c)2019浙江壳牌-版权所有
 */
@Controller("dict")
@RequestMapping(FebsConstant.VIEW_PREFIX + "dict")
public class ViewController extends BaseController{


	    @GetMapping("dictsite")
	    @RequiresPermissions("dict:dictsite:view")
	    public String dictsite() {
	        return FebsUtil.view("dict/dictsite/dictsite");
	    }

}
