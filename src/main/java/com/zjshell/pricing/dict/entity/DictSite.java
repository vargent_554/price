package com.zjshell.pricing.dict.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;
import com.zjshell.pricing.common.converter.TimeConverter;

import lombok.Data;

/**
 * 
 * 类名称:SiteGroup
 * 类描述:  
 * 创建人:杨磊   
 * 修改人:XXX 
 * 修改时间:2019年10月10日 上午10:53:07
 * 修改备注:  
 * @version 1.0.0
 * Copyright(c)2019浙江壳牌-版权所有
 */
@Data
@TableName("t_dict_site")
@Excel("油站字典")
public class DictSite implements Serializable {

	private static final long serialVersionUID = -1882050139642509040L;

	@TableId(value = "did", type = IdType.AUTO)
    private Long did;
	
	@TableField("parent_id")
	private Long parentId;
	
	@TableField("order_num")
    private Integer orderNum;

	@TableField("dict_name")
	@NotBlank(message = "{required}")
	@Size(max = 30, message = "{noMoreThan}")
	@ExcelField(value = "字典名称")
	private String dictName;
	 
	@TableField("creat_time")
    @ExcelField(value = "创建时间", writeConverter = TimeConverter.class)
    private Date createTime;
	
	@TableField("dlevel")
	private int dlevel;

	@TableField("droot")
	private String droot;
	

	public int getDlevel() {
		return dlevel;
	}

	public void setDlevel(int dlevel) {
		this.dlevel = dlevel;
	}

	public String getDroot() {
		return droot;
	}

	public void setDroot(String droot) {
		this.droot = droot;
	}

	public Long getDid() {
		return did;
	}

	public void setDid(Long did) {
		this.did = did;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public String getDictName() {
		return dictName;
	}

	public void setDictName(String dictName) {
		this.dictName = dictName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	
	
}
