package com.zjshell.pricing.dict.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;

import lombok.Data;

@Data
@TableName("t_shell_station")
@Excel("油站字典")
public class ShellStation implements Serializable {

	private static final long serialVersionUID = 6027107853887797486L;
	
	@TableId(value = "id", type = IdType.AUTO)
    private int id;
	
	@TableField("station_code")
	private String stationCode;
	
	@TableField("station_name")
    private String stationName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	
	

}
