package com.zjshell.pricing.dict.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjshell.pricing.dict.entity.DictSite;

/**
 * 
 * 类名称:DictSiteMapper
 * 类描述:  
 * 创建人:杨磊   
 * 修改人:XXX 
 * 修改时间:2019年10月22日 上午9:57:28
 * 修改备注:  
 * @version 1.0.0
 * Copyright(c)2019浙江壳牌-版权所有
 */
public interface DictSiteMapper extends BaseMapper<DictSite>{
	
	List<DictSite> getListByParentId(DictSite dictSite);
	
	List<DictSite> getByParentId(DictSite dictSite);

}
