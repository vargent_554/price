package com.zjshell.pricing.dict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjshell.pricing.dict.entity.ShellStation;


public interface ShellStationMapper extends BaseMapper<ShellStation>{

}
