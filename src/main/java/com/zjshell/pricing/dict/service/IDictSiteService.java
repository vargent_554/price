package com.zjshell.pricing.dict.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjshell.pricing.common.entity.DictSiteTree;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.dict.entity.DictSite;

/**
 * 
 * 类名称:IDictSiteService
 * 类描述:  
 * 创建人:杨磊   
 * 修改人:XXX 
 * 修改时间:2019年10月22日 上午10:07:33
 * 修改备注:  
 * @version 1.0.0
 * Copyright(c)2019浙江壳牌-版权所有
 */
public interface IDictSiteService extends IService<DictSite>{

	List<DictSiteTree<DictSite>> findDictSites();
	
	List<DictSiteTree<DictSite>> findDictSites(DictSite dictSite);
	
	List<DictSite> findDictSites(DictSite dictSite, QueryRequest request);
	
	void createDictSite(DictSite dictSite);
	
	void updateDictSite(DictSite dictSite);
	
	void deleteDictSites(String[] dsIds);
	
	List<DictSite> getListByParentId(DictSite dictSite);
	
	List<DictSiteTree<DictSite>> findParentDictSites(DictSite dictSite);
}
