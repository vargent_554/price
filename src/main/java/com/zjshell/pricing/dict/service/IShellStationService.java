package com.zjshell.pricing.dict.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjshell.pricing.dict.entity.ShellStation;

public interface IShellStationService extends IService<ShellStation>{

	List<ShellStation> findShellStation(ShellStation shellStation);
}
