package com.zjshell.pricing.dict.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjshell.pricing.common.entity.DictSiteTree;
import com.zjshell.pricing.common.entity.FebsConstant;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.utils.SortUtil;
import com.zjshell.pricing.common.utils.TreeUtil;
import com.zjshell.pricing.dict.entity.DictSite;
import com.zjshell.pricing.dict.mapper.DictSiteMapper;
import com.zjshell.pricing.dict.service.IDictSiteService;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class DictSiteServiceImpl extends ServiceImpl<DictSiteMapper, DictSite> implements IDictSiteService{
	
	@Autowired
	private DictSiteMapper dictsitemapper;

	@Override
	public List<DictSiteTree<DictSite>> findDictSites() {
		 List<DictSite> dictSites = this.baseMapper.selectList(new QueryWrapper<>());
	     List<DictSiteTree<DictSite>> trees = this.convertDictSites(dictSites);
	     return TreeUtil.buildDictSiteTree(trees);
	}

	@Override
	public List<DictSiteTree<DictSite>> findDictSites(DictSite dictSite) {
		QueryWrapper<DictSite> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(dictSite.getDictName()))
            queryWrapper.lambda().eq(DictSite::getDictName, dictSite.getDictName());
        queryWrapper.lambda().orderByAsc(DictSite::getOrderNum);

        List<DictSite> dictSites = this.baseMapper.selectList(queryWrapper);
        List<DictSiteTree<DictSite>> trees =  this.convertDictSites(dictSites);
        return TreeUtil.buildDictSiteTree(trees);
	}

	@Override
	public List<DictSite> findDictSites(DictSite dictSite, QueryRequest request) {
		QueryWrapper<DictSite> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotBlank(dictSite.getDictName()))
            queryWrapper.lambda().eq(DictSite::getDictName, dictSite.getDictName());
        SortUtil.handleWrapperSort(request, queryWrapper, "orderNum", FebsConstant.ORDER_ASC, true);
        return this.baseMapper.selectList(queryWrapper);
	}

	@Override
	@Transactional
	public void createDictSite(DictSite dictSite) {
		Long parentId = dictSite.getParentId();
        if (parentId == null)
        	dictSite.setParentId(0L);
        dictSite.setCreateTime(new Date());
        this.save(dictSite);
	}

	@Override
	@Transactional
	public void updateDictSite(DictSite dictSite) {
		this.baseMapper.updateById(dictSite);
	}

	@Override
	@Transactional
	public void deleteDictSites(String[] dsIds) {
		delete(Arrays.asList(dsIds));
	}
	
	private List<DictSiteTree<DictSite>> convertDictSites(List<DictSite> dictSites){
        List<DictSiteTree<DictSite>> trees = new ArrayList<>();
        dictSites.forEach(dictSite -> {
        	DictSiteTree<DictSite> tree = new DictSiteTree<>();
            tree.setId(String.valueOf(dictSite.getDid()));
            tree.setParentId(String.valueOf(dictSite.getParentId()));
            tree.setName(dictSite.getDictName());
            tree.setData(dictSite);
            trees.add(tree);
        });
        return trees;
    }
	
	private void delete(List<String> dsIds) {
        removeByIds(dsIds);
        LambdaQueryWrapper<DictSite> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(DictSite::getParentId, dsIds);
        List<DictSite> dictSites = baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(dictSites)) {
            List<String> dseptIdList = new ArrayList<>();
            dictSites.forEach(d -> dseptIdList.add(String.valueOf(d.getDid())));
            this.delete(dseptIdList);
        }
    }

	@Override
	public List<DictSite> getListByParentId(DictSite dictSite) {
		return dictsitemapper.getByParentId(dictSite);
	}
	
	@Override
	public List<DictSiteTree<DictSite>> findParentDictSites(DictSite dictSite) {
		/*QueryWrapper<DictSite> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(String.valueOf(dictSite.getParentId())))
            queryWrapper.lambda().eq(DictSite::getParentId, dictSite.getParentId());
        queryWrapper.lambda().orderByAsc(DictSite::getOrderNum);*/

        List<DictSite> dictSites = this.baseMapper.getListByParentId(dictSite);
        List<DictSiteTree<DictSite>> trees =  this.convertDictSites(dictSites);
        return TreeUtil.buildDictSiteTree(trees);
	}

}
