package com.zjshell.pricing.dict.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjshell.pricing.dict.entity.ShellStation;
import com.zjshell.pricing.dict.mapper.ShellStationMapper;
import com.zjshell.pricing.dict.service.IShellStationService;


@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ShellStationServiceImpl extends ServiceImpl<ShellStationMapper, ShellStation> implements IShellStationService{

	@Override
	public List<ShellStation> findShellStation(ShellStation shellStation) {
		QueryWrapper<ShellStation> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(shellStation.getStationName()))
            queryWrapper.lambda().like(ShellStation::getStationName, shellStation.getStationName());
        return this.baseMapper.selectList(queryWrapper);
	}

	
}
