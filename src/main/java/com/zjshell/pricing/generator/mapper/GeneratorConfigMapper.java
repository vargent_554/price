package com.zjshell.pricing.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjshell.pricing.generator.entity.GeneratorConfig;

/**
 * @author YangLei
 */
public interface GeneratorConfigMapper extends BaseMapper<GeneratorConfig> {

}
