package com.zjshell.pricing.generator.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.generator.entity.Column;
import com.zjshell.pricing.generator.entity.Table;

import java.util.List;

/**
 * @author YangLei
 */
public interface IGeneratorService {

    List<String> getDatabases(String databaseType);

    IPage<Table> getTables(String tableName, QueryRequest request, String databaseType, String schemaName);

    List<Column> getColumns(String databaseType, String schemaName, String tableName);
}
