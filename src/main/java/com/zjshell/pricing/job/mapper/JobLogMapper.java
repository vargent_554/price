package com.zjshell.pricing.job.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjshell.pricing.job.entity.JobLog;

/**
 * @author YangLei
 */
public interface JobLogMapper extends BaseMapper<JobLog> {
}