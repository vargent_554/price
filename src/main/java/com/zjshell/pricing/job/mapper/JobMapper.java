package com.zjshell.pricing.job.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjshell.pricing.job.entity.Job;

import java.util.List;

/**
 * @author YangLei
 */
public interface JobMapper extends BaseMapper<Job> {
	
	List<Job> queryList();
}