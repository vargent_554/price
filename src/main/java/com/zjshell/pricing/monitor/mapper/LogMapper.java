package com.zjshell.pricing.monitor.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjshell.pricing.monitor.entity.SystemLog;

/**
 * @author YangLei
 */
public interface LogMapper extends BaseMapper<SystemLog> {

}
