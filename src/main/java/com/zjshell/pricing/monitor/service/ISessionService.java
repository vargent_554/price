package com.zjshell.pricing.monitor.service;

import java.util.List;

import com.zjshell.pricing.monitor.entity.ActiveUser;

/**
 * @author YangLei
 */
public interface ISessionService {

    /**
     * 获取在线用户列表
     *
     * @param username 用户名
     * @return List<ActiveUser>
     */
    List<ActiveUser> list(String username);

    /**
     * 踢出用户
     *
     * @param sessionId sessionId
     */
    void forceLogout(String sessionId);
}
