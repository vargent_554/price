package com.zjshell.pricing.others.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjshell.pricing.others.entity.Eximport;

/**
 * @author YangLei
 */
public interface EximportMapper extends BaseMapper<Eximport> {

}
