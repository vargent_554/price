package com.zjshell.pricing.price.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsConstant;
import com.zjshell.pricing.common.utils.FebsUtil;

@Controller("price")
@RequestMapping(FebsConstant.VIEW_PREFIX + "price")
public class ViewController extends BaseController{

	@GetMapping("oilprice")
    @RequiresPermissions("price:oilprice:view")
    public String oilPrice(Model model) {
		return FebsUtil.view("price/oil/oilPrice");
	}
	
	@GetMapping("oilprice/add")
    @RequiresPermissions("oilprice:add")
    public String oilPriceAdd(Model model) {
		return FebsUtil.view("price/oil/oilPriceAdd");
	}
}
