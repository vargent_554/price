package com.zjshell.pricing.price.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;

import lombok.Data;

@Data
@TableName("Prices_By_Date")
@Excel("油站字典")
public class EnvoryPrice implements Serializable{

	private static final long serialVersionUID = -6579896691474868253L;

	@TableId(value = "rowid", type = IdType.AUTO)
	private long rowid;

	@TableField("Store")
	private String store;
	
	@TableField("UPC_PLU")
	private String UpcPlu;
	
	@TableField("UOM")
	private String Uom;
	
	@TableField("From_Date")
	private String FromDate;
	
	@TableField("From_Time")
	private String FromTime;
	
	@TableField("Happy_Hour_From_Time")
	private String HappyHourFromTime;
	
	@TableField("Happy_Hour_To_Time")
	private String HappyHourToTime;
	
	@TableField("Price")
	private double Price;
	
	@TableField("Done_Date")
	private String DoneDate;
	
	@TableField("Done_Time")
	private String DoneTime;
	
	@TableField("Biz_User")
	private String BizUser;
	
	@TableField("TIMESTAMP")
	private Date TimesTamp;

	public long getRowid() {
		return rowid;
	}

	public void setRowid(long rowid) {
		this.rowid = rowid;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getUpcPlu() {
		return UpcPlu;
	}

	public void setUpcPlu(String upcPlu) {
		UpcPlu = upcPlu;
	}

	public String getUom() {
		return Uom;
	}

	public void setUom(String uom) {
		Uom = uom;
	}

	public String getFromDate() {
		return FromDate;
	}

	public void setFromDate(String fromDate) {
		FromDate = fromDate;
	}

	public String getFromTime() {
		return FromTime;
	}

	public void setFromTime(String fromTime) {
		FromTime = fromTime;
	}

	public String getHappyHourFromTime() {
		return HappyHourFromTime;
	}

	public void setHappyHourFromTime(String happyHourFromTime) {
		HappyHourFromTime = happyHourFromTime;
	}

	public String getHappyHourToTime() {
		return HappyHourToTime;
	}

	public void setHappyHourToTime(String happyHourToTime) {
		HappyHourToTime = happyHourToTime;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public String getDoneDate() {
		return DoneDate;
	}

	public void setDoneDate(String doneDate) {
		DoneDate = doneDate;
	}

	public String getDoneTime() {
		return DoneTime;
	}

	public void setDoneTime(String doneTime) {
		DoneTime = doneTime;
	}

	public String getBizUser() {
		return BizUser;
	}

	public void setBizUser(String bizUser) {
		BizUser = bizUser;
	}

	public Date getTimesTamp() {
		return TimesTamp;
	}

	public void setTimesTamp(Date timesTamp) {
		TimesTamp = timesTamp;
	}
	
	
}
