package com.zjshell.pricing.price.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;

import lombok.Data;

@Data
@TableName("t_oil_price")
@Excel("油品价格")
public class OilPrice implements Serializable{

	private static final long serialVersionUID = -2260971238031330709L;
	
	@TableField("id")
	private long id;
	
	@TableField("site_code")
	private String siteCode;
	
	@TableField("oil92")
	private double oil92;
	
	@TableField("oil95")
	private double oil95;
	
	@TableField("oil98")
	private double oil98;
	
	@TableField("oil0")
	private double oil0;
	
	@TableField("creat_time")
	private Date creatTime;
	
	@TableField("adduser")
	private String adduser;
	
	@TableField("price_type")
	private int priceType;
	
	@TableField("from_date")
	private String fromDate;
	
	@TableField("from_time")
	private String fromTime;
	
	@TableField("oa_checked")
	private int oaChecked;
	
	@TableField("fdId")
	private String fdId;
	
	@TableField("envory_update")
	private int envoryUpdate;
	
	@TableField("fileurl")
	private String fileUrl;
	
	@TableField("price_text")
	private String priceText;
	
	
	public String getPriceText() {
		return priceText;
	}

	public void setPriceText(String priceText) {
		this.priceText = priceText;
	}



	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}



	@TableField(exist = false)
    private String fromDateFrom;
	
    @TableField(exist = false)
    private String fromDateTo;

	public int getOaChecked() {
		return oaChecked;
	}

	public void setOaChecked(int oaChecked) {
		this.oaChecked = oaChecked;
	}

	public int getEnvoryUpdate() {
		return envoryUpdate;
	}

	public void setEnvoryUpdate(int envoryUpdate) {
		this.envoryUpdate = envoryUpdate;
	}

	public String getFdId() {
		return fdId;
	}

	public void setFdId(String fdId) {
		this.fdId = fdId;
	}
	
	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public double getOil92() {
		return oil92;
	}

	public void setOil92(double oil92) {
		this.oil92 = oil92;
	}

	public double getOil95() {
		return oil95;
	}

	public String getFromDateFrom() {
		return fromDateFrom;
	}

	public void setFromDateFrom(String fromDateFrom) {
		this.fromDateFrom = fromDateFrom;
	}

	public String getFromDateTo() {
		return fromDateTo;
	}

	public void setFromDateTo(String fromDateTo) {
		this.fromDateTo = fromDateTo;
	}

	public void setOil95(double oil95) {
		this.oil95 = oil95;
	}

	public double getOil98() {
		return oil98;
	}

	public void setOil98(double oil98) {
		this.oil98 = oil98;
	}

	public double getOil0() {
		return oil0;
	}

	public void setOil0(double oil0) {
		this.oil0 = oil0;
	}

	public Date getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public String getAdduser() {
		return adduser;
	}

	public void setAdduser(String adduser) {
		this.adduser = adduser;
	}

	public int getPriceType() {
		return priceType;
	}

	public void setPriceType(int priceType) {
		this.priceType = priceType;
	}
	
	


}
