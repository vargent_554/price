package com.zjshell.pricing.price.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjshell.pricing.price.entity.EnvoryPrice;

public interface EnvoryPriceMapper extends BaseMapper<EnvoryPrice> {

	void insertEnvoryPrice(EnvoryPrice envoryPrice);
}
