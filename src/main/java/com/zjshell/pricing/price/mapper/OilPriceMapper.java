package com.zjshell.pricing.price.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjshell.pricing.price.entity.OilPrice;

public interface OilPriceMapper extends BaseMapper<OilPrice> {

	List<OilPrice> findBySiteId(@Param("oilPrice") OilPrice oilPrice);
	
	OilPrice findById(@Param("oilPrice") OilPrice oilPrice);
	
	IPage<OilPrice> findOilPricePage(Page page, @Param("oilPrice") OilPrice oilPrice);
}
