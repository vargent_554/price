package com.zjshell.pricing.price.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjshell.pricing.price.entity.EnvoryPrice;

public interface IEnvoryPriceService extends IService<EnvoryPrice>{

	void insertEnvoryPrice(EnvoryPrice envoryPrice);
}
