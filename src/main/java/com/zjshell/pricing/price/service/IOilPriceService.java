package com.zjshell.pricing.price.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.price.entity.OilPrice;

public interface IOilPriceService  extends IService<OilPrice> {
	
	IPage<OilPrice> findOilPriceDetail(OilPrice oilPrice, QueryRequest request);

	OilPrice findById(OilPrice oilPrice);
	
	List<OilPrice> findBySiteId(OilPrice oilPrice);
	
	void createOilPrice(OilPrice oilPrice);
	
	void deleteOilPrices(String[] ids);
	
	void updateOilPrice(OilPrice oilPrice);
}
