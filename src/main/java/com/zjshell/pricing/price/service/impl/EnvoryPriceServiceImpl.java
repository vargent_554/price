package com.zjshell.pricing.price.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjshell.pricing.price.entity.EnvoryPrice;
import com.zjshell.pricing.price.mapper.EnvoryPriceMapper;
import com.zjshell.pricing.price.service.IEnvoryPriceService;

@Service
@DS("envory")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class EnvoryPriceServiceImpl extends ServiceImpl<EnvoryPriceMapper, EnvoryPrice> implements IEnvoryPriceService{

	@Autowired
	private EnvoryPriceMapper envoryPriceMapper;
	
	@Override
	public void insertEnvoryPrice(EnvoryPrice envoryPrice) {
		envoryPriceMapper.insertEnvoryPrice(envoryPrice);
	}

}
