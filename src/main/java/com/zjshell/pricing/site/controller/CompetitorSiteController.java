package com.zjshell.pricing.site.controller;

import java.util.Date;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.zjshell.pricing.common.annotation.ControllerEndpoint;
import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsResponse;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.exception.FebsException;
import com.zjshell.pricing.site.entity.CompetitorSite;
import com.zjshell.pricing.site.entity.ShellSite;
import com.zjshell.pricing.site.service.ICompetitorSiteService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 类名称:CompetitorSiteController
 * 类描述:  
 * 创建人:杨磊   
 * 修改人:XXX 
 * 修改时间:2019年12月23日 下午3:06:29
 * 修改备注:  
 * @version 1.0.0
 * Copyright(c)2019浙江壳牌-版权所有
 */
@Slf4j
@RestController
@RequestMapping("competitor")
public class CompetitorSiteController extends BaseController {
	
	@Autowired
    private ICompetitorSiteService competitorSiteService;
	

	@GetMapping("list")
	@ControllerEndpoint(exceptionMessage = "获取竞争对手油站信息失败")
    public FebsResponse competitorSiteList(CompetitorSite competitorSite, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.competitorSiteService.findCompetitorSiteDetail(competitorSite, request));
        return new FebsResponse().success().data(dataTable);
    }
	
	@GetMapping("listAll")
	@RequiresPermissions("competitor:listAll")
    public FebsResponse getCompetitorStations(CompetitorSite competitorSite) {
        return new FebsResponse().success().data(competitorSiteService.findCompetitorSiteDetail(competitorSite));
    }
	
	
	@PostMapping
    @RequiresPermissions("competitor:add")
    @ControllerEndpoint(operation = "新增竞争对手油站", exceptionMessage = "新增竞争对手油站失败")
    public FebsResponse addDictSite(@Valid CompetitorSite competitorSite) {
		competitorSite.setCreatTime(new Date());
		Long siteid = this.competitorSiteService.getCompetitorSiteId();
		competitorSite.setSiteid(siteid);
        this.competitorSiteService.createCompetitorSite(competitorSite);
        return new FebsResponse().success();
    }
	
	@PostMapping("update")
    @RequiresPermissions("competitor:update")
    @ControllerEndpoint(operation = "修改竞争对手油站", exceptionMessage = "修改竞争对手油站失败")
    public FebsResponse updateCompetitorSite(@Valid CompetitorSite competitorSite)  throws FebsException{
        this.competitorSiteService.updateById(competitorSite);
        return new FebsResponse().success();
    }

	 @GetMapping("delete/{siteids}")
	 @RequiresPermissions("competitor:delete")
	 @ControllerEndpoint(operation = "删除竞争对手油站", exceptionMessage = "删除竞争对手油站失败")
	 public FebsResponse deleteCompetitorSite(@NotBlank(message = "{required}") @PathVariable String siteids) throws FebsException {
	        String[] ids = siteids.split(StringPool.COMMA);
	        this.competitorSiteService.deleteCompetitorSites(ids);
	        return new FebsResponse().success();
	 }
	
}
