package com.zjshell.pricing.site.controller;

import java.util.Date;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.zjshell.pricing.common.annotation.ControllerEndpoint;
import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsResponse;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.exception.FebsException;
import com.zjshell.pricing.site.entity.ShellSite;
import com.zjshell.pricing.site.service.IShellSiteService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * 类名称:ShellSiteController
 * 类描述:  
 * 创建人:杨磊   
 * 修改人:XXX 
 * 修改时间:2019年12月23日 下午3:06:36
 * 修改备注:  
 * @version 1.0.0
 * Copyright(c)2019浙江壳牌-版权所有
 */
@Slf4j
@RestController
@RequestMapping("shell")
public class ShellSiteController extends BaseController {
	
	@Autowired
    private IShellSiteService shellSiteService;


	@GetMapping("list")
	@ControllerEndpoint(exceptionMessage = "获取壳牌油站信息失败")
    public FebsResponse shellSiteList(ShellSite shellSite, QueryRequest request) {
        Map<String, Object> dataTable = getDataTable(this.shellSiteService.findShellSiteDetail(shellSite, request));
        return new FebsResponse().success().data(dataTable);
    }
	
	@GetMapping("listAll")
	@ControllerEndpoint(exceptionMessage = "获取壳牌油站失败")
    public FebsResponse getShellStations() throws FebsException {
		ShellSite shellSite = new ShellSite();
        return new FebsResponse().success().data(shellSiteService.findShellSiteDetail(shellSite));
    }
	
	@GetMapping("listAll/{siteid}")
	@ControllerEndpoint(exceptionMessage = "获取壳牌油站失败")
    public FebsResponse getShellStations(@NotBlank(message = "{required}") @PathVariable Long siteid) throws FebsException {
		ShellSite shellSite = new ShellSite();
		shellSite.setSiteid(siteid);
        return new FebsResponse().success().data(shellSiteService.findShellSiteDetail(shellSite));
    }
	
	
	@PostMapping
    @RequiresPermissions("shell:add")
    @ControllerEndpoint(operation = "新增壳牌油站", exceptionMessage = "新增壳牌油站失败")
    public FebsResponse addShellSite(@Valid ShellSite shellSite)  throws FebsException{
		shellSite.setCreatTime(new Date());
		Long siteid = this.shellSiteService.getshellsiteId();
		shellSite.setSiteid(siteid);
        this.shellSiteService.createShellSite(shellSite);
        return new FebsResponse().success().data(siteid);
    }
	
	
	@PostMapping("update")
    @RequiresPermissions("shell:update")
    @ControllerEndpoint(operation = "修改壳牌油站", exceptionMessage = "修改壳牌油站失败")
    public FebsResponse updateShellSite(@Valid ShellSite shellSite)  throws FebsException{
        this.shellSiteService.updateById(shellSite);
        return new FebsResponse().success();
    }

	
	 @GetMapping("delete/{siteids}")
	 @RequiresPermissions("shell:delete")
	 @ControllerEndpoint(operation = "删除壳牌油站", exceptionMessage = "删除壳牌油站失败")
	 public FebsResponse deleteShellSite(@NotBlank(message = "{required}") @PathVariable String siteids) throws FebsException {
	        String[] ids = siteids.split(StringPool.COMMA);
	        this.shellSiteService.deleteShellSites(ids);
	        return new FebsResponse().success();
	 }
	
}
