package com.zjshell.pricing.site.controller;

import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.zjshell.pricing.common.annotation.ControllerEndpoint;
import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsResponse;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.exception.FebsException;
import com.zjshell.pricing.site.entity.ShellSiteInfo;
import com.zjshell.pricing.site.service.IShellSiteInfoService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("shellinfo")
public class ShellSiteInfoController  extends BaseController{

	@Autowired
    private IShellSiteInfoService shellSiteinfoService;
	
	@GetMapping("list/{siteid}/{did}")
	@ControllerEndpoint(exceptionMessage = "获取油站其它信息失败")
    public FebsResponse shellSiteInfoList(@PathVariable long siteid, @PathVariable long did, QueryRequest request) {
		ShellSiteInfo shellSiteInfo = new ShellSiteInfo();
		shellSiteInfo.setSiteid(siteid);
		shellSiteInfo.setDid(did);
        Map<String, Object> dataTable = getDataTable(this.shellSiteinfoService.findShellSiteInfoPage(shellSiteInfo, request));
        return new FebsResponse().success().data(dataTable);
    }
	
	
	@PostMapping
    @RequiresPermissions("shellinfo:add")
    @ControllerEndpoint(operation = "新增壳牌油站其它信息", exceptionMessage = "新增壳牌油站其它信息失败")
    public FebsResponse addShellSiteInfo(@Valid ShellSiteInfo shellSiteInfo)  throws FebsException{
        this.shellSiteinfoService.createShellSiteInfo(shellSiteInfo);
        return new FebsResponse().success();
    }
	
	 @GetMapping("delete/{ids}")
	 @RequiresPermissions("shellInfo:delete")
	 @ControllerEndpoint(operation = "删除壳牌油站其它信息", exceptionMessage = "删除壳牌油站其它信息失败")
	 public FebsResponse deleteShellSiteInfo(@NotBlank(message = "{required}") @PathVariable String ids) throws FebsException {
	        String[] idss = ids.split(StringPool.COMMA);
	        this.shellSiteinfoService.deleteShellSiteInfos(idss);
	        return new FebsResponse().success();
	 }
	 
}
