package com.zjshell.pricing.site.controller;

import java.util.Date;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.zjshell.pricing.common.annotation.ControllerEndpoint;
import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsResponse;
import com.zjshell.pricing.common.exception.FebsException;
import com.zjshell.pricing.site.entity.ShellSiteOil;
import com.zjshell.pricing.site.service.IShellSiteOilService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("shelloil")
public class ShellSiteOilController extends BaseController{

	@Autowired
    private IShellSiteOilService shellSiteOilService;
	
	
	@PostMapping
    @RequiresPermissions("shelloil:add")
    @ControllerEndpoint(operation = "新增壳牌油站油品信息", exceptionMessage = "新增壳牌油站油品信息失败")
    public FebsResponse addShellSiteOillInfo(@Valid ShellSiteOil shellSiteOil)  throws FebsException{
		shellSiteOil.setCreatTime(new Date());
        this.shellSiteOilService.createShellSiteOil(shellSiteOil);
        return new FebsResponse().success();
    }
	
	 @GetMapping("delete/{ids}")
	 @RequiresPermissions("shelloil:delete")
	 @ControllerEndpoint(operation = "删除壳牌油站油品信息", exceptionMessage = "删除壳牌油站油品信息失败")
	 public FebsResponse deleteShellSiteOilInfo(@NotBlank(message = "{required}") @PathVariable String ids) throws FebsException {
	        String[] idss = ids.split(StringPool.COMMA);
	        this.shellSiteOilService.deleteShellSiteOils(idss);
	        return new FebsResponse().success();
	 }

}
