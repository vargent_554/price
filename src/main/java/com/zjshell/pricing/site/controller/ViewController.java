package com.zjshell.pricing.site.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.zjshell.pricing.common.controller.BaseController;
import com.zjshell.pricing.common.entity.FebsConstant;
import com.zjshell.pricing.common.utils.FebsUtil;
import com.zjshell.pricing.dict.entity.DictSite;
import com.zjshell.pricing.dict.service.IDictSiteService;
import com.zjshell.pricing.site.entity.CompetitorSite;
import com.zjshell.pricing.site.entity.ShellSite;
import com.zjshell.pricing.site.entity.ShellSiteInfo;
import com.zjshell.pricing.site.entity.ShellSiteInfoList;
import com.zjshell.pricing.site.entity.ShellSiteOil;
import com.zjshell.pricing.site.service.ICompetitorSiteService;
import com.zjshell.pricing.site.service.IShellSiteInfoService;
import com.zjshell.pricing.site.service.IShellSiteOilService;
import com.zjshell.pricing.site.service.IShellSiteService;


/**
 * 
 * 类名称:ViewController
 * 类描述:  
 * 创建人:杨磊   
 * 修改人:XXX 
 * 修改时间:2019年10月22日 上午10:55:33
 * 修改备注:  
 * @version 1.0.0
 * Copyright(c)2019浙江壳牌-版权所有
 */
@Controller("site")
@RequestMapping(FebsConstant.VIEW_PREFIX + "site")
public class ViewController extends BaseController{

		@Autowired
		private IShellSiteService shellSiteService;
		
		@Autowired
		private ICompetitorSiteService competitorSiteService;
	
		@Autowired
	    private IShellSiteInfoService shellSiteinfoService;
		
		@Autowired
	    private IShellSiteOilService shellSiteOilService;
		
		@Autowired
	    private IDictSiteService dictSiteService;
	    
	    @GetMapping("shell")
	    @RequiresPermissions("site:shell:view")
	    public String shellsite() {
	        return FebsUtil.view("site/shell/site");
	    }
	    
	    @GetMapping("shell/add")
	    @RequiresPermissions("shell:add")
	    public String shellsiteAdd(Model model) {
	    	long id1 = 4;
	    	DictSite ds1 = new DictSite();
	    	ds1.setParentId(id1);
	    	List<DictSite> listsiteArea = dictSiteService.getListByParentId(ds1);
	    	
	    	long id2 = 20;
	    	DictSite ds2 = new DictSite();
	    	ds2.setParentId(id2);
	    	List<DictSite> listoilCode = dictSiteService.getListByParentId(ds2);
	    	
	    	long id3 = 7;
	    	DictSite ds3 = new DictSite();
	    	ds3.setParentId(id3);
	    	List<DictSite> listsiteTm = dictSiteService.getListByParentId(ds3);
	    	
	    	long id4 = 10;
	    	DictSite ds4 = new DictSite();
	    	ds4.setParentId(id4);
	    	List<DictSite> listroadType = dictSiteService.getListByParentId(ds4);
	    	
	    	long id5 = 3;
	    	DictSite ds5 = new DictSite();
	    	ds5.setParentId(id5);
	    	List<DictSite> listsiteType = dictSiteService.getListByParentId(ds5);
	    	
	    	long id6 = 13;
	    	DictSite ds6 = new DictSite();
	    	ds6.setParentId(id6);
	    	List<DictSite> listshopType = dictSiteService.getListByParentId(ds6);
	    	
	    	long id7 = 14;
	    	DictSite ds7 = new DictSite();
	    	ds7.setParentId(id7);
	    	List<DictSite> listserviceType = dictSiteService.getListByParentId(ds7);
	    	
	    	long id8 = 15;
	    	DictSite ds8 = new DictSite();
	    	ds8.setParentId(id8);
	    	List<DictSite> listoperateType = dictSiteService.getListByParentId(ds8);
	    	
	    	long id9 = 16;
	    	DictSite ds9 = new DictSite();
	    	ds9.setParentId(id9);
	    	List<DictSite> listsiteLevel = dictSiteService.getListByParentId(ds9);
	    	
	    	long id10 = 17;
	    	DictSite ds10 = new DictSite();
	    	ds10.setParentId(id10);
	    	List<DictSite> listsiteManagerLevel = dictSiteService.getListByParentId(ds10);
	    	
	    	long id11 = 18;
	    	DictSite ds11 = new DictSite();
	    	ds11.setParentId(id11);
	    	List<DictSite> listsiteFacilities = dictSiteService.getListByParentId(ds11);
	    	
	    	long id12 = 19;
	    	DictSite ds12 = new DictSite();
	    	ds12.setParentId(id12);
	    	List<DictSite> listpropertyType = dictSiteService.getListByParentId(ds12);
	    	
	    	
	    	
	    	model.addAttribute("listsiteArea", listsiteArea);
	    	model.addAttribute("listoilCode", listoilCode);
	    	model.addAttribute("listsiteTm", listsiteTm);
	    	model.addAttribute("listroadType", listroadType);
	    	model.addAttribute("listsiteType", listsiteType);
	    	model.addAttribute("listshopType", listshopType);
	    	model.addAttribute("listserviceType", listserviceType);
	    	model.addAttribute("listoperateType", listoperateType);
	    	model.addAttribute("listsiteLevel", listsiteLevel);
	    	model.addAttribute("listsiteManagerLevel", listsiteManagerLevel);
	    	model.addAttribute("listsiteFacilities", listsiteFacilities);
	    	model.addAttribute("listpropertyType", listpropertyType);
	    	
	        return FebsUtil.view("site/shell/siteAdd");
	    }
	    
	    @GetMapping("shell/update/{siteid}")
	    @RequiresPermissions("shell:update")
	    public String shellSiteUpdate(@PathVariable Long siteid, Model model) {
	    	
	    	long id1 = 4;
	    	DictSite ds1 = new DictSite();
	    	ds1.setParentId(id1);
	    	List<DictSite> listsiteArea = dictSiteService.getListByParentId(ds1);
	    	
	    	long id2 = 20;
	    	DictSite ds2 = new DictSite();
	    	ds2.setParentId(id2);
	    	List<DictSite> listoilCode = dictSiteService.getListByParentId(ds2);
	    	
	    	long id3 = 7;
	    	DictSite ds3 = new DictSite();
	    	ds3.setParentId(id3);
	    	List<DictSite> listsiteTm = dictSiteService.getListByParentId(ds3);
	    	
	    	long id4 = 10;
	    	DictSite ds4 = new DictSite();
	    	ds4.setParentId(id4);
	    	List<DictSite> listroadType = dictSiteService.getListByParentId(ds4);
	    	
	    	long id5 = 3;
	    	DictSite ds5 = new DictSite();
	    	ds5.setParentId(id5);
	    	List<DictSite> listsiteType = dictSiteService.getListByParentId(ds5);
	    	
	    	long id6 = 13;
	    	DictSite ds6 = new DictSite();
	    	ds6.setParentId(id6);
	    	List<DictSite> listshopType = dictSiteService.getListByParentId(ds6);
	    	
	    	long id7 = 14;
	    	DictSite ds7 = new DictSite();
	    	ds7.setParentId(id7);
	    	List<DictSite> listserviceType = dictSiteService.getListByParentId(ds7);
	    	
	    	long id8 = 15;
	    	DictSite ds8 = new DictSite();
	    	ds8.setParentId(id8);
	    	List<DictSite> listoperateType = dictSiteService.getListByParentId(ds8);
	    	
	    	long id9 = 16;
	    	DictSite ds9 = new DictSite();
	    	ds9.setParentId(id9);
	    	List<DictSite> listsiteLevel = dictSiteService.getListByParentId(ds9);
	    	
	    	long id10 = 17;
	    	DictSite ds10 = new DictSite();
	    	ds10.setParentId(id10);
	    	List<DictSite> listsiteManagerLevel = dictSiteService.getListByParentId(ds10);
	    	
	    	long id11 = 18;
	    	DictSite ds11 = new DictSite();
	    	ds11.setParentId(id11);
	    	List<DictSite> listsiteFacilities = dictSiteService.getListByParentId(ds11);
	    	
	    	long id12 = 19;
	    	DictSite ds12 = new DictSite();
	    	ds12.setParentId(id12);
	    	List<DictSite> listpropertyType = dictSiteService.getListByParentId(ds12);
	    	
	    	
	    	
	    	model.addAttribute("listsiteArea", listsiteArea);
	    	model.addAttribute("listoilCode", listoilCode);
	    	model.addAttribute("listsiteTm", listsiteTm);
	    	model.addAttribute("listroadType", listroadType);
	    	model.addAttribute("listsiteType", listsiteType);
	    	model.addAttribute("listshopType", listshopType);
	    	model.addAttribute("listserviceType", listserviceType);
	    	model.addAttribute("listoperateType", listoperateType);
	    	model.addAttribute("listsiteLevel", listsiteLevel);
	    	model.addAttribute("listsiteManagerLevel", listsiteManagerLevel);
	    	model.addAttribute("listsiteFacilities", listsiteFacilities);
	    	model.addAttribute("listpropertyType", listpropertyType);
	    	ShellSite shellSite = shellSiteService.findById(siteid);
	        model.addAttribute("shellsite", shellSite);
	        return FebsUtil.view("site/shell/siteUpdate");
	    }
	    
	    
	    @GetMapping("shell/siteView/{siteid}")
	    @RequiresPermissions("shell:siteView")
	    public String shellsiteView(@PathVariable Long siteid, Model model) {
	    	ShellSite shellSite = shellSiteService.findById(siteid);
	        model.addAttribute("shellsite", shellSite);
	        long id1 = 21;
	    	long id2 = 24;
	    	long id3 = 20;
	    	DictSite ds1 = new DictSite();
	    	ds1.setParentId(id1);
	    	DictSite ds2 = new DictSite();
	    	ds2.setParentId(id2);
	    	DictSite ds3 = new DictSite();
	    	ds3.setParentId(id3);
	    	List<DictSite> listoilcode = dictSiteService.getListByParentId(ds3);
	    	List<DictSite> listds1 = dictSiteService.getListByParentId(ds1);
	    	List<ShellSiteInfoList> listssil1 = new ArrayList<ShellSiteInfoList>();
	    	List<ShellSiteInfoList> listssil2 = new ArrayList<ShellSiteInfoList>();
	    	for (DictSite dictSite : listds1) {
	    		ShellSiteInfoList shellSiteInfoList = new ShellSiteInfoList();
	    		shellSiteInfoList.setDictSite(dictSite);
				ShellSiteInfo ssi = new ShellSiteInfo();
				ssi.setDid(dictSite.getDid());
				ssi.setSiteid(siteid);
				List<ShellSiteInfo> listssi = shellSiteinfoService.findBySiteId(ssi);
				shellSiteInfoList.setListssi(listssi);
				ShellSiteOil shellSiteOil = new ShellSiteOil();
				shellSiteOil.setSiteId(siteid);
				shellSiteOil.setDid(dictSite.getDid());
				List<ShellSiteOil> listsso = shellSiteOilService.findBySiteId(shellSiteOil);
				shellSiteInfoList.setListsso(listsso);
				listssil1.add(shellSiteInfoList);
			}
	    	List<DictSite> listds2 = dictSiteService.getListByParentId(ds2);
	    	for (DictSite dictSite : listds2) {
	    		ShellSiteInfoList shellSiteInfoList = new ShellSiteInfoList();
	    		shellSiteInfoList.setDictSite(dictSite);
				ShellSiteInfo ssi = new ShellSiteInfo();
				ssi.setDid(dictSite.getDid());
				ssi.setSiteid(siteid);
				List<ShellSiteInfo> listssi = shellSiteinfoService.findBySiteId(ssi);
				shellSiteInfoList.setListssi(listssi);
				ShellSiteOil shellSiteOil = new ShellSiteOil();
				shellSiteOil.setSiteId(siteid);
				shellSiteOil.setDid(dictSite.getDid());
				List<ShellSiteOil> listsso = shellSiteOilService.findBySiteId(shellSiteOil);
				shellSiteInfoList.setListsso(listsso);
				listssil2.add(shellSiteInfoList);
			}
	    	long ids1 = 4;
	    	DictSite dss1 = new DictSite();
	    	dss1.setParentId(ids1);
	    	List<DictSite> listsiteArea = dictSiteService.getListByParentId(dss1);
	    	
	    	long ids2 = 20;
	    	DictSite dss2 = new DictSite();
	    	dss2.setParentId(ids2);
	    	List<DictSite> listoilCode = dictSiteService.getListByParentId(dss2);
	    	
	    	long ids3 = 7;
	    	DictSite dss3 = new DictSite();
	    	dss3.setParentId(ids3);
	    	List<DictSite> listsiteTm = dictSiteService.getListByParentId(dss3);
	    	
	    	long id4 = 10;
	    	DictSite ds4 = new DictSite();
	    	ds4.setParentId(id4);
	    	List<DictSite> listroadType = dictSiteService.getListByParentId(ds4);
	    	
	    	long id5 = 3;
	    	DictSite ds5 = new DictSite();
	    	ds5.setParentId(id5);
	    	List<DictSite> listsiteType = dictSiteService.getListByParentId(ds5);
	    	
	    	long id6 = 13;
	    	DictSite ds6 = new DictSite();
	    	ds6.setParentId(id6);
	    	List<DictSite> listshopType = dictSiteService.getListByParentId(ds6);
	    	
	    	long id7 = 14;
	    	DictSite ds7 = new DictSite();
	    	ds7.setParentId(id7);
	    	List<DictSite> listserviceType = dictSiteService.getListByParentId(ds7);
	    	
	    	long id8 = 15;
	    	DictSite ds8 = new DictSite();
	    	ds8.setParentId(id8);
	    	List<DictSite> listoperateType = dictSiteService.getListByParentId(ds8);
	    	
	    	long id9 = 16;
	    	DictSite ds9 = new DictSite();
	    	ds9.setParentId(id9);
	    	List<DictSite> listsiteLevel = dictSiteService.getListByParentId(ds9);
	    	
	    	long id10 = 17;
	    	DictSite ds10 = new DictSite();
	    	ds10.setParentId(id10);
	    	List<DictSite> listsiteManagerLevel = dictSiteService.getListByParentId(ds10);
	    	
	    	long id11 = 18;
	    	DictSite ds11 = new DictSite();
	    	ds11.setParentId(id11);
	    	List<DictSite> listsiteFacilities = dictSiteService.getListByParentId(ds11);
	    	
	    	long id12 = 19;
	    	DictSite ds12 = new DictSite();
	    	ds12.setParentId(id12);
	    	List<DictSite> listpropertyType = dictSiteService.getListByParentId(ds12);
	    	
	    	
	    	
	    	model.addAttribute("listsiteArea", listsiteArea);
	    	model.addAttribute("listoilCode", listoilCode);
	    	model.addAttribute("listsiteTm", listsiteTm);
	    	model.addAttribute("listroadType", listroadType);
	    	model.addAttribute("listsiteType", listsiteType);
	    	model.addAttribute("listshopType", listshopType);
	    	model.addAttribute("listserviceType", listserviceType);
	    	model.addAttribute("listoperateType", listoperateType);
	    	model.addAttribute("listsiteLevel", listsiteLevel);
	    	model.addAttribute("listsiteManagerLevel", listsiteManagerLevel);
	    	model.addAttribute("listsiteFacilities", listsiteFacilities);
	    	model.addAttribute("listpropertyType", listpropertyType);
	    	model.addAttribute("listssil1", listssil1);
	    	model.addAttribute("listssil2", listssil2);
	    	model.addAttribute("siteid", siteid);
	    	model.addAttribute("listoilcode",listoilcode);
	        return FebsUtil.view("site/shell/siteView");
	    }

	    
	    @GetMapping("shell/info/{siteid}")
	    @RequiresPermissions("shell:info")
	    public String shellsiteInfoAdd(@PathVariable long siteid, Model model) {
	    	long id1 = 21;
	    	long id2 = 24;
	    	long id3 = 20;
	    	DictSite ds1 = new DictSite();
	    	ds1.setParentId(id1);
	    	DictSite ds2 = new DictSite();
	    	ds2.setParentId(id2);
	    	DictSite ds3 = new DictSite();
	    	ds3.setParentId(id3);
	    	List<DictSite> listoilcode = dictSiteService.getListByParentId(ds3);
	    	/*List<DictSite> listoilcode = new ArrayList<DictSite>();
	    	ShellSite shellSite = shellSiteService.findById(siteid);
	    	String oilcode = shellSite.getOilCode();
	    	if (oilcode.length()>0 && oilcode != null) {
	    		String[] ilStrings = oilcode.split(",");
		    	
		    	for(String x : ilStrings){
		    		DictSite dsoil = dictSiteService.getById(Long.parseLong(x));
		    		listoilcode.add(dsoil);
		    	}
			}*/
	    	List<DictSite> listds1 = dictSiteService.getListByParentId(ds1);
	    	List<ShellSiteInfoList> listssil1 = new ArrayList<ShellSiteInfoList>();
	    	List<ShellSiteInfoList> listssil2 = new ArrayList<ShellSiteInfoList>();
	    	for (DictSite dictSite : listds1) {
	    		ShellSiteInfoList shellSiteInfoList = new ShellSiteInfoList();
	    		shellSiteInfoList.setDictSite(dictSite);
				ShellSiteInfo ssi = new ShellSiteInfo();
				ssi.setDid(dictSite.getDid());
				ssi.setSiteid(siteid);
				List<ShellSiteInfo> listssi = shellSiteinfoService.findBySiteId(ssi);
				shellSiteInfoList.setListssi(listssi);
				ShellSiteOil shellSiteOil = new ShellSiteOil();
				shellSiteOil.setSiteId(siteid);
				shellSiteOil.setDid(dictSite.getDid());
				List<ShellSiteOil> listsso = shellSiteOilService.findBySiteId(shellSiteOil);
				shellSiteInfoList.setListsso(listsso);
				listssil1.add(shellSiteInfoList);
			}
	    	List<DictSite> listds2 = dictSiteService.getListByParentId(ds2);
	    	for (DictSite dictSite : listds2) {
	    		ShellSiteInfoList shellSiteInfoList = new ShellSiteInfoList();
	    		shellSiteInfoList.setDictSite(dictSite);
				ShellSiteInfo ssi = new ShellSiteInfo();
				ssi.setDid(dictSite.getDid());
				ssi.setSiteid(siteid);
				List<ShellSiteInfo> listssi = shellSiteinfoService.findBySiteId(ssi);
				shellSiteInfoList.setListssi(listssi);
				ShellSiteOil shellSiteOil = new ShellSiteOil();
				shellSiteOil.setSiteId(siteid);
				shellSiteOil.setDid(dictSite.getDid());
				List<ShellSiteOil> listsso = shellSiteOilService.findBySiteId(shellSiteOil);
				shellSiteInfoList.setListsso(listsso);
				listssil2.add(shellSiteInfoList);
			}
	    	model.addAttribute("listssil1", listssil1);
	    	model.addAttribute("listssil2", listssil2);
	    	model.addAttribute("siteid", siteid);
	    	model.addAttribute("listoilcode",listoilcode);
	        return FebsUtil.view("site/shell/siteInfo");
	    }
	    
	    @GetMapping("shell/oilinfoadd/{siteid}/{did}")
	    @RequiresPermissions("shelloil:add")
	    public String shellAddOilInfo(@PathVariable long siteid, @PathVariable long did,Model model) {
	    	ShellSiteOil shellSiteOil = new ShellSiteOil();
			shellSiteOil.setSiteId(siteid);
			shellSiteOil.setDid(did);
			List<ShellSiteOil> listsso = shellSiteOilService.findBySiteId(shellSiteOil);
			model.addAttribute("listsso",listsso);
	    	model.addAttribute("siteid",siteid);
	    	model.addAttribute("did",did);
	    	return FebsUtil.view("site/shell/siteOilAdd");
	    }
	    
	    @GetMapping("shell/infoadd/{siteid}/{did}")
	    @RequiresPermissions("shellinfo:add")
	    public String shellAddSiteInfo(@PathVariable long siteid, @PathVariable long did,Model model) {
	    	ShellSiteInfo ssi = new ShellSiteInfo();
			ssi.setDid(did);
			ssi.setSiteid(siteid);
			DictSite dictSite = dictSiteService.getById(did);
			List<ShellSiteInfo> listssi = shellSiteinfoService.findBySiteId(ssi);
			model.addAttribute("listssi",listssi);
	    	model.addAttribute("siteid",siteid);
	    	model.addAttribute("did",did);
	    	model.addAttribute("dictSite",dictSite);
	    	return FebsUtil.view("site/shell/siteInfoAdd");
	    }
	    
	    @GetMapping("competitor")
	    @RequiresPermissions("site:competitor:view")
	    public String competitorSiteList() {
	        return FebsUtil.view("site/competitor/site");
	    }
	    
	    @GetMapping("competitor/add")
	    @RequiresPermissions("competitor:add")
	    public String competitorsiteAdd(Model model) {
	    	long id1 = 4;
	    	DictSite ds1 = new DictSite();
	    	ds1.setParentId(id1);
	    	List<DictSite> listsiteArea = dictSiteService.getListByParentId(ds1);
	    	
	    	long id2 = 20;
	    	DictSite ds2 = new DictSite();
	    	ds2.setParentId(id2);
	    	List<DictSite> listoilCode = dictSiteService.getListByParentId(ds2);
	    	
	    	long id3 = 7;
	    	DictSite ds3 = new DictSite();
	    	ds3.setParentId(id3);
	    	List<DictSite> listsiteTm = dictSiteService.getListByParentId(ds3);
	    	
	    	long id4 = 10;
	    	DictSite ds4 = new DictSite();
	    	ds4.setParentId(id4);
	    	List<DictSite> listroadType = dictSiteService.getListByParentId(ds4);
	    	
	    	long id5 = 3;
	    	DictSite ds5 = new DictSite();
	    	ds5.setParentId(id5);
	    	List<DictSite> listsiteType = dictSiteService.getListByParentId(ds5);
	    	
	    	long id6 = 13;
	    	DictSite ds6 = new DictSite();
	    	ds6.setParentId(id6);
	    	List<DictSite> listshopType = dictSiteService.getListByParentId(ds6);
	    	
	    	long id7 = 14;
	    	DictSite ds7 = new DictSite();
	    	ds7.setParentId(id7);
	    	List<DictSite> listserviceType = dictSiteService.getListByParentId(ds7);
	    	
	    	long id8 = 15;
	    	DictSite ds8 = new DictSite();
	    	ds8.setParentId(id8);
	    	List<DictSite> listoperateType = dictSiteService.getListByParentId(ds8);
	    	
	    	long id9 = 16;
	    	DictSite ds9 = new DictSite();
	    	ds9.setParentId(id9);
	    	List<DictSite> listsiteLevel = dictSiteService.getListByParentId(ds9);
	    	
	    	long id10 = 17;
	    	DictSite ds10 = new DictSite();
	    	ds10.setParentId(id10);
	    	List<DictSite> listsiteManagerLevel = dictSiteService.getListByParentId(ds10);
	    	
	    	long id11 = 18;
	    	DictSite ds11 = new DictSite();
	    	ds11.setParentId(id11);
	    	List<DictSite> listsiteFacilities = dictSiteService.getListByParentId(ds11);
	    	
	    	long id12 = 19;
	    	DictSite ds12 = new DictSite();
	    	ds12.setParentId(id12);
	    	List<DictSite> listpropertyType = dictSiteService.getListByParentId(ds12);
	    	
	    	long id13 = 70;
	    	DictSite ds13 = new DictSite();
	    	ds13.setParentId(id13);
	    	List<DictSite> listSiteBrand = dictSiteService.getListByParentId(ds13);
	    	
	    	long id14 = 78;
	    	DictSite ds14 = new DictSite();
	    	ds14.setParentId(id14);
	    	List<DictSite> listSiteSide = dictSiteService.getListByParentId(ds14);
	    	
	    	List<ShellSite> listshellsite = shellSiteService.list();
	    	
	    	model.addAttribute("listsiteArea", listsiteArea);
	    	model.addAttribute("listoilCode", listoilCode);
	    	model.addAttribute("listsiteTm", listsiteTm);
	    	model.addAttribute("listroadType", listroadType);
	    	model.addAttribute("listsiteType", listsiteType);
	    	model.addAttribute("listshopType", listshopType);
	    	model.addAttribute("listserviceType", listserviceType);
	    	model.addAttribute("listoperateType", listoperateType);
	    	model.addAttribute("listsiteLevel", listsiteLevel);
	    	model.addAttribute("listshellsite", listshellsite);
	    	model.addAttribute("listSiteBrand", listSiteBrand);
	    	model.addAttribute("listSiteSide", listSiteSide);
	    	model.addAttribute("listsiteManagerLevel", listsiteManagerLevel);
	    	model.addAttribute("listsiteFacilities", listsiteFacilities);
	    	model.addAttribute("listpropertyType", listpropertyType);
	    	
	        return FebsUtil.view("site/competitor/siteAdd");
	    }
	    
	    
	    @GetMapping("competitor/update/{siteid}")
	    @RequiresPermissions("competitor:update")
	    public String competitorSiteUpdate(@PathVariable Long siteid, Model model) {
	    	
	    	long id1 = 4;
	    	DictSite ds1 = new DictSite();
	    	ds1.setParentId(id1);
	    	List<DictSite> listsiteArea = dictSiteService.getListByParentId(ds1);
	    	
	    	long id2 = 20;
	    	DictSite ds2 = new DictSite();
	    	ds2.setParentId(id2);
	    	List<DictSite> listoilCode = dictSiteService.getListByParentId(ds2);
	    	
	    	long id3 = 7;
	    	DictSite ds3 = new DictSite();
	    	ds3.setParentId(id3);
	    	List<DictSite> listsiteTm = dictSiteService.getListByParentId(ds3);
	    	
	    	long id4 = 10;
	    	DictSite ds4 = new DictSite();
	    	ds4.setParentId(id4);
	    	List<DictSite> listroadType = dictSiteService.getListByParentId(ds4);
	    	
	    	long id5 = 3;
	    	DictSite ds5 = new DictSite();
	    	ds5.setParentId(id5);
	    	List<DictSite> listsiteType = dictSiteService.getListByParentId(ds5);
	    	
	    	long id6 = 13;
	    	DictSite ds6 = new DictSite();
	    	ds6.setParentId(id6);
	    	List<DictSite> listshopType = dictSiteService.getListByParentId(ds6);
	    	
	    	long id7 = 14;
	    	DictSite ds7 = new DictSite();
	    	ds7.setParentId(id7);
	    	List<DictSite> listserviceType = dictSiteService.getListByParentId(ds7);
	    	
	    	long id8 = 15;
	    	DictSite ds8 = new DictSite();
	    	ds8.setParentId(id8);
	    	List<DictSite> listoperateType = dictSiteService.getListByParentId(ds8);
	    	
	    	long id9 = 16;
	    	DictSite ds9 = new DictSite();
	    	ds9.setParentId(id9);
	    	List<DictSite> listsiteLevel = dictSiteService.getListByParentId(ds9);
	    	
	    	long id10 = 17;
	    	DictSite ds10 = new DictSite();
	    	ds10.setParentId(id10);
	    	List<DictSite> listsiteManagerLevel = dictSiteService.getListByParentId(ds10);
	    	
	    	long id11 = 18;
	    	DictSite ds11 = new DictSite();
	    	ds11.setParentId(id11);
	    	List<DictSite> listsiteFacilities = dictSiteService.getListByParentId(ds11);
	    	
	    	long id12 = 19;
	    	DictSite ds12 = new DictSite();
	    	ds12.setParentId(id12);
	    	List<DictSite> listpropertyType = dictSiteService.getListByParentId(ds12);
	    	
	    	long id13 = 70;
	    	DictSite ds13 = new DictSite();
	    	ds13.setParentId(id13);
	    	List<DictSite> listSiteBrand = dictSiteService.getListByParentId(ds13);
	    	
	    	long id14 = 78;
	    	DictSite ds14 = new DictSite();
	    	ds14.setParentId(id14);
	    	List<DictSite> listSiteSide = dictSiteService.getListByParentId(ds14);
	    	
	    	List<ShellSite> listshellsite = shellSiteService.list();
	    	
	    	model.addAttribute("listsiteArea", listsiteArea);
	    	model.addAttribute("listoilCode", listoilCode);
	    	model.addAttribute("listsiteTm", listsiteTm);
	    	model.addAttribute("listroadType", listroadType);
	    	model.addAttribute("listsiteType", listsiteType);
	    	model.addAttribute("listshopType", listshopType);
	    	model.addAttribute("listserviceType", listserviceType);
	    	model.addAttribute("listoperateType", listoperateType);
	    	model.addAttribute("listsiteLevel", listsiteLevel);
	    	model.addAttribute("listsiteManagerLevel", listsiteManagerLevel);
	    	model.addAttribute("listsiteFacilities", listsiteFacilities);
	    	model.addAttribute("listpropertyType", listpropertyType);
	    	model.addAttribute("listSiteBrand", listSiteBrand);
	    	model.addAttribute("listSiteSide", listSiteSide);
	    	model.addAttribute("listshellsite", listshellsite);
	    	CompetitorSite competitorSite = competitorSiteService.findById(siteid);
	        model.addAttribute("competitorsite", competitorSite);
	        return FebsUtil.view("site/competitor/siteUpdate");
	    }
	    
	    @GetMapping("competitor/siteView/{siteid}")
	    @RequiresPermissions("competitor:siteView")
	    public String competitorSiteView(@PathVariable Long siteid, Model model) {
	    	
	    	long id1 = 4;
	    	DictSite ds1 = new DictSite();
	    	ds1.setParentId(id1);
	    	List<DictSite> listsiteArea = dictSiteService.getListByParentId(ds1);
	    	
	    	long id2 = 20;
	    	DictSite ds2 = new DictSite();
	    	ds2.setParentId(id2);
	    	List<DictSite> listoilCode = dictSiteService.getListByParentId(ds2);
	    	
	    	long id3 = 7;
	    	DictSite ds3 = new DictSite();
	    	ds3.setParentId(id3);
	    	List<DictSite> listsiteTm = dictSiteService.getListByParentId(ds3);
	    	
	    	long id4 = 10;
	    	DictSite ds4 = new DictSite();
	    	ds4.setParentId(id4);
	    	List<DictSite> listroadType = dictSiteService.getListByParentId(ds4);
	    	
	    	long id5 = 3;
	    	DictSite ds5 = new DictSite();
	    	ds5.setParentId(id5);
	    	List<DictSite> listsiteType = dictSiteService.getListByParentId(ds5);
	    	
	    	long id6 = 13;
	    	DictSite ds6 = new DictSite();
	    	ds6.setParentId(id6);
	    	List<DictSite> listshopType = dictSiteService.getListByParentId(ds6);
	    	
	    	long id7 = 14;
	    	DictSite ds7 = new DictSite();
	    	ds7.setParentId(id7);
	    	List<DictSite> listserviceType = dictSiteService.getListByParentId(ds7);
	    	
	    	long id8 = 15;
	    	DictSite ds8 = new DictSite();
	    	ds8.setParentId(id8);
	    	List<DictSite> listoperateType = dictSiteService.getListByParentId(ds8);
	    	
	    	long id9 = 16;
	    	DictSite ds9 = new DictSite();
	    	ds9.setParentId(id9);
	    	List<DictSite> listsiteLevel = dictSiteService.getListByParentId(ds9);
	    	
	    	long id10 = 17;
	    	DictSite ds10 = new DictSite();
	    	ds10.setParentId(id10);
	    	List<DictSite> listsiteManagerLevel = dictSiteService.getListByParentId(ds10);
	    	
	    	long id11 = 18;
	    	DictSite ds11 = new DictSite();
	    	ds11.setParentId(id11);
	    	List<DictSite> listsiteFacilities = dictSiteService.getListByParentId(ds11);
	    	
	    	long id12 = 19;
	    	DictSite ds12 = new DictSite();
	    	ds12.setParentId(id12);
	    	List<DictSite> listpropertyType = dictSiteService.getListByParentId(ds12);
	    	
	    	long id13 = 70;
	    	DictSite ds13 = new DictSite();
	    	ds13.setParentId(id13);
	    	List<DictSite> listSiteBrand = dictSiteService.getListByParentId(ds13);
	    	
	    	long id14 = 78;
	    	DictSite ds14 = new DictSite();
	    	ds14.setParentId(id14);
	    	List<DictSite> listSiteSide = dictSiteService.getListByParentId(ds14);
	    	
	    	List<ShellSite> listshellsite = shellSiteService.list();
	    	
	    	model.addAttribute("listsiteArea", listsiteArea);
	    	model.addAttribute("listoilCode", listoilCode);
	    	model.addAttribute("listsiteTm", listsiteTm);
	    	model.addAttribute("listroadType", listroadType);
	    	model.addAttribute("listsiteType", listsiteType);
	    	model.addAttribute("listshopType", listshopType);
	    	model.addAttribute("listserviceType", listserviceType);
	    	model.addAttribute("listoperateType", listoperateType);
	    	model.addAttribute("listsiteLevel", listsiteLevel);
	    	model.addAttribute("listsiteManagerLevel", listsiteManagerLevel);
	    	model.addAttribute("listsiteFacilities", listsiteFacilities);
	    	model.addAttribute("listpropertyType", listpropertyType);
	    	model.addAttribute("listSiteBrand", listSiteBrand);
	    	model.addAttribute("listSiteSide", listSiteSide);
	    	model.addAttribute("listshellsite", listshellsite);
	    	CompetitorSite competitorSite = competitorSiteService.findById(siteid);
	        model.addAttribute("competitorsite", competitorSite);
	        return FebsUtil.view("site/competitor/siteView");
	    }
	    
/*//	    油品添加
		@GetMapping("shell/siteoiladd/{siteid}/{did}")
		@RequiresPermissions("shell:oilinfoadd")
		public String shellAddSiteOil(@PathVariable long siteid, @PathVariable long did,Model model) {
			ShellSiteOil sso = new ShellSiteOil();
			sso.setDid(did);
			sso.setSiteId(siteid);
			List<ShellSiteOil> listsso = shellSiteOilService.findBySiteId(sso);
			model.addAttribute("listsso",listsso);
	    	model.addAttribute("siteid",siteid);
	    	model.addAttribute("did",did);
			return FebsUtil.view("site/shell/siteOilAdd");
		}
*/
	    /*private void resolveUserModel(String username, Model model, Boolean transform) {
	        User user = userService.findByName(username);
	        model.addAttribute("user", user);
	        if (transform) {
	            String ssex = user.getSex();
	            if (User.SEX_MALE.equals(ssex)) user.setSex("男");
	            else if (User.SEX_FEMALE.equals(ssex)) user.setSex("女");
	            else user.setSex("保密");
	        }
	        if (user.getLastLoginTime() != null)
	            model.addAttribute("lastLoginTime", DateUtil.getDateFormat(user.getLastLoginTime(), DateUtil.FULL_TIME_SPLIT_PATTERN));
	    }*/
}
