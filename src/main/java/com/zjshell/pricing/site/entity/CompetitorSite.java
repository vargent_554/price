package com.zjshell.pricing.site.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;

import lombok.Data;

@Data
@TableName("t_site_competitor")
@Excel("竞争油站基本信息")
public class CompetitorSite implements Serializable{

	private static final long serialVersionUID = -9187791724435440479L;

	//@TableId(value = "siteid", type = IdType.AUTO)
	@TableId(value = "siteid")
    private Long siteid;
	
	@TableField("site_name")
	private String siteName;
	
	@TableField("longitude")
	private String longitude;
	
	@TableField("latitude")
	private String latitude;
	
	@TableField("site_address")
	private String siteAddress;
	
	@TableField("site_group")
	private Long siteGroup;
	
	@TableField("site_type")
	private Long siteType;
	
	@TableField(exist = false)
	private String siteTypeName;
	
	@TableField("open_date")
	private String openDate;
	
	@TableField(exist = false)
    private String openDateFrom;
	
    @TableField(exist = false)
    private String openDateTo;
	
	@TableField("trade_area")
	private Long tradeArea;
	
	@TableField("site_area")
	private Long siteArea;
	
	@TableField(exist = false)
	private String siteAreaName;
	
	@TableField("service_type")
	private Long serviceType;
	
	@TableField("site_level")
	private Long siteLevel;
	
	@TableField("site_manager_level")
	private Long siteManagerLevel;
	
	@TableField("site_facilities")
	private Long siteFacilities;
	
	@TableField("road_type")
	private Long roadType;
	
	@TableField("site_code")
	private String siteCode;
	
	@TableField("status")
	private int status;
	
	@TableField("member_ratio")
	private String memberRatio;
	
	@TableField("sales_ratio")
	private String salesRatio;
	
	@TableField("creat_time")
	private Date creatTime;
	
	@TableField("property_type")
	private long propertyType;
	
	@TableField("np_trade_area")
	private String npTradeArea;
	
	@TableField("oil_code")
	private String oilCode;
	
	@TableField("site_manager_name")
	private String siteManagerName;
	
	@TableField("site_manager_phone")
	private String siteManagerPhone;
	
	@TableField("site_phone")
	private String sitePhone;
	
	@TableField("traffic_flow")
	private String trafficFlow;
	
	@TableField("site_service_area")
	private String siteServiceArea;
	
	@TableField("oil_tank")
	private String oilTank;
	
	@TableField("oil_island")
	private String oilIsland;
	
	@TableField("oil_gun_92")
	private String oilGun92;
	
	@TableField("oil_gun_0")
	private String oilGun0;
	
	@TableField("oil_gun_95")
	private String oilGun95;
	
	@TableField("compare_site")
	private String compareSite;

	@TableField("service_area")
	private int serviceArea;
	
	@TableField("site_brand")
	private long siteBrand;
	
	@TableField("shell_siteid")
	private long shellSiteid;
	
	@TableField(exist = false)
	private String shellSiteName;
	
	@TableField("operate_type")
	private long operateType;
	
	@TableField("distance")
	private String distance;
	
	@TableField("supplier")
	private String supplier;
	
	@TableField("site_side")
	private long siteSide;
	
	@TableField(exist = false)
	private String siteSideName;
	
	@TableField("site_tags")
	private String siteTags;
	
	@TableField(exist = false)
	private String siteTagNames;
	
	
	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getSiteTagNames() {
		return siteTagNames;
	}

	public void setSiteTagNames(String siteTagNames) {
		this.siteTagNames = siteTagNames;
	}

	public String getSiteSideName() {
		return siteSideName;
	}

	public void setSiteSideName(String siteSideName) {
		this.siteSideName = siteSideName;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public long getSiteSide() {
		return siteSide;
	}

	public void setSiteSide(long siteSide) {
		this.siteSide = siteSide;
	}

	public String getSiteTags() {
		return siteTags;
	}

	public void setSiteTags(String siteTags) {
		this.siteTags = siteTags;
	}

	public long getOperateType() {
		return operateType;
	}

	public void setOperateType(long operateType) {
		this.operateType = operateType;
	}

	public String getOpenDateFrom() {
		return openDateFrom;
	}

	public void setOpenDateFrom(String openDateFrom) {
		this.openDateFrom = openDateFrom;
	}

	public String getOpenDateTo() {
		return openDateTo;
	}

	public void setOpenDateTo(String openDateTo) {
		this.openDateTo = openDateTo;
	}

	public long getSiteBrand() {
		return siteBrand;
	}

	public void setSiteBrand(long siteBrand) {
		this.siteBrand = siteBrand;
	}

	public String getNpTradeArea() {
		return npTradeArea;
	}

	public void setNpTradeArea(String npTradeArea) {
		this.npTradeArea = npTradeArea;
	}
	
	public String getSiteTypeName() {
		return siteTypeName;
	}

	public void setSiteTypeName(String siteTypeName) {
		this.siteTypeName = siteTypeName;
	}

	public String getSiteManagerPhone() {
		return siteManagerPhone;
	}

	public void setSiteManagerPhone(String siteManagerPhone) {
		this.siteManagerPhone = siteManagerPhone;
	}

	public int getServiceArea() {
		return serviceArea;
	}

	public void setServiceArea(int serviceArea) {
		this.serviceArea = serviceArea;
	}

	public String getShellSiteName() {
		return shellSiteName;
	}

	public void setShellSiteName(String shellSiteName) {
		this.shellSiteName = shellSiteName;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	
	public String getSiteAreaName() {
		return siteAreaName;
	}

	public void setSiteAreaName(String siteAreaName) {
		this.siteAreaName = siteAreaName;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public Long getSiteGroup() {
		return siteGroup;
	}

	public void setSiteGroup(Long siteGroup) {
		this.siteGroup = siteGroup;
	}

	public Long getSiteType() {
		return siteType;
	}

	public void setSiteType(Long siteType) {
		this.siteType = siteType;
	}

	public Long getTradeArea() {
		return tradeArea;
	}

	public void setTradeArea(Long tradeArea) {
		this.tradeArea = tradeArea;
	}

	public Long getSiteArea() {
		return siteArea;
	}

	public void setSiteArea(Long siteArea) {
		this.siteArea = siteArea;
	}

	public Long getServiceType() {
		return serviceType;
	}

	public void setServiceType(Long serviceType) {
		this.serviceType = serviceType;
	}

	public Long getSiteLevel() {
		return siteLevel;
	}

	public void setSiteLevel(Long siteLevel) {
		this.siteLevel = siteLevel;
	}

	public Long getSiteFacilities() {
		return siteFacilities;
	}

	public void setSiteFacilities(Long siteFacilities) {
		this.siteFacilities = siteFacilities;
	}

	public Long getRoadType() {
		return roadType;
	}

	public void setRoadType(Long roadType) {
		this.roadType = roadType;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getMemberRatio() {
		return memberRatio;
	}

	public void setMemberRatio(String memberRatio) {
		this.memberRatio = memberRatio;
	}

	public String getSalesRatio() {
		return salesRatio;
	}

	public void setSalesRatio(String salesRatio) {
		this.salesRatio = salesRatio;
	}

	public Date getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public long getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(long propertyType) {
		this.propertyType = propertyType;
	}

	public String getOilCode() {
		return oilCode;
	}

	public void setOilCode(String oilCode) {
		this.oilCode = oilCode;
	}

	public String getSitePhone() {
		return sitePhone;
	}

	public void setSitePhone(String sitePhone) {
		this.sitePhone = sitePhone;
	}

	public String getTrafficFlow() {
		return trafficFlow;
	}

	public void setTrafficFlow(String trafficFlow) {
		this.trafficFlow = trafficFlow;
	}

	public String getSiteServiceArea() {
		return siteServiceArea;
	}

	public void setSiteServiceArea(String siteServiceArea) {
		this.siteServiceArea = siteServiceArea;
	}

	public String getOilTank() {
		return oilTank;
	}

	public void setOilTank(String oilTank) {
		this.oilTank = oilTank;
	}

	public String getOilIsland() {
		return oilIsland;
	}

	public void setOilIsland(String oilIsland) {
		this.oilIsland = oilIsland;
	}

	public String getOilGun92() {
		return oilGun92;
	}

	public void setOilGun92(String oilGun92) {
		this.oilGun92 = oilGun92;
	}

	public String getOilGun0() {
		return oilGun0;
	}

	public void setOilGun0(String oilGun0) {
		this.oilGun0 = oilGun0;
	}

	public String getOilGun95() {
		return oilGun95;
	}

	public void setOilGun95(String oilGun95) {
		this.oilGun95 = oilGun95;
	}

	public String getCompareSite() {
		return compareSite;
	}

	public void setCompareSite(String compareSite) {
		this.compareSite = compareSite;
	}

	public String getOpenDate() {
		return openDate;
	}

	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}

	public Long getSiteManagerLevel() {
		return siteManagerLevel;
	}

	public void setSiteManagerLevel(Long siteManagerLevel) {
		this.siteManagerLevel = siteManagerLevel;
	}

	public String getSiteManagerName() {
		return siteManagerName;
	}

	public void setSiteManagerName(String siteManagerName) {
		this.siteManagerName = siteManagerName;
	}

	public long getShellSiteid() {
		return shellSiteid;
	}

	public void setShellSiteid(long shellSiteid) {
		this.shellSiteid = shellSiteid;
	}

}
