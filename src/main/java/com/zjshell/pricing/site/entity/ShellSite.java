package com.zjshell.pricing.site.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;

import lombok.Data;

@Data
@TableName("t_site_shell")
@Excel("壳牌油站基本信息")
public class ShellSite implements Serializable{

	private static final long serialVersionUID = -9187791724435440479L;

	//@TableId(value = "siteid", type = IdType.AUTO)
	@TableId(value = "siteid")
    private Long siteid;
	
	@TableField("site_name")
	private String siteName;
	
	@TableField("longitude")
	private String longitude;
	
	@TableField("latitude")
	private String latitude;
	
	@TableField("site_address")
	private String siteAddress;
	
	@TableField("site_group")
	private Long siteGroup;
	
	@TableField("site_type")
	private Long siteType;
	
	@TableField(exist = false)
	private String siteTypeName;
	
	@TableField("shop_type")
	private Long shopType;
	
	@TableField("open_date")
	private String openDate;
	
	@TableField(exist = false)
    private String openDateFrom;
	
    @TableField(exist = false)
    private String openDateTo;
	
	@TableField("area_manager")
	private Long areaManager;
	
	@TableField("trade_area")
	private Long tradeArea;
	
	@TableField("site_area")
	private Long siteArea;
	
	@TableField(exist = false)
	private String siteAreaName;
	
	@TableField("service_type")
	private Long serviceType;
	
	@TableField("site_level")
	private Long siteLevel;
	
	@TableField("site_manager_level")
	private Long siteManagerLevel;
	
	@TableField("site_facilities")
	private Long siteFacilities;
	
	@TableField("road_type")
	private Long roadType;
	
	@TableField("active_area")
	private Long activeArea;
	
	@TableField("site_code")
	private String siteCode;
	
	@TableField("np_traffic_flow")
	private String npTrafficFlow;
	
	@TableField("y0")
	private String y0;
	
	@TableField("y1")
	private String y1;
	
	@TableField("y2")
	private String y2;
	
	@TableField("status")
	private int status;
	
	@TableField("ripe_year")
	private String ripeYear;
	
	@TableField("operate_type")
	private Long operateType;
	
	@TableField("member_ratio")
	private String memberRatio;
	
	@TableField("sales_ratio")
	private String salesRatio;
	
	@TableField("creat_time")
	private Date creatTime;
	
	@TableField("supplier")
	private String supplier;
	
	@TableField("property_type")
	private long propertyType;
	
	@TableField("np_visibility")
	private String npVisibility;
	
	@TableField("np_accessible")
	private String npAccessible;
	
	@TableField("np_trade_area")
	private String npTradeArea;
	
	@TableField("oil_code")
	private String oilCode;
	
	@TableField(exist = false)
	private String siteTmName;
	
	@TableField("site_tm")
	private Long siteTm;
	
	@TableField("site_manager_name")
	private String siteManagerName;
	
	@TableField("site_manager_phone")
	private String siteManagerPhone;
	
	@TableField("site_phone")
	private String sitePhone;
	
	@TableField("traffic_flow")
	private String trafficFlow;
	
	@TableField("site_service_area")
	private String siteServiceArea;
	
	@TableField("oil_tank")
	private String oilTank;
	
	@TableField("oil_island")
	private String oilIsland;
	
	@TableField("oil_gun_92")
	private String oilGun92;
	
	@TableField("oil_gun_0")
	private String oilGun0;
	
	@TableField("oil_gun_95")
	private String oilGun95;
	
	@TableField("compare_site")
	private String compareSite;
	
	@TableField("opex")
	private String opex;
	
	@TableField("turn_in_92")
	private String turnIn92;
	
	@TableField("turn_in_95")
	private String turnIn95;
	
	@TableField("service_area")
	private String serviceArea;

	@TableField("site_brand")
	private long siteBrand;
	
	@TableField(exist = false)
	private int ShellCompare;
	
	public String getSiteTmName() {
		return siteTmName;
	}

	public void setSiteTmName(String siteTmName) {
		this.siteTmName = siteTmName;
	}

	public int getShellCompare() {
		return ShellCompare;
	}

	public void setShellCompare(int shellCompare) {
		ShellCompare = shellCompare;
	}

	public String getOpenDateFrom() {
		return openDateFrom;
	}

	public void setOpenDateFrom(String openDateFrom) {
		this.openDateFrom = openDateFrom;
	}

	public String getOpenDateTo() {
		return openDateTo;
	}

	public void setOpenDateTo(String openDateTo) {
		this.openDateTo = openDateTo;
	}

	public long getSiteBrand() {
		return siteBrand;
	}

	public void setSiteBrand(long siteBrand) {
		this.siteBrand = siteBrand;
	}

	public String getServiceArea() {
		return serviceArea;
	}

	public void setServiceArea(String serviceArea) {
		this.serviceArea = serviceArea;
	}

	public Long getSiteid() {
		return siteid;
	}

	public void setSiteid(Long siteid) {
		this.siteid = siteid;
	}
	
	public String getSiteAreaName() {
		return siteAreaName;
	}

	public void setSiteAreaName(String siteAreaName) {
		this.siteAreaName = siteAreaName;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public Long getSiteGroup() {
		return siteGroup;
	}

	public void setSiteGroup(Long siteGroup) {
		this.siteGroup = siteGroup;
	}

	public Long getSiteType() {
		return siteType;
	}

	public void setSiteType(Long siteType) {
		this.siteType = siteType;
	}

	public Long getShopType() {
		return shopType;
	}

	public void setShopType(Long shopType) {
		this.shopType = shopType;
	}

	public Long getAreaManager() {
		return areaManager;
	}

	public void setAreaManager(Long areaManager) {
		this.areaManager = areaManager;
	}

	public Long getTradeArea() {
		return tradeArea;
	}

	public void setTradeArea(Long tradeArea) {
		this.tradeArea = tradeArea;
	}

	public Long getSiteArea() {
		return siteArea;
	}

	public void setSiteArea(Long siteArea) {
		this.siteArea = siteArea;
	}

	public Long getServiceType() {
		return serviceType;
	}

	public void setServiceType(Long serviceType) {
		this.serviceType = serviceType;
	}

	public Long getSiteLevel() {
		return siteLevel;
	}

	public void setSiteLevel(Long siteLevel) {
		this.siteLevel = siteLevel;
	}

	public Long getSiteManagerLevel() {
		return siteManagerLevel;
	}

	public void setSiteManagerLevel(Long siteManagerLevel) {
		this.siteManagerLevel = siteManagerLevel;
	}

	public Long getSiteFacilities() {
		return siteFacilities;
	}

	public void setSiteFacilities(Long siteFacilities) {
		this.siteFacilities = siteFacilities;
	}

	public Long getRoadType() {
		return roadType;
	}

	public void setRoadType(Long roadType) {
		this.roadType = roadType;
	}

	public Long getActiveArea() {
		return activeArea;
	}

	public void setActiveArea(Long activeArea) {
		this.activeArea = activeArea;
	}

	public String getSiteCode() {
		return siteCode;
	}

	public void setSiteCode(String siteCode) {
		this.siteCode = siteCode;
	}

	public String getNpTrafficFlow() {
		return npTrafficFlow;
	}

	public void setNpTrafficFlow(String npTrafficFlow) {
		this.npTrafficFlow = npTrafficFlow;
	}

	public String getY0() {
		return y0;
	}

	public void setY0(String y0) {
		this.y0 = y0;
	}

	public String getY1() {
		return y1;
	}

	public void setY1(String y1) {
		this.y1 = y1;
	}

	public String getY2() {
		return y2;
	}

	public void setY2(String y2) {
		this.y2 = y2;
	}

	public String getRipeYear() {
		return ripeYear;
	}

	public void setRipeYear(String ripeYear) {
		this.ripeYear = ripeYear;
	}

	public Long getOperateType() {
		return operateType;
	}

	public void setOperateType(Long operateType) {
		this.operateType = operateType;
	}

	public String getMemberRatio() {
		return memberRatio;
	}

	public void setMemberRatio(String memberRatio) {
		this.memberRatio = memberRatio;
	}

	public String getSalesRatio() {
		return salesRatio;
	}

	public void setSalesRatio(String salesRatio) {
		this.salesRatio = salesRatio;
	}

	public Date getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public long getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(long propertyType) {
		this.propertyType = propertyType;
	}

	public String getNpVisibility() {
		return npVisibility;
	}

	public void setNpVisibility(String npVisibility) {
		this.npVisibility = npVisibility;
	}

	public String getNpAccessible() {
		return npAccessible;
	}

	public void setNpAccessible(String npAccessible) {
		this.npAccessible = npAccessible;
	}

	public String getNpTradeArea() {
		return npTradeArea;
	}

	public void setNpTradeArea(String npTradeArea) {
		this.npTradeArea = npTradeArea;
	}

	public String getOilCode() {
		return oilCode;
	}

	public void setOilCode(String oilCode) {
		this.oilCode = oilCode;
	}

	public Long getSiteTm() {
		return siteTm;
	}

	public void setSiteTm(Long siteTm) {
		this.siteTm = siteTm;
	}

	public String getSiteManagerName() {
		return siteManagerName;
	}

	public void setSiteManagerName(String siteManagerName) {
		this.siteManagerName = siteManagerName;
	}

	public String getSiteManagerPhone() {
		return siteManagerPhone;
	}

	public void setSiteManagerPhone(String siteManagerPhone) {
		this.siteManagerPhone = siteManagerPhone;
	}

	public String getSitePhone() {
		return sitePhone;
	}

	public void setSitePhone(String sitePhone) {
		this.sitePhone = sitePhone;
	}

	public String getTrafficFlow() {
		return trafficFlow;
	}

	public void setTrafficFlow(String trafficFlow) {
		this.trafficFlow = trafficFlow;
	}

	public String getSiteServiceArea() {
		return siteServiceArea;
	}

	public void setSiteServiceArea(String siteServiceArea) {
		this.siteServiceArea = siteServiceArea;
	}

	public String getOilTank() {
		return oilTank;
	}

	public void setOilTank(String oilTank) {
		this.oilTank = oilTank;
	}

	public String getOilIsland() {
		return oilIsland;
	}

	public void setOilIsland(String oilIsland) {
		this.oilIsland = oilIsland;
	}

	public String getOilGun92() {
		return oilGun92;
	}

	public void setOilGun92(String oilGun92) {
		this.oilGun92 = oilGun92;
	}

	public String getOilGun0() {
		return oilGun0;
	}

	public void setOilGun0(String oilGun0) {
		this.oilGun0 = oilGun0;
	}

	public String getOilGun95() {
		return oilGun95;
	}

	public void setOilGun95(String oilGun95) {
		this.oilGun95 = oilGun95;
	}

	public String getCompareSite() {
		return compareSite;
	}

	public void setCompareSite(String compareSite) {
		this.compareSite = compareSite;
	}

	public String getOpex() {
		return opex;
	}

	public void setOpex(String opex) {
		this.opex = opex;
	}

	public String getTurnIn92() {
		return turnIn92;
	}

	public void setTurnIn92(String turnIn92) {
		this.turnIn92 = turnIn92;
	}

	public String getTurnIn95() {
		return turnIn95;
	}

	public void setTurnIn95(String turnIn95) {
		this.turnIn95 = turnIn95;
	}

	public String getSiteTypeName() {
		return siteTypeName;
	}

	public void setSiteTypeName(String siteTypeName) {
		this.siteTypeName = siteTypeName;
	}

	public String getOpenDate() {
		return openDate;
	}

	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}

	/*private List<ShellSiteInfo> listssi;
	
	private List<ShellSitePs> listsp;*/
	
	

	
	
	
	
}
