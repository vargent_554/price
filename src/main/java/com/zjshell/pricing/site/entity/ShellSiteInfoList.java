package com.zjshell.pricing.site.entity;

import java.io.Serializable;
import java.util.List;

import com.zjshell.pricing.dict.entity.DictSite;

import lombok.Data;

@Data
public class ShellSiteInfoList implements Serializable{

	private static final long serialVersionUID = 172118643042307665L;

	private DictSite dictSite;
	
	private List<ShellSiteInfo> listssi;
	
	private List<ShellSitePs> listssp;
	
	private List<ShellSiteOil> listsso;

	public DictSite getDictSite() {
		return dictSite;
	}

	public void setDictSite(DictSite dictSite) {
		this.dictSite = dictSite;
	}

	public List<ShellSiteInfo> getListssi() {
		return listssi;
	}

	public void setListssi(List<ShellSiteInfo> listssi) {
		this.listssi = listssi;
	}

	public List<ShellSitePs> getListssp() {
		return listssp;
	}

	public void setListssp(List<ShellSitePs> listssp) {
		this.listssp = listssp;
	}

	public List<ShellSiteOil> getListsso() {
		return listsso;
	}

	public void setListsso(List<ShellSiteOil> listsso) {
		this.listsso = listsso;
	}
	
	
	
}
