package com.zjshell.pricing.site.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;

import lombok.Data;

@Data
@TableName("t_site_shell_oil")
@Excel("壳牌油品多条信息")
public class ShellSiteOil implements Serializable{

	private static final long serialVersionUID = -6284423565242896653L;
	
	@TableId(value = "id", type = IdType.AUTO)
	private long id;
	
	@TableField("did")
	private long did;
	
	@TableField("oil92")
	private double oil92;
	
	@TableField("oil95")
	private double oil95;
	
	@TableField("oil98")
	private double oil98;
	
	@TableField("oil0")
	private double oil0;
	
	@TableField("start_date")
	private String startDate;
	
	@TableField("end_date")
	private String endDate;
	
	@TableField("info_value")
	private String infoValue;
	
	@TableField("creat_time")
	private Date creatTime;
	
	@TableField("siteid")
	private long siteId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDid() {
		return did;
	}

	public void setDid(long did) {
		this.did = did;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getInfoValue() {
		return infoValue;
	}

	public void setInfoValue(String infoValue) {
		this.infoValue = infoValue;
	}

	public Date getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public long getSiteId() {
		return siteId;
	}

	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}

	public double getOil92() {
		return oil92;
	}

	public void setOil92(double oil92) {
		this.oil92 = oil92;
	}

	public double getOil95() {
		return oil95;
	}

	public void setOil95(double oil95) {
		this.oil95 = oil95;
	}

	public double getOil98() {
		return oil98;
	}

	public void setOil98(double oil98) {
		this.oil98 = oil98;
	}

	public double getOil0() {
		return oil0;
	}

	public void setOil0(double oil0) {
		this.oil0 = oil0;
	}
	
	
	

}
