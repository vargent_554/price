package com.zjshell.pricing.site.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wuwenze.poi.annotation.Excel;

import lombok.Data;

@Data
@TableName("t_site_shell_ps")
@Excel("壳牌油站pricing statement")
public class ShellSitePs implements Serializable{

	private static final long serialVersionUID = 4436798453628800791L;
	
	@TableId(value = "id", type = IdType.AUTO)
    private Long id;
	
	@TableField("siteid")
	private long siteid;
}
