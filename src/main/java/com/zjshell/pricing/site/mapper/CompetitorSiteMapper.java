package com.zjshell.pricing.site.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjshell.pricing.site.entity.CompetitorSite;


public interface CompetitorSiteMapper extends BaseMapper<CompetitorSite>  {

	CompetitorSite findById(Long siteid);
	
	IPage<CompetitorSite> findCompetitorSiteDetailPage(Page page, @Param("competitorSite") CompetitorSite competitorSite);
	
	List<CompetitorSite> findCompetitorSiteDetail(@Param("competitorSite") CompetitorSite competitorSite);
	
	Long getCompetitorSiteId();
	
	//匹配code名称查询油站
	List<CompetitorSite> findCompetitorListMatchCodeName(@Param("competitorSite")CompetitorSite competitorSite);

	CompetitorSite findEntityById(@Param("competitorSite") CompetitorSite competitorSite);
	
	//通过壳牌油站id查询
    List<CompetitorSite> findByShellSiteid(Long siteid);
}
