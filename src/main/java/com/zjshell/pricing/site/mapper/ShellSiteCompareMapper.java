package com.zjshell.pricing.site.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjshell.pricing.site.entity.ShellSite;
import com.zjshell.pricing.site.entity.ShellSiteCompare;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShellSiteCompareMapper extends BaseMapper<ShellSiteCompare>  {

	//根据壳牌油站查询竞争对手油站
	List<ShellSiteCompare> findBySiteid(Long siteid);
	//根据竞争对手 油站查询关联壳牌油站
	List<ShellSiteCompare> findByCompareSiteid(Long siteid);
}
