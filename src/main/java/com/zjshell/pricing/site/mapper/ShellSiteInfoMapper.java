package com.zjshell.pricing.site.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjshell.pricing.site.entity.ShellSiteInfo;

public interface ShellSiteInfoMapper extends BaseMapper<ShellSiteInfo>{

	List<ShellSiteInfo> findBySiteId(@Param("shellSiteInfo") ShellSiteInfo shellSiteInfo);
	
	ShellSiteInfo findById(@Param("shellSiteInfo") ShellSiteInfo shellSiteInfo);
	
	IPage<ShellSiteInfo> findShellSiteInfoPage(Page page, @Param("shellSiteInfo") ShellSiteInfo ShellSiteInfo);
}
