package com.zjshell.pricing.site.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjshell.pricing.site.entity.ShellSite;

public interface ShellSiteMapper extends BaseMapper<ShellSite>  {

	ShellSite findById(Long siteid);
	
	IPage<ShellSite> findShellSiteDetailPage(Page page, @Param("shellSite") ShellSite shellSite);
	
	List<ShellSite> findShellSiteDetail(@Param("shellSite") ShellSite shellSite);
	
	Long getshellsiteId();
	
	//匹配code名称查询油站
	List<ShellSite> findShellListMatchCodeName(@Param("shellSite")ShellSite shellSite);

	ShellSite findEntityById(@Param("shellSite") ShellSite shellSite);
}
