package com.zjshell.pricing.site.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zjshell.pricing.site.entity.ShellSiteOil;

public interface ShellSiteOilMapper extends BaseMapper<ShellSiteOil>{

	List<ShellSiteOil> findBySiteId(@Param("shellSiteOil") ShellSiteOil shellSiteOil);
	
	ShellSiteOil findById(@Param("shellSiteOil") ShellSiteOil shellSiteOil);
	
	IPage<ShellSiteOil> findShellSiteOilInfoPage(Page page, @Param("shellSiteOil") ShellSiteOil shellSiteOil);
}
