package com.zjshell.pricing.site.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.site.entity.CompetitorSite;

public interface ICompetitorSiteService extends IService<CompetitorSite> {

	/**
	 * 
	 *描述:findById 查询壳牌油站
	 *返回:ShellSite  
	 *@param siteid油站ID
	 *@return   
	 *@exception   
	 *@since  1.0.0
	 */
	CompetitorSite findById(Long siteid);
	
	/**
	 * 
	 *描述:getCompetitorSiteId
	 *返回:Long  
	 *@return   
	 *@exception   
	 *@since  1.0.0
	 */
	Long getCompetitorSiteId();
	
	/**
	 * 
	 *描述:findShellSiteDetail查询壳自动油站
	 *返回:IPage<ShellSite>  
	 *@param shellSite
	 *@param request
	 *@return   
	 *@exception   
	 *@since  1.0.0
	 */
	IPage<CompetitorSite> findCompetitorSiteDetail(CompetitorSite competitorSite, QueryRequest request);
	
	/**
	 * 
	 *描述:findShellSiteDetail
	 *返回:ShellSite  
	 *@param siteid
	 *@return   
	 *@exception   
	 *@since  1.0.0
	 */
	List<CompetitorSite> findCompetitorSiteDetail(CompetitorSite competitorSite);
	
	/**
	 * 
	 *描述:createShellSite 新建壳牌油站
	 *返回:void  
	 *@param shellSite   
	 *@exception   
	 *@since  1.0.0
	 */
	void createCompetitorSite(CompetitorSite competitorSite);
	
	/**
	 * 
	 *描述:deleteShellSites 删除壳牌油站
	 *返回:void  
	 *@param siteids   
	 *@exception   
	 *@since  1.0.0
	 */
	void deleteCompetitorSites(String[] siteids);
	
	/**
	 * 
	 *描述:updateShellSite 更新壳牌油站
	 *返回:void  
	 *@param shellSite   
	 *@exception   
	 *@since  1.0.0
	 */
	void updateCompetitorSite(CompetitorSite competitorSite);
}
