package com.zjshell.pricing.site.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.site.entity.ShellSite;
import com.zjshell.pricing.site.entity.ShellSiteInfo;

public interface IShellSiteInfoService extends IService<ShellSiteInfo> {

	/**
	 * 
	 *描述:findById 查询油站其它信息
	 *返回:ShellSite  
	 *@param siteid油站ID
	 *@return   
	 *@exception   
	 *@since  1.0.0
	 */
	ShellSiteInfo findById(ShellSiteInfo shellSiteInfo);
	
	
	List<ShellSiteInfo> findBySiteId(ShellSiteInfo shellSiteInfo);
	
	IPage<ShellSiteInfo> findShellSiteInfoPage(ShellSiteInfo shellSiteInfo, QueryRequest request);
	/**
	 * 
	 *描述:createShellSite 新建壳牌油站
	 *返回:void  
	 *@param shellSite   
	 *@exception   
	 *@since  1.0.0
	 */
	void createShellSiteInfo(ShellSiteInfo shellSiteInfo);
	
	/**
	 * 
	 *描述:deleteShellSites 删除壳牌油站
	 *返回:void  
	 *@param siteids   
	 *@exception   
	 *@since  1.0.0
	 */
	void deleteShellSiteInfos(String[] ids);
	
	/**
	 * 
	 *描述:updateShellSite 更新壳牌油站
	 *返回:void  
	 *@param shellSite   
	 *@exception   
	 *@since  1.0.0
	 */
	void updateShellSiteInfo(ShellSiteInfo shellSiteInfo);
}
