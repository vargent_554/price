package com.zjshell.pricing.site.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.site.entity.ShellSiteInfo;
import com.zjshell.pricing.site.entity.ShellSiteOil;

public interface IShellSiteOilService extends IService<ShellSiteOil> {

	/**
	 * 
	 *描述:findById 查询油站其它信息
	 *返回:ShellSite  
	 *@param siteid油站ID
	 *@return   
	 *@exception   
	 *@since  1.0.0
	 */
	ShellSiteOil findById(ShellSiteOil shellSiteOil);
	
	
	List<ShellSiteOil> findBySiteId(ShellSiteOil shellSiteOil);
	
	IPage<ShellSiteOil> findShellSiteOilInfoPage(ShellSiteOil shellSiteOil, QueryRequest request);
	/**
	 * 
	 *描述:createShellSite 新建壳牌油站
	 *返回:void  
	 *@param shellSite   
	 *@exception   
	 *@since  1.0.0
	 */
	void createShellSiteOil(ShellSiteOil shellSiteOil);
	
	/**
	 * 
	 *描述:deleteShellSites 删除壳牌油站
	 *返回:void  
	 *@param siteids   
	 *@exception   
	 *@since  1.0.0
	 */
	void deleteShellSiteOils(String[] ids);
	
	/**
	 * 
	 *描述:updateShellSite 更新壳牌油站
	 *返回:void  
	 *@param shellSite   
	 *@exception   
	 *@since  1.0.0
	 */
	void updateShellSiteOil(ShellSiteOil shellSiteOil);
}
