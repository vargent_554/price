package com.zjshell.pricing.site.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjshell.pricing.common.entity.FebsConstant;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.utils.SortUtil;
import com.zjshell.pricing.site.entity.CompetitorSite;
import com.zjshell.pricing.site.mapper.CompetitorSiteMapper;
import com.zjshell.pricing.site.service.ICompetitorSiteService;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class CompetitorSiteServiceImpl extends ServiceImpl<CompetitorSiteMapper, CompetitorSite> implements ICompetitorSiteService{

	@Autowired
	private CompetitorSiteMapper competitorSiteMapper;

	@Override
	public CompetitorSite findById(Long siteid) {
		return competitorSiteMapper.findById(siteid);
	}

	@Override
	public IPage<CompetitorSite> findCompetitorSiteDetail(CompetitorSite competitorSite, QueryRequest request) {
		Page<CompetitorSite> page = new Page<CompetitorSite>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "site_code", FebsConstant.ORDER_ASC, false);
        return this.baseMapper.findCompetitorSiteDetailPage(page, competitorSite);
	}


	@Override
	public void createCompetitorSite(CompetitorSite competitorSite) {
		this.baseMapper.insert(competitorSite);
	}

	@Override
	public void deleteCompetitorSites(String[] siteids) {
		delete(Arrays.asList(siteids));
	}

	@Override
	public void updateCompetitorSite(CompetitorSite competitorSite) {
		this.baseMapper.updateById(competitorSite);
		
	}
	
	private void delete(List<String> siteids) {
        removeByIds(siteids);
        LambdaQueryWrapper<CompetitorSite> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(CompetitorSite::getSiteid, siteids);
        List<CompetitorSite> shellSites = baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(shellSites)) {
            List<String> dseptIdList = new ArrayList<>();
            shellSites.forEach(d -> dseptIdList.add(String.valueOf(d.getSiteid())));
            this.delete(dseptIdList);
        }
    }


	@Override
	public List<CompetitorSite> findCompetitorSiteDetail(CompetitorSite competitorSite) {
		return competitorSiteMapper.findCompetitorSiteDetail(competitorSite);
	}

	@Override
	public Long getCompetitorSiteId() {
		return competitorSiteMapper.getCompetitorSiteId();
	}

}
