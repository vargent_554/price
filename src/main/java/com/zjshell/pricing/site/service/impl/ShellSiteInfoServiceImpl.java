package com.zjshell.pricing.site.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjshell.pricing.common.entity.FebsConstant;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.utils.SortUtil;
import com.zjshell.pricing.site.entity.ShellSiteInfo;
import com.zjshell.pricing.site.mapper.ShellSiteInfoMapper;
import com.zjshell.pricing.site.service.IShellSiteInfoService;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ShellSiteInfoServiceImpl extends ServiceImpl<ShellSiteInfoMapper, ShellSiteInfo> implements IShellSiteInfoService{

	@Autowired
	private ShellSiteInfoMapper shellSiteInfoMapper;
	
	@Override
	public ShellSiteInfo findById(ShellSiteInfo shellSiteInfo) {
		return shellSiteInfoMapper.findById(shellSiteInfo);
	}

	@Override
	public List<ShellSiteInfo> findBySiteId(ShellSiteInfo shellSiteInfo) {
		return shellSiteInfoMapper.findBySiteId(shellSiteInfo);
	}

	@Override
	public void createShellSiteInfo(ShellSiteInfo shellSiteInfo) {
		this.baseMapper.insert(shellSiteInfo);
	}

	@Override
	public void deleteShellSiteInfos(String[] ids) {
		delete(Arrays.asList(ids));
	}

	@Override
	public void updateShellSiteInfo(ShellSiteInfo shellSiteInfo) {
		this.baseMapper.updateById(shellSiteInfo);
	}

	private void delete(List<String> ids) {
        removeByIds(ids);
        LambdaQueryWrapper<ShellSiteInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ShellSiteInfo::getId, ids);
        List<ShellSiteInfo> shellSiteInfos = baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(shellSiteInfos)) {
            List<String> dseptIdList = new ArrayList<>();
            shellSiteInfos.forEach(d -> dseptIdList.add(String.valueOf(d.getId())));
            this.delete(dseptIdList);
        }
    }

	@Override
	public IPage<ShellSiteInfo> findShellSiteInfoPage(ShellSiteInfo shellSiteInfo, QueryRequest request) {
		Page<ShellSiteInfo> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_DESC, false);
        return this.baseMapper.findShellSiteInfoPage(page, shellSiteInfo);
	}

}
