package com.zjshell.pricing.site.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjshell.pricing.common.entity.FebsConstant;
import com.zjshell.pricing.common.entity.QueryRequest;
import com.zjshell.pricing.common.utils.SortUtil;
import com.zjshell.pricing.site.entity.ShellSiteOil;
import com.zjshell.pricing.site.mapper.ShellSiteOilMapper;
import com.zjshell.pricing.site.service.IShellSiteOilService;


@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ShellSiteOilServiceImpl extends ServiceImpl<ShellSiteOilMapper, ShellSiteOil> implements IShellSiteOilService{

	@Autowired
	private ShellSiteOilMapper shellSiteOilMapper;
	
	@Override
	public ShellSiteOil findById(ShellSiteOil shellSiteOil) {
		return shellSiteOilMapper.findById(shellSiteOil);
	}

	@Override
	public List<ShellSiteOil> findBySiteId(ShellSiteOil shellSiteOil) {
		return shellSiteOilMapper.findBySiteId(shellSiteOil);
	}

	@Override
	public void createShellSiteOil(ShellSiteOil shellSiteOil) {
		this.baseMapper.insert(shellSiteOil);
	}

	@Override
	public void deleteShellSiteOils(String[] ids) {
		delete(Arrays.asList(ids));
	}

	@Override
	public void updateShellSiteOil(ShellSiteOil shellSiteOil) {
		this.baseMapper.updateById(shellSiteOil);
	}

	private void delete(List<String> ids) {
        removeByIds(ids);
        LambdaQueryWrapper<ShellSiteOil> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ShellSiteOil::getId, ids);
        List<ShellSiteOil> shellSiteOils = baseMapper.selectList(queryWrapper);
        if (CollectionUtils.isNotEmpty(shellSiteOils)) {
            List<String> dseptIdList = new ArrayList<>();
            shellSiteOils.forEach(d -> dseptIdList.add(String.valueOf(d.getId())));
            this.delete(dseptIdList);
        }
    }

	@Override
	public IPage<ShellSiteOil> findShellSiteOilInfoPage(ShellSiteOil shellSiteOil, QueryRequest request) {
		Page<ShellSiteOil> page = new Page<>(request.getPageNum(), request.getPageSize());
        SortUtil.handlePageSort(request, page, "id", FebsConstant.ORDER_DESC, false);
        return this.baseMapper.findShellSiteOilInfoPage(page, shellSiteOil);
	}

}
