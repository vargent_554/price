package com.zjshell.pricing.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjshell.pricing.system.entity.Dept;

/**
 * @author YangLei
 */
public interface DeptMapper extends BaseMapper<Dept> {
}
