package com.zjshell.pricing.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zjshell.pricing.system.entity.UserRole;

/**
 * @author YangLei
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
