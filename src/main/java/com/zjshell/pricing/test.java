package com.zjshell.pricing;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zjshell.pricing.webservice.entity.AttachmentForm;
import com.zjshell.pricing.webservice.entity.KmReviewParamterForm;

public class test {

	public static void main(String[] args) throws Exception {
		
		ResourceBundle resource = ResourceBundle.getBundle("api");
		String url = resource.getString("apiurl1");
		//HTTPSClient client = new HTTPSClient();
		KmReviewParamterForm form = new KmReviewParamterForm();
		form.setFdTemplateId("1605857029887201bf7ea2842708a6a6");
		form.setDocSubject("发改委调价20200218");
		form.setDocCreator("{\"LoginName\": \"700029\"}");
		String formValues = "{\"fd_35c391bffd924c\":{\"LoginName\": \"700029\"}, \"fd_35c391cd077fae\":\"1602080e70307058b610b38491586625\", \"fd_35c391da35aca8\":\"2011-10-26\", \"fd_35c391fad19852\":{\"fd_35c391fad19852.fd_35c39240ef2d3c\":[\"92#\",\"95#\",\"0#\"], \"fd_35c391fad19852.fd_35c3929396ffae\":[\"6.7\",\"6.8\",\"5.6\"], \"fd_35c391fad19852.fd_35c392958474c8\":[\"6.6\",\"6.7\",\"5.5\"], \"fd_35c391fad19852.fd_35e950dcf929e6\":[\"0.1\",\"0.1\",\"0.1\"], \"fd_35c391fad19852.fd_35c39297a9cf56\":[\"6.6\",\"6.7\",\"5.5\"]}}";
		form.setFormValues(formValues);
		List<AttachmentForm> attForms = new ArrayList<AttachmentForm>();
		AttachmentForm attForm01 = createAtt("E:\\eclipse-workspace\\price\\target\\pricing\\WEB-INF\\classes\\static\\uploadfile\\price\\123.jpg");
		attForms.add(attForm01);
		form.getAttachmentForms().addAll(attForms);
		/*String ss = client.PostOaApi(url, form);
		JSONObject jsonObject = JSONObject.parseObject(ss);
        String jsd = jsonObject.getString("addReviewResponse");
        JSONObject jsonObject1 = JSONObject.parseObject(jsd);
		String fdid = jsonObject1.getString("return");
        System.out.println(fdid);*/
	}
	
	/**
	 * 创建附件对象
	 */
	static AttachmentForm createAtt(String fileurl) throws Exception {
		AttachmentForm attForm = new AttachmentForm();
		attForm.setFdFileName("发改委调价20200218");
		// 设置附件关键字，表单模式下为附件控件的id
		attForm.setFdKey("fd_35c3927c26cf2e");

		byte[] data = file2bytes(fileurl);
		attForm.setFdAttachment(data);

		return attForm;
	}

	/**
	 * 将文件转换为字节编码
	 */
	static byte[] file2bytes(String fileName) throws Exception {
		InputStream in = new FileInputStream(fileName);
		byte[] data = new byte[in.available()];

		try {
			in.read(data);
		} finally {
			try {
				in.close();
			} catch (IOException ex) {
			}
		}

		return data;
	}

}
