<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
          xmlns:o="urn:schemas-microsoft-com:office:office"
          xmlns:x="urn:schemas-microsoft-com:office:excel"
          xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
          xmlns:html="http://www.w3.org/TR/REC-html40">
    <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
        <LastAuthor>Microsoft Office User</LastAuthor>
        <Created>2015-06-05T18:19:34Z</Created>
        <LastSaved>2019-12-30T01:00:48Z</LastSaved>
        <Version>16.00</Version>
    </DocumentProperties>
    <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
        <AllowPNG/>
        <Colors>
            <Color>
                <Index>39</Index>
                <RGB>#E3E3E3</RGB>
            </Color>
        </Colors>
    </OfficeDocumentSettings>
    <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
        <WindowHeight>16460</WindowHeight>
        <WindowWidth>28240</WindowWidth>
        <WindowTopX>32767</WindowTopX>
        <WindowTopY>1740</WindowTopY>
        <Calculation>ManualCalculation</Calculation>
        <DoNotCalculateBeforeSave/>
        <Iteration/>
        <ProtectStructure>False</ProtectStructure>
        <ProtectWindows>False</ProtectWindows>
        <Uncalced/>
    </ExcelWorkbook>
    <Styles>
        <Style ss:ID="Default" ss:Name="Normal">
            <Alignment ss:Vertical="Bottom"/>
                                            <Borders/>
                                                     <Font ss:FontName="等线" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
                                                                                                                            <Interior/>
                                                                                                                                      <NumberFormat/>
                                                                                                                                                    <Protection/>
        </Style>
        <Style ss:ID="s20" ss:Name="百分比">
            <NumberFormat ss:Format="0%"/>
        </Style>
        <Style ss:ID="m105553178283264">
            <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1"/>
                                                                              <Borders>
                                                                              <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                   <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                      <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                                                                                          </Borders>
                                                                                                                                                                                                                                                                                            <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                                                                                                                                                                                                                                                                                      <NumberFormat ss:Format="@"/>
        </Style>
        <Style ss:ID="s62">
            <Alignment ss:Vertical="Center"/>
                                            <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
        </Style>
        <Style ss:ID="s63">
            <Alignment ss:Vertical="Center"/>
                                            <Borders>
                                            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                 </Borders>
                                                                                                                                                                                   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
                                                                                                                                                                                                                                                         <Interior ss:Color="#FF0000" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s64">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                       <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                         </Borders>
                                                                                                                                                                                                                           <Font ss:FontName="宋体" x:CharSet="134"/>
        </Style>
        <Style ss:ID="s65">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                       <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                           <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                                                                                             </Borders>
                                                                                                                                                                                                                                                                                               <Font ss:FontName="宋体" x:CharSet="134"/>
        </Style>
        <Style ss:ID="s66">
            <Alignment ss:Vertical="Center"/>
                                            <Borders>
                                            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                 <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                                                      </Borders>
                                                                                                                                                                                                                                                        <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
                                                                                                                                                                                                                                                                                                                              <Interior ss:Color="#FF0000" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s67">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                       <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                         </Borders>
                                                                                                                                                                                                                           <Font ss:FontName="宋体" x:CharSet="134"/>
                                                                                                                                                                                                                                                                  <NumberFormat ss:Format="Long Date"/>
        </Style>
        <Style ss:ID="s68">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                       <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                         </Borders>
                                                                                                                                                                                                                           <Font ss:FontName="宋体" x:CharSet="134"/>
                                                                                                                                                                                                                                                                  <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s69">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                       <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                           <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                                                                                             </Borders>
                                                                                                                                                                                                                                                                                               <Font ss:FontName="宋体" x:CharSet="134"/>
                                                                                                                                                                                                                                                                                                                                      <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s70">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                     <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                       </Borders>
                                                                                                                                                                                                                         <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
                                                                                                                                                                                                                                                                                               <Interior ss:Color="#FF0000" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s71">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                    </Borders>
                                                                                                                                                      <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
                                                                                                                                                                                                                            <Interior ss:Color="#FF0000" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s72">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                      <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
                                                                                                                                                                                                                        </Borders>
                                                                                                                                                                                                                          <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
                                                                                                                                                                                                                                                                                                <Interior ss:Color="#FF0000" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s80">
            <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
        </Style>
        <Style ss:ID="s81">
            <Alignment ss:Vertical="Center"/>
                                            <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                                      <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s82">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
                                                                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s83">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <Interior ss:Color="#339966" ss:Pattern="Solid"/>
                                                                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s84">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
                                                                                                       <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s85">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
                                                                                                       <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s86">
            <Alignment ss:Vertical="Center"/>
                                            <Borders>
                                            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s87">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
                                                                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s88">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s89">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
        </Style>
        <Style ss:ID="s90">
            <Alignment ss:Vertical="Center"/>
                                            <Borders>
                                            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
        </Style>
        <Style ss:ID="s91">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
                                                                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s92">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
                                                                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s93">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
                                                                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s94">
            <Alignment ss:Vertical="Center"/>
                                            <Borders>
                                            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
                                                                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s95">
            <Alignment ss:Vertical="Center"/>
                                            <Borders>
                                            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
                                                                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s96">
            <Alignment ss:Vertical="Center"/>
                                            <Borders>
                                            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
        </Style>
        <Style ss:ID="s97">
            <Alignment ss:Vertical="Center"/>
                                            <Borders>
                                            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s98">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
        </Style>
        <Style ss:ID="s99">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
                                                                                                       <Interior ss:Color="#333399" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s100">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
                                                                                                       <Interior ss:Color="#333399" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s101">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                                                             <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s102">
            <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
                                                                 <Borders>
                                                                 <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
        </Style>
        <Style ss:ID="s103">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s104">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s105">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s106">
            <Alignment ss:Vertical="Center"/>
                                            <Borders>
                                            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <NumberFormat ss:Format="Short Date"/>
        </Style>
        <Style ss:ID="s107">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s108">
            <Alignment ss:Vertical="Center"/>
                                            <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                                      <NumberFormat ss:Format="Short Date"/>
        </Style>
        <Style ss:ID="s109">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
                                                                                                       <Interior ss:Color="#FFCC00" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s110">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
                                                                                                       <Interior ss:Color="#FFCC00" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s112" ss:Parent="s20">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
        </Style>
        <Style ss:ID="s113" ss:Parent="s20">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
        </Style>
        <Style ss:ID="s114" ss:Parent="s20">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
        </Style>
        <Style ss:ID="s115" ss:Parent="s20">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
        </Style>
        <Style ss:ID="s116">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s117" ss:Parent="s20">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s118">
            <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
                                                                 <Borders>
                                                                 <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"
            ss:Underline="Single"/>
        </Style>
        <Style ss:ID="s119">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"
            ss:Underline="SingleAccounting"/>
                                            <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s120">
            <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
                                                                   <Borders>
                                                                   <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"
            ss:Underline="SingleAccounting"/>
                                            <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s121">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
        </Style>
        <Style ss:ID="s122">
            <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
                                                                 <Borders>
                                                                 <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
        </Style>
        <Style ss:ID="s123">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
                                                                                           <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
        </Style>
        <Style ss:ID="s124">
            <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
                                                                                  <Borders>
                                                                                  <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
            ss:Color="#969696"/>
                               </Borders>
                                 <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
        </Style>
    </Styles>
    <Worksheet ss:Name="活动信息">
        <Table ss:ExpandedColumnCount="10" ss:ExpandedRowCount="24" x:FullColumns="1"
               x:FullRows="1" ss:StyleID="s62" ss:DefaultColumnWidth="54"
               ss:DefaultRowHeight="14">
            <Column ss:Index="2" ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="108"/>
            <Row ss:AutoFitHeight="0" ss:Height="15"/>
            <Row ss:AutoFitHeight="0" ss:Height="15">
                <Cell ss:Index="2" ss:StyleID="s63"><Data ss:Type="String">规则ID</Data></Cell>
                <Cell ss:StyleID="s64"><Data ss:Type="String">${shellActivityApplyDto.shellActivityApply.id}</Data></Cell>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s65"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="15">
                <Cell ss:Index="2" ss:StyleID="s63"><Data ss:Type="String">活动名称</Data></Cell>
                <Cell ss:StyleID="s64"><Data ss:Type="String">${shellActivityApplyDto.shellActivityApply.activityName!""}</Data></Cell>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s65"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s66"><Data ss:Type="String">活动范围</Data></Cell>
                <Cell ss:StyleID="s64"><Data ss:Type="String">${shellActivityApplyDto.shellActivityApply.shellName!""}</Data></Cell>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s65"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell><Data ss:Type="String"> </Data></Cell>
                <Cell ss:StyleID="s66"><Data ss:Type="String">活动开始时间</Data></Cell>
                <Cell ss:StyleID="s67"><Data ss:Type="String">${shellActivityApplyDto.shellActivityApply.beginTime?string("yyyy-MM-dd")}</Data></Cell>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s65"/>
                <Cell><Data ss:Type="String">年/月/日</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s66"><Data ss:Type="String">活动结束时间</Data></Cell>
                <Cell ss:StyleID="s67"><Data ss:Type="String">${shellActivityApplyDto.shellActivityApply.endTime?string("yyyy-MM-dd")}</Data></Cell>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s65"/>
                <Cell><Data ss:Type="String">年/月/日</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s66"><Data ss:Type="String">活动期</Data></Cell>
                <Cell ss:StyleID="s68"><Data ss:Type="String">${shellActivityApplyDto.shellActivityApply.activityDays}</Data></Cell>
                <Cell ss:StyleID="s68"/>
                <Cell ss:StyleID="s68"/>
                <Cell ss:StyleID="s68"/>
                <Cell ss:StyleID="s68"/>
                <Cell ss:StyleID="s68"/>
                <Cell ss:StyleID="s69"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s66"><Data ss:Type="String">活动前影响天数</Data></Cell>
                <Cell ss:StyleID="s64"><Data ss:Type="String">${shellActivityApplyDto.shellActivityApply.activityBeforeDays}</Data></Cell>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s65"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s66"><Data ss:Type="String">活动后影响天数</Data></Cell>
                <Cell ss:StyleID="s64"><Data ss:Type="String">${shellActivityApplyDto.shellActivityApply.activityAfterDays}</Data></Cell>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s64"/>
                <Cell ss:StyleID="s65"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s66"><Data ss:Type="String">活动全周期影响天数</Data></Cell>
                <Cell ss:StyleID="s68"><Data ss:Type="String">${shellActivityApplyDto.shellActivityApply.activityAllDays}</Data></Cell>
                <Cell ss:StyleID="s68"/>
                <Cell ss:StyleID="s68"/>
                <Cell ss:StyleID="s68"/>
                <Cell ss:StyleID="s68"/>
                <Cell ss:StyleID="s68"/>
                <Cell ss:StyleID="s69"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="15"/>
            <Row ss:AutoFitHeight="0" ss:Height="13.5">
                <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动描述</Data></Cell>
                <Cell ss:StyleID="s71"/>
                <Cell ss:StyleID="s71"/>
                <Cell ss:StyleID="s71"/>
                <Cell ss:StyleID="s71"/>
                <Cell ss:StyleID="s71"/>
                <Cell ss:StyleID="s71"/>
                <Cell ss:StyleID="s72"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:MergeAcross="7" ss:MergeDown="11"
                      ss:StyleID="m105553178283264"><Data ss:Type="String">${shellActivityApplyDto.shellActivityApply.remark!""}</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Span="9"/>
            <Row ss:Index="24" ss:AutoFitHeight="0" ss:Height="15"/>
        </Table>
        <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
            <PageSetup>
                <Header x:Margin="0.3"/>
                <Footer x:Margin="0.3"/>
                <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
            </PageSetup>
            <Unsynced/>
            <Selected/>
            <DoNotDisplayGridlines/>
            <Panes>
                <Pane>
                    <Number>3</Number>
                    <ActiveRow>12</ActiveRow>
                    <ActiveCol>1</ActiveCol>
                    <RangeSelection>R13C2:R24C9</RangeSelection>
                </Pane>
            </Panes>
            <ProtectObjects>False</ProtectObjects>
            <ProtectScenarios>False</ProtectScenarios>
        </WorksheetOptions>
    </Worksheet>
    <Worksheet ss:Name="名称定义">
        <Table ss:ExpandedColumnCount="2" ss:ExpandedRowCount="7" x:FullColumns="1"
               x:FullRows="1" ss:StyleID="s80" ss:DefaultColumnWidth="54"
               ss:DefaultRowHeight="14">
            <Column ss:StyleID="s80" ss:AutoFitWidth="0" ss:Width="108"/>
            <Column ss:StyleID="s80" ss:AutoFitWidth="0" ss:Width="440"/>
            <Row ss:AutoFitHeight="0">
                <Cell><Data ss:Type="String">名称</Data></Cell>
                <Cell><Data ss:Type="String">定义</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell><Data ss:Type="String">会员拉新/有效会员</Data></Cell>
                <Cell><Data ss:Type="String">有过消费记录的会员</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell><Data ss:Type="String">活跃会员（当月）</Data></Cell>
                <Cell><Data ss:Type="String">在上一个度量周期没有消费，但在本度量周期内，有消费记录的会员</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell><Data ss:Type="String">留存会员（两个月）</Data></Cell>
                <Cell><Data ss:Type="String">在本度量周期及上一个度量周期，均消费过一次的会员</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell><Data ss:Type="String">忠诚会员（三个月）</Data></Cell>
                <Cell><Data ss:Type="String">在本度量周期，上一个度量周期，及上上个度量周期，均消费过一次的会员</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell><Data ss:Type="String">沉睡会员</Data></Cell>
                <Cell><Data ss:Type="String">在本度量周期没有消费过的会员</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell><Data ss:Type="String">流失会员</Data></Cell>
                <Cell><Data ss:Type="String">在本度量周期，上一个度量周期，及上上个度量周期，均没有消费过的有效会员</Data></Cell>
            </Row>
        </Table>
        <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
            <PageSetup>
                <Header x:Margin="0.3"/>
                <Footer x:Margin="0.3"/>
                <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
            </PageSetup>
            <Unsynced/>
            <Visible>SheetHidden</Visible>
            <Panes>
                <Pane>
                    <Number>3</Number>
                    <ActiveRow>20</ActiveRow>
                    <ActiveCol>1</ActiveCol>
                </Pane>
            </Panes>
            <ProtectObjects>False</ProtectObjects>
            <ProtectScenarios>False</ProtectScenarios>
        </WorksheetOptions>
    </Worksheet>
    <Worksheet ss:Name="换购">
        <Table ss:ExpandedColumnCount="8" ss:ExpandedRowCount="107" x:FullColumns="1"
               x:FullRows="1" ss:StyleID="s62" ss:DefaultColumnWidth="54"
               ss:DefaultRowHeight="14">
            <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="24"/>
            <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="33"/>
            <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="174"/>
            <Column ss:StyleID="s81" ss:AutoFitWidth="0" ss:Width="116" ss:Span="4"/>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="4" ss:StyleID="s82"><Data ss:Type="String">油-非油换购</Data></Cell>
                <Cell ss:StyleID="s82"/>
                <Cell ss:StyleID="s82"/>
                <Cell ss:StyleID="s82"/>
                <Cell ss:StyleID="s83"><Data ss:Type="String">非油-非油换购</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="4" ss:StyleID="s84"><Data ss:Type="String">#92</Data></Cell>
                <Cell ss:StyleID="s84"><Data ss:Type="String">#95</Data></Cell>
                <Cell ss:StyleID="s84"><Data ss:Type="String">#98</Data></Cell>
                <Cell ss:StyleID="s84"><Data ss:Type="String">#0</Data></Cell>
                <Cell ss:StyleID="s85"><Data ss:Type="String">NFR</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="3" ss:StyleID="s86"><Data ss:Type="String">挂牌价（元/升）</Data></Cell>
                <Cell ss:StyleID="s87"><Data ss:Type="Number">6.5</Data></Cell>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="3" ss:StyleID="s86"><Data ss:Type="String">采购价（元/升）</Data></Cell>
                <Cell ss:StyleID="s87"><Data ss:Type="Number">5</Data></Cell>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:Index="7" ss:AutoFitHeight="0" ss:Height="16.5">
                <Cell ss:Index="2" ss:StyleID="s89"><Data ss:Type="String">商品1</Data></Cell>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品名称</Data></Cell>
                <Cell ss:StyleID="s91"><Data ss:Type="String">AAA</Data></Cell>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s93"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"><Data ss:Type="Number">100</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"><Data ss:Type="Number">70</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">最低消费金额</Data></Cell>
                <Cell ss:StyleID="s94"><Data ss:Type="Number">238</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购金额</Data></Cell>
                <Cell ss:StyleID="s94"><Data ss:Type="Number">80</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s96"/>
                <Cell ss:StyleID="s96"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="16.5">
                <Cell ss:Index="2" ss:StyleID="s89"><Data ss:Type="String">商品2</Data></Cell>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品名称</Data></Cell>
                <Cell ss:StyleID="s91"><Data ss:Type="String">BBB</Data></Cell>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s93"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"><Data ss:Type="Number">100</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"><Data ss:Type="Number">70</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">最低消费金额</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"><Data ss:Type="Number">100</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购金额</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"><Data ss:Type="Number">80</Data></Cell>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s96"/>
                <Cell ss:StyleID="s96"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="16.5">
                <Cell ss:Index="2" ss:StyleID="s89"><Data ss:Type="String">商品3</Data></Cell>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品名称</Data></Cell>
                <Cell ss:StyleID="s91"><Data ss:Type="String">CCC</Data></Cell>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s93"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">最低消费金额</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购金额</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s96"/>
                <Cell ss:StyleID="s96"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="16.5">
                <Cell ss:Index="2" ss:StyleID="s89"><Data ss:Type="String">商品4</Data></Cell>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品名称</Data></Cell>
                <Cell ss:StyleID="s91"><Data ss:Type="String">DDD</Data></Cell>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s93"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">最低消费金额</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购金额</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s96"/>
                <Cell ss:StyleID="s96"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="16.5">
                <Cell ss:Index="2" ss:StyleID="s89"><Data ss:Type="String">商品5</Data></Cell>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品名称</Data></Cell>
                <Cell ss:StyleID="s91"><Data ss:Type="String">EEE</Data></Cell>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s93"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">最低消费金额</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购金额</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s96"/>
                <Cell ss:StyleID="s96"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
                <Cell ss:StyleID="s97"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="16.5">
                <Cell ss:Index="2" ss:StyleID="s89"><Data ss:Type="String">商品6</Data></Cell>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品名称</Data></Cell>
                <Cell ss:StyleID="s91"><Data ss:Type="String">FFF</Data></Cell>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s92"/>
                <Cell ss:StyleID="s93"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">最低消费金额</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s89"/>
                <Cell ss:StyleID="s90"><Data ss:Type="String">换购金额</Data></Cell>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s94"/>
                <Cell ss:StyleID="s95"/>
            </Row>
            <Row ss:Index="44" ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s98"><Data ss:Type="String">燃油</Data></Cell>
                <Cell ss:StyleID="s98"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="15">
                <Cell ss:Index="2" ss:StyleID="s99"><Data ss:Type="String">无活动</Data></Cell>
                <Cell ss:StyleID="s100"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s102"><Data ss:Type="String">日均加油车辆（辆）</Data></Cell>
                <Cell ss:StyleID="s102"/>
                <Cell ss:StyleID="s103"/>
                <Cell ss:StyleID="s104"/>
                <Cell ss:StyleID="s104"/>
                <Cell ss:StyleID="s105"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动前</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!-活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动期</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s87"><Data ss:Type="Number">500</Data></Cell>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动后</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!+活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s102"><Data ss:Type="String">单车加油量（升）</Data></Cell>
                <Cell ss:StyleID="s102"/>
                <Cell ss:StyleID="s87"><Data ss:Type="Number">30</Data></Cell>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s102"><Data ss:Type="String">日均销量（升）</Data></Cell>
                <Cell ss:StyleID="s102"><Data ss:Type="String">无活动时日均销量（升）</Data></Cell>
                <Cell ss:StyleID="s103"/>
                <Cell ss:StyleID="s104"/>
                <Cell ss:StyleID="s104"/>
                <Cell ss:StyleID="s105"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动前</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!-活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-2]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-2]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-2]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-2]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动期</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-3]C"><Data ss:Type="Number">15000</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-3]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-3]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-3]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动后</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!+活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-4]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-4]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-4]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-5]C*R[-4]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="3" ss:StyleID="s108"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="3" ss:StyleID="s108"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s98"><Data ss:Type="String">燃油</Data></Cell>
                <Cell ss:StyleID="s98"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="15">
                <Cell ss:Index="2" ss:StyleID="s109"><Data ss:Type="String">有活动</Data></Cell>
                <Cell ss:StyleID="s110"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s102"><Data ss:Type="String">预计日均加油车辆（辆）</Data></Cell>
                <Cell ss:StyleID="s102"/>
                <Cell ss:StyleID="s103"/>
                <Cell ss:StyleID="s104"/>
                <Cell ss:StyleID="s104"/>
                <Cell ss:StyleID="s105"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动前</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!-活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动期</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s87"><Data ss:Type="Number">500</Data></Cell>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动后</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!+活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s102"><Data ss:Type="String">预计活动参与率（%）</Data></Cell>
                <Cell ss:StyleID="s102"/>
                <Cell ss:StyleID="s112"><Data ss:Type="Number">0.3</Data></Cell>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s102"><Data ss:Type="String">预计换购商品比例（%）</Data></Cell>
                <Cell ss:StyleID="s102"/>
                <Cell ss:StyleID="s113" ss:Formula="=100%-SUM(R[1]C:R[6]C)"><Data
                        ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s114" ss:Formula="=100%-SUM(R[1]C:R[6]C)"><Data
                        ss:Type="Number">1</Data></Cell>
                <Cell ss:StyleID="s114" ss:Formula="=100%-SUM(R[1]C:R[6]C)"><Data
                        ss:Type="Number">1</Data></Cell>
                <Cell ss:StyleID="s115" ss:Formula="=100%-SUM(R[1]C:R[6]C)"><Data
                        ss:Type="Number">1</Data></Cell>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s116" ss:Formula="=R[-58]C[2]"><Data
                        ss:Type="String">AAA</Data></Cell>
                <Cell ss:StyleID="s89"/>
                <Cell ss:StyleID="s112"><Data ss:Type="Number">1</Data></Cell>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s116" ss:Formula="=R[-53]C[2]"><Data
                        ss:Type="String">BBB</Data></Cell>
                <Cell ss:StyleID="s89"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s116" ss:Formula="=R[-48]C[2]"><Data
                        ss:Type="String">CCC</Data></Cell>
                <Cell ss:StyleID="s89"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s116" ss:Formula="=R[-43]C[2]"><Data
                        ss:Type="String">DDD</Data></Cell>
                <Cell ss:StyleID="s89"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s116" ss:Formula="=R[-38]C[2]"><Data
                        ss:Type="String">EEE</Data></Cell>
                <Cell ss:StyleID="s89"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s116" ss:Formula="=R[-33]C[2]"><Data
                        ss:Type="String">FFF</Data></Cell>
                <Cell ss:StyleID="s89"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s112"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s102"><Data ss:Type="String">参与活动的车辆的单升加油量（升）</Data></Cell>
                <Cell ss:StyleID="s102"/>
                <Cell ss:StyleID="s117" ss:Formula="=R[-61]C/R[-68]C"><Data ss:Type="Number">36.615384615384613</Data></Cell>
                <Cell ss:StyleID="s117"/>
                <Cell ss:StyleID="s117"/>
                <Cell ss:StyleID="s117"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s102"><Data ss:Type="String">预计日均销量（升）</Data></Cell>
                <Cell ss:StyleID="s102"><Data ss:Type="String">无活动时日均销量（升）</Data></Cell>
                <Cell ss:StyleID="s103"/>
                <Cell ss:StyleID="s104"/>
                <Cell ss:StyleID="s104"/>
                <Cell ss:StyleID="s105"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动前</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!-活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动期</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107"
                      ss:Formula="=R[-13]C*R[-11]C*R[-3]C+R[-13]C*(1-R[-11]C)*R[-24]C"><Data
                        ss:Type="Number">15992.307692307691</Data></Cell>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s88"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动后</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!+活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s107"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:Index="78" ss:AutoFitHeight="0" ss:Height="18.75">
                <Cell ss:Index="2" ss:StyleID="s118"><Data ss:Type="String">销量总变化（升）</Data></Cell>
                <Cell ss:StyleID="s118"/>
                <Cell ss:StyleID="s119" ss:Formula="=SUM(R[1]C:R[3]C)"><Data ss:Type="Number">992.30769230769147</Data></Cell>
                <Cell ss:StyleID="s119" ss:Formula="=SUM(R[1]C:R[3]C)"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s119" ss:Formula="=SUM(R[1]C:R[3]C)"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s120" ss:Formula="=SUM(R[1]C:R[3]C)"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动前</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!-活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动期</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">992.30769230769147</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动后</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!+活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="3" ss:StyleID="s108"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:Index="84" ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s121"><Data ss:Type="String">非油</Data></Cell>
                <Cell ss:StyleID="s121"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="15">
                <Cell ss:Index="2" ss:StyleID="s99"><Data ss:Type="String">无活动</Data></Cell>
                <Cell ss:StyleID="s100"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Height="14.25">
                <Cell ss:Index="2" ss:StyleID="s102"><Data ss:Type="String">预计日均销量（个）</Data></Cell>
                <Cell ss:StyleID="s122"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动前</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!-活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=SUM(R[1]C:R[6]C)"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-81]C[2]"><Data
                        ss:Type="String">AAA</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-76]C[2]"><Data
                        ss:Type="String">BBB</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-71]C[2]"><Data
                        ss:Type="String">CCC</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-66]C[2]"><Data
                        ss:Type="String">DDD</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-61]C[2]"><Data
                        ss:Type="String">EEE</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-56]C[2]"><Data
                        ss:Type="String">FFF</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动期</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=SUM(R[1]C:R[6]C)"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-81]C[2]"><Data
                        ss:Type="String">AAA</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-76]C[2]"><Data
                        ss:Type="String">BBB</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-71]C[2]"><Data
                        ss:Type="String">CCC</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-66]C[2]"><Data
                        ss:Type="String">DDD</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-61]C[2]"><Data
                        ss:Type="String">EEE</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-56]C[2]"><Data
                        ss:Type="String">FFF</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0">
                <Cell ss:Index="2" ss:StyleID="s90"><Data ss:Type="String">活动后</Data></Cell>
                <Cell ss:StyleID="s106"
                      ss:Formula="=TEXT(活动信息!#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(活动信息!#REF!+活动信息!#REF!,&quot;yyyy年m月d日&quot;)"><Data
                        ss:Type="Error">#REF!</Data></Cell>
                <Cell ss:StyleID="s107" ss:Formula="=SUM(R[1]C:R[6]C)"><Data ss:Type="Number">0</Data></Cell>
                <Cell ss:StyleID="s88"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s101"/>
                <Cell ss:StyleID="s62"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-81]C[2]"><Data
                        ss:Type="String">AAA</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-76]C[2]"><Data
                        ss:Type="String">BBB</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-71]C[2]"><Data
                        ss:Type="String">CCC</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-66]C[2]"><Data
                        ss:Type="String">DDD</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-61]C[2]"><Data
                        ss:Type="String">EEE</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
            </Row>
            <Row ss:AutoFitHeight="0" ss:Hidden="1">
                <Cell ss:Index="2" ss:StyleID="s123" ss:Formula="=R[-56]C[2]"><Data
                        ss:Type="String">FFF</Data></Cell>
                <Cell ss:StyleID="s124"/>
                <Cell ss:StyleID="s87"/>
            </Row>
        </Table>
        <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
            <PageSetup>
                <Header x:Margin="0.3"/>
                <Footer x:Margin="0.3"/>
                <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
            </PageSetup>
            <Unsynced/>
            <Visible>SheetHidden</Visible>
            <DoNotDisplayGridlines/>
            <FreezePanes/>
            <FrozenNoSplit/>
            <SplitHorizontal>2</SplitHorizontal>
            <TopRowBottomPane>57</TopRowBottomPane>
            <SplitVertical>3</SplitVertical>
            <LeftColumnRightPane>3</LeftColumnRightPane>
            <ActivePane>0</ActivePane>
            <Panes>
                <Pane>
                    <Number>3</Number>
                </Pane>
                <Pane>
                    <Number>1</Number>
                </Pane>
                <Pane>
                    <Number>2</Number>
                    <ActiveRow>1</ActiveRow>
                </Pane>
                <Pane>
                    <Number>0</Number>
                    <ActiveRow>109</ActiveRow>
                    <ActiveCol>1</ActiveCol>
                </Pane>
            </Panes>
            <ProtectObjects>False</ProtectObjects>
            <ProtectScenarios>False</ProtectScenarios>
        </WorksheetOptions>
    </Worksheet>
</Workbook>
