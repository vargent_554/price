<?xml version="1.0"?>
<?mso-application progid="Excel.Sheet"?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <LastAuthor>Microsoft Office User</LastAuthor>
  <Created>2015-06-05T18:19:34Z</Created>
  <LastSaved>2019-12-30T01:03:55Z</LastSaved>
  <Version>16.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
  <Colors>
   <Color>
    <Index>39</Index>
    <RGB>#E3E3E3</RGB>
   </Color>
  </Colors>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>16460</WindowHeight>
  <WindowWidth>28800</WindowWidth>
  <WindowTopX>32767</WindowTopX>
  <WindowTopY>460</WindowTopY>
  <Calculation>ManualCalculation</Calculation>
  <DoNotCalculateBeforeSave/>
  <Iteration/>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
  <Uncalced/>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="等线" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s34" ss:Name="百分比">
   <NumberFormat ss:Format="0%"/>
  </Style>
  <Style ss:ID="s62">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s63">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s64">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s65">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s66">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s67">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s68">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s69">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s70">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s71">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s72">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s73" ss:Parent="s34">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s74" ss:Parent="s34">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s75" ss:Parent="s34">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s76" ss:Parent="s34">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s77" ss:Parent="s34">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s78">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s79">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s80">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"
    ss:Underline="SingleAccounting"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s81">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"
    ss:Underline="SingleAccounting"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s82">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s84">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s85">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s86">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s87">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s88">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#CCFFCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s89">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#339966" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s90">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s91">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s92">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s93">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s94">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s95">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s96">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s97">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
   <Interior ss:Color="#FF0000" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s98">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#FFCC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s99">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s100">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s101">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#FFCC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s102">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s103">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#993300" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s104">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#993300"/>
   <NumberFormat ss:Format="Short Date"/>
  </Style>
  <Style ss:ID="s105">
   <Font ss:FontName="宋体" x:CharSet="134" ss:Size="11" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s106">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FF0000" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s107">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0_ ;_ * \-#,##0_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s108">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s109">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s110">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0_ ;_ * \-#,##0_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s111">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <NumberFormat ss:Format="_ * #,##0_ ;_ * \-#,##0_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s112">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s113">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Underline="Single"/>
  </Style>
  <Style ss:ID="s114">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s115">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FFFF99" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.0_ ;_ * \-#,##0.0_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s116">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Underline="Single"/>
  </Style>
  <Style ss:ID="s117">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s118">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
   <NumberFormat ss:Format="0"/>
  </Style>
  <Style ss:ID="s119">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134"/>
  </Style>
  <Style ss:ID="s121">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#993300" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s123">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
   <Interior ss:Color="#FF0000" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s124">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s125">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s126">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="2"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="2"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s127">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
   <Interior ss:Color="#333399" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s128">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
   <Interior ss:Color="#333399" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s129">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s130">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s131">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
   <Interior ss:Color="#FFCC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s132">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#FFFFFF" ss:Bold="1"/>
   <Interior ss:Color="#FFCC00" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s133">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"
    ss:Underline="Single"/>
  </Style>
  <Style ss:ID="s134">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s135">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#C0C0C0" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s136">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s137">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s138">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s139">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s140">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000" ss:Bold="1"/>
  </Style>
  <Style ss:ID="s141">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s142">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
  <Style ss:ID="s143">
   <Alignment ss:Horizontal="CenterAcrossSelection" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#969696"/>
   </Borders>
   <Font ss:FontName="宋体" x:CharSet="134" ss:Color="#000000"/>
   <Interior ss:Color="#FFFFCC" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="_ * #,##0.00_ ;_ * \-#,##0.00_ ;_ * &quot;-&quot;??_ ;_ @_ "/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Show Page">
  <Table ss:ExpandedColumnCount="9" ss:ExpandedRowCount="79" x:FullColumns="1"
   x:FullRows="1" ss:StyleID="s62" ss:DefaultColumnWidth="54"
   ss:DefaultRowHeight="14">
   <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="24"/>
   <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="21"/>
   <Column ss:Index="4" ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="167"/>
   <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="16"/>
   <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="144" ss:Span="2"/>
   <Column ss:Index="9" ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="114"/>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="4" ss:StyleID="s98"><Data ss:Type="String">活动内容</Data></Cell>
    <Cell ss:StyleID="s101"/>
    <Cell ss:StyleID="s124"/>
    <Cell ss:StyleID="s125"/>
    <Cell ss:StyleID="s126"/>
   </Row>
   <Row>
    <Cell ss:Index="4" ss:StyleID="s99"/>
    <Cell ss:StyleID="s109"/>
    <Cell ss:StyleID="s95"/>
    <Cell ss:StyleID="s95"/>
    <Cell ss:StyleID="s95"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s116"/>
    <Cell ss:StyleID="s113"><Data ss:Type="String">全活动周期</Data></Cell>
    <Cell ss:Index="6" ss:StyleID="s93"><Data ss:Type="String">#92</Data></Cell>
    <Cell ss:StyleID="s93"><Data ss:Type="String">#95</Data></Cell>
    <Cell ss:StyleID="s93"><Data ss:Type="String">#0</Data></Cell>
    <Cell ss:StyleID="s93"><Data ss:Type="String">Total</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="2" ss:StyleID="s123"><Data ss:Type="String">1. 基准情况(BASE CASE SCENARIO )</Data></Cell>
    <Cell ss:StyleID="s123"/>
    <Cell ss:StyleID="s123"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s106"/>
    <Cell ss:StyleID="s106"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="13.5">
    <Cell ss:Index="2" ss:StyleID="s108"/>
    <Cell ss:StyleID="s108"/>
    <Cell ss:StyleID="s108"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
   </Row>
   <Row>
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">无活动日均车辆（辆）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number"><#if shellActivityApplyDto.shellActivityApplyOilDtoList??><#list shellActivityApplyDto.shellActivityApplyOilDtoList as list><#if list.shellActivityApplyOil.oilCode == "#92">${list.shellActivityApplyOil.basePerdayCar!"0"}</#if></#list></#if></Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">102</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">103</Data></Cell>
    <Cell ss:StyleID="s110"><Data ss:Type="Number">${shellActivityApplyDto.shellActivityApply.baseTotalCar!"0"}</Data></Cell>
   </Row>
   <Row>
    <Cell ss:Index="4" ss:StyleID="s78"/>
    <Cell ss:Index="6" ss:StyleID="s63"/>
    <Cell ss:Index="9" ss:StyleID="s99"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">无活动单加（升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">105</Data></Cell>
    <Cell ss:StyleID="s115"><Data ss:Type="Number">106</Data></Cell>
    <Cell ss:StyleID="s115"><Data ss:Type="Number">107</Data></Cell>
    <Cell ss:StyleID="s111"/>
   </Row>
   <Row>
    <Cell ss:Index="4" ss:StyleID="s78"/>
    <Cell ss:Index="6" ss:StyleID="s63"/>
    <Cell ss:Index="9" ss:StyleID="s99"/>
   </Row>
   <Row>
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">无活动日均销量（升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">108</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">109</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">110</Data></Cell>
    <Cell ss:StyleID="s110"><Data ss:Type="Number">111</Data></Cell>
   </Row>
   <Row ss:Index="12" ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">无活动时单升C3（元/升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s91"><Data ss:Type="Number">112</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">113</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">114</Data></Cell>
    <Cell ss:StyleID="s111"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25"/>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s123"><Data ss:Type="String">2. 促销情况 (PROMOTION SCENARIO)</Data></Cell>
    <Cell ss:StyleID="s123"/>
    <Cell ss:StyleID="s123"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s106"/>
    <Cell ss:StyleID="s106"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s108"/>
    <Cell ss:StyleID="s108"/>
    <Cell ss:StyleID="s108"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">有活动日均车辆（辆）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">115</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">116</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">117</Data></Cell>
    <Cell ss:StyleID="s110"><Data ss:Type="Number">118</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75" ss:Hidden="1">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">活动期日均车辆（辆）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107" ss:Formula="=#REF!"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s107" ss:Formula="=#REF!"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s107" ss:Formula="=#REF!"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s110" ss:Formula="=SUM(RC[-3]:RC[-1])"><Data ss:Type="Number">0</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75" ss:Hidden="1">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">影响期日均车辆（辆）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"
     ss:Formula="=(#REF!*#REF!+#REF!*#REF!)/(#REF!+#REF!)"><Data ss:Type="Error">#DIV/0!</Data></Cell>
    <Cell ss:StyleID="s107" ss:Formula="=(#REF!*#REF!+#REF!*#REF!)/(#REF!+#REF!)"><Data
      ss:Type="Error">#DIV/0!</Data></Cell>
    <Cell ss:StyleID="s107" ss:Formula="=(#REF!*#REF!+#REF!*#REF!)/(#REF!+#REF!)"><Data
      ss:Type="Error">#DIV/0!</Data></Cell>
    <Cell ss:StyleID="s110" ss:Formula="=SUM(RC[-3]:RC[-1])"><Data ss:Type="Error">#DIV/0!</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="9">
    <Cell ss:Index="4" ss:StyleID="s78"/>
    <Cell ss:Index="6" ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">活动期单加（升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">119</Data></Cell>
    <Cell ss:StyleID="s115"><Data ss:Type="Number">120</Data></Cell>
    <Cell ss:StyleID="s115"><Data ss:Type="Number">121</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="7.5">
    <Cell ss:Index="4" ss:StyleID="s78"/>
    <Cell ss:Index="6" ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">有活动日均销量（升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">122</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">123</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">124</Data></Cell>
    <Cell ss:StyleID="s110"><Data ss:Type="Number">125</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="9">
    <Cell ss:Index="3" ss:StyleID="s96"/>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s111"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">直降（元/升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s91"><Data ss:Type="Number">126</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">127</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">128</Data></Cell>
    <Cell ss:StyleID="s111"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">优惠券（元/升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s91"><Data ss:Type="Number">129</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">130</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">131</Data></Cell>
    <Cell ss:StyleID="s111"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="8.25">
    <Cell ss:Index="3" ss:StyleID="s96"/>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s111"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">有活动时单升C3（元/升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s91"><Data ss:Type="Number">132</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">133</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">134</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"/>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s92"/>
    <Cell ss:StyleID="s92"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="2" ss:StyleID="s123"><Data ss:Type="String">3. 促销KPI</Data></Cell>
    <Cell ss:StyleID="s123"/>
    <Cell ss:StyleID="s123"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s106"/>
    <Cell ss:StyleID="s106"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="7.5">
    <Cell ss:Index="2" ss:StyleID="s94"/>
    <Cell ss:StyleID="s94"/>
    <Cell ss:StyleID="s94"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="2" ss:StyleID="s94"/>
    <Cell ss:StyleID="s119"><Data ss:Type="String">活动全周期（天）</Data></Cell>
    <Cell ss:StyleID="s119"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s112"><Data ss:Type="Number">135</Data></Cell>
    <Cell ss:StyleID="s102"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String">销量贡献(KL)</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s100"><Data ss:Type="Number">136</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="3"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#92</Data></Cell>
    <Cell ss:Index="6" ss:StyleID="s112"><Data ss:Type="Number">137</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="3"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#95</Data></Cell>
    <Cell ss:Index="6" ss:StyleID="s100"><Data ss:Type="Number">138</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="3"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#0</Data></Cell>
    <Cell ss:Index="6" ss:StyleID="s112"><Data ss:Type="Number">139</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String">C3贡献(KRMB)</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s100"><Data ss:Type="Number">140</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#92</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s112"><Data ss:Type="Number">141</Data></Cell>
    <Cell ss:StyleID="s63"/>
    <Cell ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#95</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s100"><Data ss:Type="Number">142</Data></Cell>
    <Cell ss:StyleID="s63"/>
    <Cell ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#0</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s112"><Data ss:Type="Number">143</Data></Cell>
    <Cell ss:StyleID="s63"/>
    <Cell ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-NFR</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s100"><Data ss:Type="Number">144</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String" x:Ticked="1">   直降</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s112"><Data ss:Type="Number">145</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String" x:Ticked="1">   优惠券</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s100"><Data ss:Type="Number">146</Data></Cell>
    <Cell ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String" x:Ticked="1">   换购折扣</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s112"><Data ss:Type="Number">147</Data></Cell>
    <Cell ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String" x:Ticked="1">   赠品（SP&amp;A)</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s100"><Data ss:Type="Number">148</Data></Cell>
    <Cell ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String">   宣传等物料投入(SP&amp;A)</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s112"><Data ss:Type="Number">149</Data></Cell>
    <Cell ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s121"><Data ss:Type="String">净利润</Data></Cell>
    <Cell ss:StyleID="s121"/>
    <Cell ss:Index="6" ss:StyleID="s100"><Data ss:Type="Number">150</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s103"><Data ss:Type="String" x:Ticked="1">总成本</Data></Cell>
    <Cell ss:StyleID="s104"/>
    <Cell ss:Index="6" ss:StyleID="s112"><Data ss:Type="Number">151</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s103"><Data ss:Type="String" x:Ticked="1">ROI</Data></Cell>
    <Cell ss:StyleID="s104"/>
    <Cell ss:Index="6" ss:StyleID="s100"><Data ss:Type="Number">152</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="4" ss:StyleID="s78"/>
    <Cell ss:Index="6" ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="2" ss:StyleID="s123"><Data ss:Type="String">4. PIR</Data></Cell>
    <Cell ss:StyleID="s123"/>
    <Cell ss:StyleID="s123"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s97"/>
    <Cell ss:StyleID="s106"/>
    <Cell ss:StyleID="s106"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="7.5">
    <Cell ss:Index="2" ss:StyleID="s108"/>
    <Cell ss:StyleID="s108"/>
    <Cell ss:StyleID="s108"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s102"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">活动期间日均车辆（辆）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">153</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">154</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">155</Data></Cell>
    <Cell ss:StyleID="s114"><Data ss:Type="Number">156</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">活动前后日均车辆（辆）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">157</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">178</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">183</Data></Cell>
    <Cell ss:StyleID="s114"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="9">
    <Cell ss:Index="4" ss:StyleID="s78"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">活动期间单加（升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">158</Data></Cell>
    <Cell ss:StyleID="s115"><Data ss:Type="Number">179</Data></Cell>
    <Cell ss:StyleID="s115"><Data ss:Type="Number">184</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">活动前后单加（升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">159</Data></Cell>
    <Cell ss:StyleID="s115"><Data ss:Type="Number">180</Data></Cell>
    <Cell ss:StyleID="s115"><Data ss:Type="Number">185</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="9">
    <Cell ss:Index="4" ss:StyleID="s78"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">活动全周期日均销量（升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">160</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">181</Data></Cell>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">186</Data></Cell>
    <Cell ss:StyleID="s110"><Data ss:Type="Number">188</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="8.25">
    <Cell ss:Index="3" ss:StyleID="s96"/>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">161</Data></Cell>
    <Cell ss:StyleID="s117"/>
    <Cell ss:StyleID="s117"/>
    <Cell ss:StyleID="s118"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String">有活动时单升C3（元/升）</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="7" ss:StyleID="s91"><Data ss:Type="Number">182</Data></Cell>
    <Cell ss:StyleID="s91"><Data ss:Type="Number">187</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="9.75">
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">162</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s119"><Data ss:Type="String">活动全周期（天）</Data></Cell>
    <Cell ss:StyleID="s119"/>
    <Cell ss:StyleID="s102"/>
    <Cell ss:StyleID="s107"><Data ss:Type="Number">163</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String">销量贡献（KL）</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="7" ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#92</Data></Cell>
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">164</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#95</Data></Cell>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">165</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#0</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String">C3贡献</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">166</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#92</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">167</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#95</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="7" ss:StyleID="s63"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-#0</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">168</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s96"><Data ss:Type="String" x:Ticked="1">&#45;&#45;-NFR</Data></Cell>
    <Cell ss:StyleID="s96"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">169</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String" x:Ticked="1">   直降</Data></Cell>
    <Cell ss:StyleID="s90"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String" x:Ticked="1">   优惠券</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">170</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String" x:Ticked="1">   换购折扣</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">171</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String" x:Ticked="1">   赠品（SP&amp;A)</Data></Cell>
    <Cell ss:StyleID="s90"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s90"><Data ss:Type="String">   宣传等物料投入(SP&amp;A)</Data></Cell>
    <Cell ss:StyleID="s90"/>
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">172</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s121"><Data ss:Type="String">净利润(KRMB)</Data></Cell>
    <Cell ss:StyleID="s121"/>
    <Cell ss:Index="6" ss:StyleID="s107"><Data ss:Type="Number">173</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s103"><Data ss:Type="String" x:Ticked="1">总成本(KRMB)</Data></Cell>
    <Cell ss:StyleID="s104"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15.75">
    <Cell ss:Index="3" ss:StyleID="s103"><Data ss:Type="String" x:Ticked="1">ROI</Data></Cell>
    <Cell ss:StyleID="s104"/>
    <Cell ss:Index="6" ss:StyleID="s115"><Data ss:Type="Number">174</Data></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>-1</HorizontalResolution>
    <VerticalResolution>-1</VerticalResolution>
   </Print>
   <Selected/>
   <DoNotDisplayGridlines/>
   <FreezePanes/>
   <FrozenNoSplit/>
   <SplitHorizontal>3</SplitHorizontal>
   <TopRowBottomPane>33</TopRowBottomPane>
   <SplitVertical>4</SplitVertical>
   <LeftColumnRightPane>4</LeftColumnRightPane>
   <ActivePane>0</ActivePane>
   <Panes>
    <Pane>
     <Number>3</Number>
    </Pane>
    <Pane>
     <Number>1</Number>
    </Pane>
    <Pane>
     <Number>2</Number>
     <ActiveRow>4</ActiveRow>
    </Pane>
    <Pane>
     <Number>0</Number>
     <ActiveRow>41</ActiveRow>
     <ActiveCol>7</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <Worksheet ss:Name="名称定义">
  <Table ss:ExpandedColumnCount="2" ss:ExpandedRowCount="7" x:FullColumns="1"
   x:FullRows="1" ss:StyleID="s105" ss:DefaultColumnWidth="54"
   ss:DefaultRowHeight="14">
   <Column ss:StyleID="s105" ss:AutoFitWidth="0" ss:Width="108"/>
   <Column ss:StyleID="s105" ss:AutoFitWidth="0" ss:Width="440"/>
   <Row>
    <Cell><Data ss:Type="String">名称</Data></Cell>
    <Cell><Data ss:Type="String">定义</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">会员拉新/有效会员</Data></Cell>
    <Cell><Data ss:Type="String">有过消费记录的会员</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">活跃会员（当月）</Data></Cell>
    <Cell><Data ss:Type="String">在上一个度量周期没有消费，但在本度量周期内，有消费记录的会员</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">留存会员（两个月）</Data></Cell>
    <Cell><Data ss:Type="String">在本度量周期及上一个度量周期，均消费过一次的会员</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">忠诚会员（三个月）</Data></Cell>
    <Cell><Data ss:Type="String">在本度量周期，上一个度量周期，及上上个度量周期，均消费过一次的会员</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">沉睡会员</Data></Cell>
    <Cell><Data ss:Type="String">在本度量周期没有消费过的会员</Data></Cell>
   </Row>
   <Row>
    <Cell><Data ss:Type="String">流失会员</Data></Cell>
    <Cell><Data ss:Type="String">在本度量周期，上一个度量周期，及上上个度量周期，均没有消费过的有效会员</Data></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Visible>SheetHidden</Visible>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>20</ActiveRow>
     <ActiveCol>1</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <Worksheet ss:Name="换购">
  <Table ss:ExpandedColumnCount="8" ss:ExpandedRowCount="108" x:FullColumns="1"
   x:FullRows="1" ss:StyleID="s62" ss:DefaultColumnWidth="54"
   ss:DefaultRowHeight="14">
   <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="24"/>
   <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="33"/>
   <Column ss:StyleID="s62" ss:AutoFitWidth="0" ss:Width="174"/>
   <Column ss:StyleID="s63" ss:AutoFitWidth="0" ss:Width="116" ss:Span="4"/>
   <Row>
    <Cell ss:Index="4" ss:StyleID="s135"><Data ss:Type="String">油-非油换购</Data></Cell>
    <Cell ss:StyleID="s135"/>
    <Cell ss:StyleID="s135"/>
    <Cell ss:StyleID="s135"/>
    <Cell ss:StyleID="s89"><Data ss:Type="String">非油-非油换购</Data></Cell>
   </Row>
   <Row>
    <Cell ss:Index="4" ss:StyleID="s79"><Data ss:Type="String">#92</Data></Cell>
    <Cell ss:StyleID="s79"><Data ss:Type="String">#95</Data></Cell>
    <Cell ss:StyleID="s79"><Data ss:Type="String">#98</Data></Cell>
    <Cell ss:StyleID="s79"><Data ss:Type="String">#0</Data></Cell>
    <Cell ss:StyleID="s87"><Data ss:Type="String">NFR</Data></Cell>
   </Row>
   <Row>
    <Cell ss:Index="3" ss:StyleID="s82"><Data ss:Type="String">挂牌价（元/升）</Data></Cell>
    <Cell ss:StyleID="s66"><Data ss:Type="Number">6.5</Data></Cell>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row>
    <Cell ss:Index="3" ss:StyleID="s82"><Data ss:Type="String">采购价（元/升）</Data></Cell>
    <Cell ss:StyleID="s66"><Data ss:Type="Number">5</Data></Cell>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:Index="7" ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="2" ss:StyleID="s130"><Data ss:Type="String">商品1</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品名称</Data></Cell>
    <Cell ss:StyleID="s141"><Data ss:Type="String">AAA</Data></Cell>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s143"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"><Data ss:Type="Number">100</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"><Data ss:Type="Number">70</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">最低消费金额</Data></Cell>
    <Cell ss:StyleID="s85"><Data ss:Type="Number">238</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购金额</Data></Cell>
    <Cell ss:StyleID="s85"><Data ss:Type="Number">80</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s71"/>
    <Cell ss:StyleID="s71"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="2" ss:StyleID="s130"><Data ss:Type="String">商品2</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品名称</Data></Cell>
    <Cell ss:StyleID="s141"><Data ss:Type="String">BBB</Data></Cell>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s143"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"><Data ss:Type="Number">100</Data></Cell>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"><Data ss:Type="Number">70</Data></Cell>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">最低消费金额</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"><Data ss:Type="Number">100</Data></Cell>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购金额</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"><Data ss:Type="Number">80</Data></Cell>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s71"/>
    <Cell ss:StyleID="s71"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="2" ss:StyleID="s130"><Data ss:Type="String">商品3</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品名称</Data></Cell>
    <Cell ss:StyleID="s141"><Data ss:Type="String">CCC</Data></Cell>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s143"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">最低消费金额</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购金额</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s71"/>
    <Cell ss:StyleID="s71"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="2" ss:StyleID="s130"><Data ss:Type="String">商品4</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品名称</Data></Cell>
    <Cell ss:StyleID="s141"><Data ss:Type="String">DDD</Data></Cell>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s143"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">最低消费金额</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购金额</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s71"/>
    <Cell ss:StyleID="s71"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="2" ss:StyleID="s130"><Data ss:Type="String">商品5</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品名称</Data></Cell>
    <Cell ss:StyleID="s141"><Data ss:Type="String">EEE</Data></Cell>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s143"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">最低消费金额</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购金额</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s71"/>
    <Cell ss:StyleID="s71"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
    <Cell ss:StyleID="s86"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="16.5">
    <Cell ss:Index="2" ss:StyleID="s130"><Data ss:Type="String">商品6</Data></Cell>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品名称</Data></Cell>
    <Cell ss:StyleID="s141"><Data ss:Type="String">FFF</Data></Cell>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s142"/>
    <Cell ss:StyleID="s143"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品原价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购商品进价（含税）</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">最低消费金额</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s130"/>
    <Cell ss:StyleID="s70"><Data ss:Type="String">换购金额</Data></Cell>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s85"/>
    <Cell ss:StyleID="s88"/>
   </Row>
   <Row ss:Index="44">
    <Cell ss:Index="2" ss:StyleID="s139"><Data ss:Type="String">燃油</Data></Cell>
    <Cell ss:StyleID="s139"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15">
    <Cell ss:Index="2" ss:StyleID="s127"><Data ss:Type="String">无活动</Data></Cell>
    <Cell ss:StyleID="s128"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25">
    <Cell ss:Index="2" ss:StyleID="s129"><Data ss:Type="String">日均加油车辆（辆）</Data></Cell>
    <Cell ss:StyleID="s129"/>
    <Cell ss:StyleID="s69"/>
    <Cell ss:StyleID="s67"/>
    <Cell ss:StyleID="s67"/>
    <Cell ss:StyleID="s68"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动前</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!-#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动期</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s66"><Data ss:Type="Number">500</Data></Cell>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动后</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!+#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25">
    <Cell ss:Index="2" ss:StyleID="s129"><Data ss:Type="String">单车加油量（升）</Data></Cell>
    <Cell ss:StyleID="s129"/>
    <Cell ss:StyleID="s66"><Data ss:Type="Number">30</Data></Cell>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25">
    <Cell ss:Index="2" ss:StyleID="s129"><Data ss:Type="String">日均销量（升）</Data></Cell>
    <Cell ss:StyleID="s129"><Data ss:Type="String">无活动时日均销量（升）</Data></Cell>
    <Cell ss:StyleID="s69"/>
    <Cell ss:StyleID="s67"/>
    <Cell ss:StyleID="s67"/>
    <Cell ss:StyleID="s68"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动前</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!-#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-2]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-2]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-2]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-2]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动期</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-3]C"><Data ss:Type="Number">15000</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-3]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-3]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-3]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动后</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!+#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-4]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-4]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-4]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-5]C*R[-4]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row>
    <Cell ss:Index="3" ss:StyleID="s78"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row>
    <Cell ss:Index="3" ss:StyleID="s78"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s139"><Data ss:Type="String">燃油</Data></Cell>
    <Cell ss:StyleID="s139"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15">
    <Cell ss:Index="2" ss:StyleID="s131"><Data ss:Type="String">有活动</Data></Cell>
    <Cell ss:StyleID="s132"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25">
    <Cell ss:Index="2" ss:StyleID="s129"><Data ss:Type="String">预计日均加油车辆（辆）</Data></Cell>
    <Cell ss:StyleID="s129"/>
    <Cell ss:StyleID="s69"/>
    <Cell ss:StyleID="s67"/>
    <Cell ss:StyleID="s67"/>
    <Cell ss:StyleID="s68"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动前</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!-#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动期</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s66"><Data ss:Type="Number">500</Data></Cell>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动后</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!+#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25">
    <Cell ss:Index="2" ss:StyleID="s129"><Data ss:Type="String">预计活动参与率（%）</Data></Cell>
    <Cell ss:StyleID="s129"/>
    <Cell ss:StyleID="s77"><Data ss:Type="Number">0.3</Data></Cell>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25">
    <Cell ss:Index="2" ss:StyleID="s129"><Data ss:Type="String">预计换购商品比例（%）</Data></Cell>
    <Cell ss:StyleID="s129"/>
    <Cell ss:StyleID="s73" ss:Formula="=100%-SUM(R[1]C:R[6]C)"><Data
      ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=100%-SUM(R[1]C:R[6]C)"><Data
      ss:Type="Number">1</Data></Cell>
    <Cell ss:StyleID="s74" ss:Formula="=100%-SUM(R[1]C:R[6]C)"><Data
      ss:Type="Number">1</Data></Cell>
    <Cell ss:StyleID="s75" ss:Formula="=100%-SUM(R[1]C:R[6]C)"><Data
      ss:Type="Number">1</Data></Cell>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s134" ss:Formula="=R[-58]C[2]"><Data
      ss:Type="String">AAA</Data></Cell>
    <Cell ss:StyleID="s130"/>
    <Cell ss:StyleID="s77"><Data ss:Type="Number">1</Data></Cell>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s134" ss:Formula="=R[-53]C[2]"><Data
      ss:Type="String">BBB</Data></Cell>
    <Cell ss:StyleID="s130"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s134" ss:Formula="=R[-48]C[2]"><Data
      ss:Type="String">CCC</Data></Cell>
    <Cell ss:StyleID="s130"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s134" ss:Formula="=R[-43]C[2]"><Data
      ss:Type="String">DDD</Data></Cell>
    <Cell ss:StyleID="s130"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s134" ss:Formula="=R[-38]C[2]"><Data
      ss:Type="String">EEE</Data></Cell>
    <Cell ss:StyleID="s130"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25" ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s134" ss:Formula="=R[-33]C[2]"><Data
      ss:Type="String">FFF</Data></Cell>
    <Cell ss:StyleID="s130"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s77"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25">
    <Cell ss:Index="2" ss:StyleID="s129"><Data ss:Type="String">参与活动的车辆的单升加油量（升）</Data></Cell>
    <Cell ss:StyleID="s129"/>
    <Cell ss:StyleID="s76" ss:Formula="=R[-61]C/R[-68]C"><Data ss:Type="Number">36.615384615384613</Data></Cell>
    <Cell ss:StyleID="s76"/>
    <Cell ss:StyleID="s76"/>
    <Cell ss:StyleID="s76"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25">
    <Cell ss:Index="2" ss:StyleID="s129"><Data ss:Type="String">预计日均销量（升）</Data></Cell>
    <Cell ss:StyleID="s129"><Data ss:Type="String">无活动时日均销量（升）</Data></Cell>
    <Cell ss:StyleID="s69"/>
    <Cell ss:StyleID="s67"/>
    <Cell ss:StyleID="s67"/>
    <Cell ss:StyleID="s68"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动前</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!-#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动期</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65"
     ss:Formula="=R[-13]C*R[-11]C*R[-3]C+R[-13]C*(1-R[-11]C)*R[-24]C"><Data
      ss:Type="Number">15992.307692307691</Data></Cell>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s84"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动后</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!+#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s65"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Index="78" ss:AutoFitHeight="0" ss:Height="18.75">
    <Cell ss:Index="2" ss:StyleID="s133"><Data ss:Type="String">销量总变化（升）</Data></Cell>
    <Cell ss:StyleID="s133"/>
    <Cell ss:StyleID="s80" ss:Formula="=SUM(R[1]C:R[3]C)"><Data ss:Type="Number">992.30769230769147</Data></Cell>
    <Cell ss:StyleID="s80" ss:Formula="=SUM(R[1]C:R[3]C)"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s80" ss:Formula="=SUM(R[1]C:R[3]C)"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s81" ss:Formula="=SUM(R[1]C:R[3]C)"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动前</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!-#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动期</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">992.30769230769147</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动后</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!+#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=R[-6]C-R[-27]C"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row>
    <Cell ss:Index="3" ss:StyleID="s78"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Index="84">
    <Cell ss:Index="2" ss:StyleID="s140"><Data ss:Type="String">非油</Data></Cell>
    <Cell ss:StyleID="s140"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="15">
    <Cell ss:Index="2" ss:StyleID="s127"><Data ss:Type="String">无活动</Data></Cell>
    <Cell ss:StyleID="s128"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="14.25">
    <Cell ss:Index="2" ss:StyleID="s129"><Data ss:Type="String">预计日均销量（个）</Data></Cell>
    <Cell ss:StyleID="s136"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动前</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!-#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!-1,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=SUM(R[1]C:R[6]C)"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-81]C[2]"><Data
      ss:Type="String">AAA</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-76]C[2]"><Data
      ss:Type="String">BBB</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-71]C[2]"><Data
      ss:Type="String">CCC</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-66]C[2]"><Data
      ss:Type="String">DDD</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-61]C[2]"><Data
      ss:Type="String">EEE</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-56]C[2]"><Data
      ss:Type="String">FFF</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动期</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=SUM(R[1]C:R[6]C)"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-81]C[2]"><Data
      ss:Type="String">AAA</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-76]C[2]"><Data
      ss:Type="String">BBB</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-71]C[2]"><Data
      ss:Type="String">CCC</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-66]C[2]"><Data
      ss:Type="String">DDD</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-61]C[2]"><Data
      ss:Type="String">EEE</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-56]C[2]"><Data
      ss:Type="String">FFF</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row>
    <Cell ss:Index="2" ss:StyleID="s70"><Data ss:Type="String">活动后</Data></Cell>
    <Cell ss:StyleID="s72"
     ss:Formula="=TEXT(#REF!+1,&quot;yyyy年m月d日&quot;)&amp;&quot; - &quot;&amp;TEXT(#REF!+#REF!,&quot;yyyy年m月d日&quot;)"><Data
      ss:Type="Error">#REF!</Data></Cell>
    <Cell ss:StyleID="s65" ss:Formula="=SUM(R[1]C:R[6]C)"><Data ss:Type="Number">0</Data></Cell>
    <Cell ss:StyleID="s84"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s64"/>
    <Cell ss:StyleID="s62"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-81]C[2]"><Data
      ss:Type="String">AAA</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-76]C[2]"><Data
      ss:Type="String">BBB</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-71]C[2]"><Data
      ss:Type="String">CCC</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-66]C[2]"><Data
      ss:Type="String">DDD</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-61]C[2]"><Data
      ss:Type="String">EEE</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
   </Row>
   <Row ss:Hidden="1">
    <Cell ss:Index="2" ss:StyleID="s137" ss:Formula="=R[-56]C[2]"><Data
      ss:Type="String">FFF</Data></Cell>
    <Cell ss:StyleID="s138"/>
    <Cell ss:StyleID="s66"/>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Visible>SheetHidden</Visible>
   <DoNotDisplayGridlines/>
   <FreezePanes/>
   <FrozenNoSplit/>
   <SplitHorizontal>2</SplitHorizontal>
   <TopRowBottomPane>57</TopRowBottomPane>
   <SplitVertical>3</SplitVertical>
   <LeftColumnRightPane>3</LeftColumnRightPane>
   <ActivePane>0</ActivePane>
   <Panes>
    <Pane>
     <Number>3</Number>
    </Pane>
    <Pane>
     <Number>1</Number>
    </Pane>
    <Pane>
     <Number>2</Number>
     <ActiveRow>1</ActiveRow>
    </Pane>
    <Pane>
     <Number>0</Number>
     <ActiveRow>109</ActiveRow>
     <ActiveCol>1</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
