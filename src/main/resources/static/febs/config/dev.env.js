'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // BASE_API: '"http://47.110.128.72:8081/rest"',
  BASE_API: '"http://localhost:8080/rest"'
  
})
